/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.runtime.generic.network;

import im.aist.runtime.mtproto.AsyncConnection;
import im.aist.runtime.mtproto.AsyncConnectionFactory;
import im.aist.runtime.mtproto.AsyncConnectionInterface;
import im.aist.runtime.mtproto.ConnectionEndpoint;

// Disabling Bounds checks for speeding up calculations

/*-[
#define J2OBJC_DISABLE_ARRAY_BOUND_CHECKS 1
]-*/

public class AsyncTcpConnectionFactory implements AsyncConnectionFactory {

    @Override
    public AsyncConnection createConnection(int connectionId, ConnectionEndpoint endpoint, AsyncConnectionInterface connectionInterface) {
        return new AsyncTcpConnection(connectionId, endpoint, connectionInterface);
    }
}
