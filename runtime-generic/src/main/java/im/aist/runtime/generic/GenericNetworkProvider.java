package im.aist.runtime.generic;/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

import im.aist.runtime.generic.network.AsyncTcpConnectionFactory;
import im.aist.runtime.mtproto.ManagedNetworkProvider;

public class GenericNetworkProvider extends ManagedNetworkProvider {

    public GenericNetworkProvider() {
        super(new AsyncTcpConnectionFactory());
    }
}