package im.aist.runtime.generic;

import im.aist.runtime.*;
import im.aist.runtime.Runtime;
import im.aist.runtime.crypto.primitives.kuznechik.KuznechikFastEngine;

public class GenericCryptoProvider extends DefaultCryptoRuntime {

    private static boolean isLoaded = false;
    private static final Object LOCk = new Object();

    public GenericCryptoProvider() {
        Runtime.dispatch(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                byte[] tables = Assets.loadBinAsset("kuz_tables.bin");
                if (tables != null) {
                    KuznechikFastEngine.initDump(tables);
                } else {
                    KuznechikFastEngine.initCalc();
                }
                synchronized (LOCk) {
                    isLoaded = true;
                    LOCk.notifyAll();
                }
            }
        });
    }

    @Override
    public void waitForCryptoLoaded() {
        if (isLoaded) {
            return;
        }
        synchronized (LOCk) {
            if (isLoaded) {
                return;
            }
            try {
                LOCk.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }
}
