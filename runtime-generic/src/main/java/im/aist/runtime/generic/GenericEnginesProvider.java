package im.aist.runtime.generic;

import im.aist.runtime.generic.mvvm.BindedDisplayList;
import im.aist.runtime.generic.mvvm.DisplayList;
import im.aist.runtime.generic.storage.AsyncListEngine;
import im.aist.runtime.EnginesRuntime;
import im.aist.runtime.bser.BserCreator;
import im.aist.runtime.bser.BserObject;
import im.aist.runtime.mvvm.PlatformDisplayList;
import im.aist.runtime.storage.ListEngine;
import im.aist.runtime.storage.ListEngineItem;
import im.aist.runtime.storage.ListStorage;
import im.aist.runtime.storage.ListStorageDisplayEx;

public class GenericEnginesProvider implements EnginesRuntime {

//    static {
//        ActorSystem.system().addDispatcher("display_list");
//        ActorSystem.system().addDispatcher("db", 1);
//    }

    private DisplayList.OperationMode operationMode;

    public GenericEnginesProvider(DisplayList.OperationMode operationMode) {
        this.operationMode = operationMode;
    }

    @Override
    public <T extends BserObject & ListEngineItem> ListEngine<T> createListEngine(ListStorage storage, BserCreator<T> creator) {
        return new AsyncListEngine<T>((ListStorageDisplayEx) storage, creator);
    }

    @Override
    public <T extends BserObject & ListEngineItem> PlatformDisplayList<T> createDisplayList(ListEngine<T> listEngine, boolean isSharedInstance, String clazz) {
        return new BindedDisplayList<T>((AsyncListEngine<T>) listEngine, isSharedInstance, 20, 20, operationMode);
    }
}
