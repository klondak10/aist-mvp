package im.aist.runtime.generic;

import im.aist.runtime.generic.threading.GenericAtomicInteger;
import im.aist.runtime.generic.threading.GenericAtomicLong;
import im.aist.runtime.generic.threading.GenericImmediateDispatcher;
import im.aist.runtime.generic.threading.GenericThreadLocal;
import im.aist.runtime.ThreadingRuntime;
import im.aist.runtime.actors.ThreadPriority;
import im.aist.runtime.generic.threading.GenericWeakReference;
import im.aist.runtime.threading.AtomicIntegerCompat;
import im.aist.runtime.threading.AtomicLongCompat;
import im.aist.runtime.threading.ImmediateDispatcher;
import im.aist.runtime.threading.ThreadLocalCompat;
import im.aist.runtime.threading.WeakReferenceCompat;

public abstract class GenericThreadingProvider implements ThreadingRuntime {

    public GenericThreadingProvider() {

    }

    @Override
    public long getActorTime() {
        return System.nanoTime() / 1000000;
    }

    @Override
    public long getCurrentTime() {
        return System.currentTimeMillis();
    }

    @Override
    public long getSyncedCurrentTime() {
        return getCurrentTime();
    }

    @Override
    public int getCoresCount() {
        return Runtime.getRuntime().availableProcessors();
    }

    @Override
    public AtomicIntegerCompat createAtomicInt(int value) {
        return new GenericAtomicInteger(value);
    }

    @Override
    public <T> WeakReferenceCompat<T> createWeakReference(T val) {
        return new GenericWeakReference<>(val);
    }

    @Override
    public AtomicLongCompat createAtomicLong(long value) {
        return new GenericAtomicLong(value);
    }

    @Override
    public <T> ThreadLocalCompat<T> createThreadLocal() {
        return new GenericThreadLocal<>();
    }

    @Override
    public ImmediateDispatcher createImmediateDispatcher(String name, ThreadPriority priority) {
        return new GenericImmediateDispatcher(name, priority);
    }
}
