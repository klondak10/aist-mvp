package im.aist.runtime.generic;

import im.aist.runtime.RegExpRuntime;
import im.aist.runtime.generic.regexp.GenericPattern;
import im.aist.runtime.regexp.PatternCompat;

public class GenericRegExpProvider implements RegExpRuntime {

    @Override
    public PatternCompat getPattern(String pattern) {
        return new GenericPattern(pattern);
    }
}
