package im.aist.runtime.generic.threading;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import im.aist.runtime.actors.ThreadPriority;
import im.aist.runtime.threading.ImmediateDispatcher;

public class GenericImmediateDispatcher implements ImmediateDispatcher {

    private final Executor EXECUTOR;

    public GenericImmediateDispatcher(String name, ThreadPriority priority) {
        EXECUTOR = Executors.newSingleThreadExecutor(r -> {
            Thread workingThread = new Thread(r);
            switch (priority) {
                case HIGH:
                    workingThread.setPriority(Thread.MAX_PRIORITY);
                case LOW:
                    workingThread.setPriority(Thread.MIN_PRIORITY);
                default:
                case NORMAL:
                    workingThread.setPriority(Thread.NORM_PRIORITY);
            }
            workingThread.setName(name);
            return workingThread;
        });
    }

    @Override
    public synchronized void dispatchNow(Runnable runnable) {
        EXECUTOR.execute(runnable);
    }
}
