package im.aist.core.js.providers.electron;

public interface JsElectronListener {
    void onEvent(String content);
}
