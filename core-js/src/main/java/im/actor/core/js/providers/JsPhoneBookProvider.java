/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.js.providers;

import java.util.ArrayList;

import im.aist.core.entity.PhoneBookContact;
import im.aist.core.providers.PhoneBookProvider;

public class JsPhoneBookProvider implements PhoneBookProvider {

    @Override
    public void loadPhoneBook(Callback callback) {
        callback.onLoaded(new ArrayList<PhoneBookContact>());
    }
}
