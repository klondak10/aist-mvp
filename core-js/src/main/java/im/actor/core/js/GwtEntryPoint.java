/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.js;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.Scheduler;

import org.timepedia.exporter.client.ExporterUtil;

import im.aist.core.entity.Contact;
import im.aist.core.entity.Dialog;
import im.aist.core.entity.Message;
import im.aist.core.entity.SearchEntity;
import im.aist.core.js.entity.JsContact;
import im.aist.core.js.entity.JsDialog;
import im.aist.core.js.entity.JsMessage;
import im.aist.core.js.entity.JsSearchEntity;
import im.aist.core.js.providers.Assets;
import im.aist.runtime.js.JsAssetsProvider;
import im.aist.runtime.js.JsEngineProvider;

public class GwtEntryPoint implements EntryPoint {

    public void onModuleLoad() {
        ExporterUtil.exportAll();
        JsAssetsProvider.registerBundle(Assets.INSTANCE);
        JsEngineProvider.registerEntity(Contact.ENTITY_NAME, JsContact.CONVERTER);
        JsEngineProvider.registerEntity(Dialog.ENTITY_NAME, JsDialog.CONVERTER);
        JsEngineProvider.registerEntity(Message.ENTITY_NAME, JsMessage.CONVERTER);
        JsEngineProvider.registerEntity(SearchEntity.ENTITY_NAME, JsSearchEntity.CONVERTER);

        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                onAppLoaded();
            }
        });
    }

    public native void onAppLoaded()/*-{
        if ($wnd.jsAppLoaded) $wnd.jsAppLoaded();
    }-*/;
}
