package im.aist.runtime.storage.memory;


import im.aist.runtime.StorageRuntime;
import im.aist.runtime.storage.KeyValueStorage;
import im.aist.runtime.storage.ListStorage;
import im.aist.runtime.storage.PreferencesStorage;

public class MemoryStorageRuntimeProvider implements StorageRuntime {

    @Override
    public PreferencesStorage createPreferencesStorage() {
        return new MemoryPreferencesStorage();
    }

    @Override
    public KeyValueStorage createKeyValue(String name) {
        return new MemoryKeyValueStorage();
    }

    @Override
    public ListStorage createList(String name) {
        return new MemoryListStorage();
    }

    @Override
    public void resetStorage() {
        // TODO: Implement
    }
}
