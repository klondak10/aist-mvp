package im.aist.runtime.actors.dispatch.queue;

public interface QueueCollectionListener {
    void onChanged();
}
