package im.aist.runtime.actors;

public interface ActorCancellable {

    void cancel();
}
