package im.aist.runtime.actors;

public interface Receiver {
    void onReceive(Object message);
}
