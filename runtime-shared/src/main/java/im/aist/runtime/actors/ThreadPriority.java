/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.runtime.actors;

public enum ThreadPriority {
    HIGH,
    NORMAL,
    LOW
}
