package im.aist.runtime.actors;

public interface ActorSupervisor {
    void onActorStopped(ActorRef ref);
}
