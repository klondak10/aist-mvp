package im.aist.runtime.actors;

import org.jetbrains.annotations.NotNull;

import im.aist.runtime.actors.ask.AskIntRequest;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.PoisonPill;
import im.aist.runtime.promise.Promise;
import im.aist.runtime.promise.PromiseFunc;
import im.aist.runtime.promise.PromiseResolver;

public abstract class ActorInterface {

    @NotNull
    private ActorRef dest;

    public ActorInterface(@NotNull ActorRef dest) {
        this.dest = dest;
    }

    protected ActorInterface() {

    }

    protected void setDest(@NotNull ActorRef ref) {
        this.dest = ref;
    }

    @NotNull
    public ActorRef getDest() {
        return dest;
    }

    protected void send(Object message) {
        dest.send(message);
    }

    protected <T> Promise<T> ask(@NotNull final AskMessage<T> message) {
        return new Promise<>(new PromiseFunc<T>() {
            @Override
            public void exec(@NotNull PromiseResolver<T> executor) {
                send(new AskIntRequest(message, executor));
            }
        });
    }

    public void kill() {
        dest.send(PoisonPill.INSTANCE);
    }
}
