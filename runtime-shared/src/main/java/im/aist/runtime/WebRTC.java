package im.aist.runtime;

import im.aist.runtime.promise.Promise;
import im.aist.runtime.webrtc.WebRTCIceServer;
import im.aist.runtime.webrtc.WebRTCMediaStream;
import im.aist.runtime.webrtc.WebRTCPeerConnection;
import im.aist.runtime.webrtc.WebRTCSettings;

public final class WebRTC {

    private static WebRTCRuntime rtcRuntime = new WebRTCRuntimeProvider();

    public static Promise<WebRTCPeerConnection> createPeerConnection(WebRTCIceServer[] iceServers,
                                                                     WebRTCSettings webRTCSettings) {
        return rtcRuntime.createPeerConnection(iceServers, webRTCSettings);
    }

    public static Promise<WebRTCMediaStream> getUserMedia(boolean isAudioEnabled, boolean isVideoEnabled) {
        return rtcRuntime.getUserMedia(isAudioEnabled, isVideoEnabled);
    }

    public static boolean isSupportsPreConnections() {
        return rtcRuntime.supportsPreConnections();
    }
}
