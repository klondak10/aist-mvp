package im.aist.runtime.webrtc.sdp.entities;

public enum SDPMediaMode {
    SEND_RECEIVE,
    SEND_ONLY,
    RECEIVE_ONLY,
    INACTIVE
}