package im.aist.runtime.webrtc;

public final class WebRTCTrackType {

    public static final int AUDIO = 0;
    public static final int VIDEO = 1;

    private WebRTCTrackType() {

    }
}
