package im.aist.runtime.threading;

public interface ImmediateDispatcher {
    void dispatchNow(Runnable runnable);
}
