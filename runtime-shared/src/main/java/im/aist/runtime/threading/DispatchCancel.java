package im.aist.runtime.threading;

public interface DispatchCancel {
    void cancel();
}