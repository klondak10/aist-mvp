package im.aist.runtime.threading;

public interface Dispatcher {
    DispatchCancel dispatch(Runnable message, long delay);
}