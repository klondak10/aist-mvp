package im.aist.runtime;

import im.aist.runtime.mtproto.ConnectionCallback;
import im.aist.runtime.mtproto.ConnectionEndpoint;
import im.aist.runtime.mtproto.CreateConnectionCallback;

/**
 * Created by ex3ndr on 08.08.15.
 */
public class Network {
    private static NetworkRuntime runtime = new NetworkRuntimeProvider();

    public static void createConnection(int connectionId, int mtprotoVersion, int apiMajorVersion,
                                        int apiMinorVersion, ConnectionEndpoint endpoint,
                                        ConnectionCallback callback, CreateConnectionCallback createCallback) {
        runtime.createConnection(connectionId, mtprotoVersion, apiMajorVersion, apiMinorVersion,
                endpoint, callback, createCallback);
    }
}
