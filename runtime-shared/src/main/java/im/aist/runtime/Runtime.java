package im.aist.runtime;

import com.google.j2objc.annotations.AutoreleasePool;

import im.aist.runtime.actors.ThreadPriority;
import im.aist.runtime.os.OSType;
import im.aist.runtime.power.WakeLock;
import im.aist.runtime.threading.Dispatcher;
import im.aist.runtime.threading.AtomicIntegerCompat;
import im.aist.runtime.threading.AtomicLongCompat;
import im.aist.runtime.threading.ImmediateDispatcher;
import im.aist.runtime.threading.ThreadLocalCompat;
import im.aist.runtime.threading.WeakReferenceCompat;

public class Runtime {

    private static final DispatcherRuntime dispatcherRuntime = new DispatcherRuntimeProvider();
    private static final ThreadingRuntime threadingRuntime = new ThreadingRuntimeProvider();
    private static final MainThreadRuntimeProvider mainThreadRuntime = new MainThreadRuntimeProvider();
    private static final LifecycleRuntime lifecycleRuntime = new LifecycleRuntimeProvider();
    private static final LocaleRuntime localeRuntime = new LocaleRuntimeProvider();

    public static OSType getSystemType() {
        return mainThreadRuntime.getOSType();
    }

    public static LocaleRuntime getLocaleRuntime() {
        return localeRuntime;
    }

    public static Dispatcher createDispatcher(String name) {
        return threadingRuntime.createDispatcher(name);
    }

    public static ImmediateDispatcher createImmediateDispatcher(String name, ThreadPriority priority) {
        return threadingRuntime.createImmediateDispatcher(name, priority);
    }

    public static long getActorTime() {
        return threadingRuntime.getActorTime();
    }

    public static long getCurrentTime() {
        return threadingRuntime.getCurrentTime();
    }

    public static long getCurrentSyncedTime() {
        return threadingRuntime.getSyncedCurrentTime();
    }

    public static AtomicIntegerCompat createAtomicInt(int init) {
        return threadingRuntime.createAtomicInt(init);
    }

    public static AtomicLongCompat createAtomicLong(long init) {
        return threadingRuntime.createAtomicLong(init);
    }

    public static <T> ThreadLocalCompat<T> createThreadLocal() {
        return threadingRuntime.createThreadLocal();
    }

    public static <T> WeakReferenceCompat<T> createWeakReference(T val) {
        return threadingRuntime.createWeakReference(val);
    }

    public static boolean isSingleThread() {
        return mainThreadRuntime.isSingleThread();
    }

    public static int getCoresCount() {
        return threadingRuntime.getCoresCount();
    }

    public static void checkMainThread() {
        if (RuntimeEnvironment.isProduction()) {
            // Do Nothing in production mode
            return;
        }
        if (mainThreadRuntime.isSingleThread()) {
            return;
        }
        if (!mainThreadRuntime.isMainThread()) {
            throw new RuntimeException("Unable to perform operation not from Main Thread");
        }
    }

    public static boolean isMainThread() {
        return mainThreadRuntime.isSingleThread() || mainThreadRuntime.isMainThread();
    }

    @AutoreleasePool
    public static void postToMainThread(Runnable runnable) {
        mainThreadRuntime.postToMainThread(runnable);
    }

    @AutoreleasePool
    public static void dispatch(Runnable runnable) {
        dispatcherRuntime.dispatch(runnable);
    }

    public static void killApp() {
        lifecycleRuntime.killApp();
    }

    public static WakeLock makeWakeLock() {
        return lifecycleRuntime.makeWakeLock();
    }
}