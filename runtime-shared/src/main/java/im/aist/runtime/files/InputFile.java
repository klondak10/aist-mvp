/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.runtime.files;

import com.google.j2objc.annotations.ObjectiveCName;

import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

public interface InputFile {

    @ObjectiveCName("readWithOffset:withLength:")
    Promise<FilePart> read(int fileOffset, int len);

    @ObjectiveCName("close")
    Promise<Void> close();
}