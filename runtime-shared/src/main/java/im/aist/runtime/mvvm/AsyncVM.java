/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.runtime.mvvm;

public abstract class AsyncVM {

    private boolean isDetached;

    protected final void post(final Object obj) {
        im.aist.runtime.Runtime.postToMainThread(() -> {
            if (!isDetached) {
                onObjectReceived(obj);
            }
        });
    }

    protected abstract void onObjectReceived(Object obj);

    public void detach() {
        isDetached = true;
    }
}