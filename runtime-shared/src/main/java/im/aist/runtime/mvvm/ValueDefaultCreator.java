package im.aist.runtime.mvvm;

public interface ValueDefaultCreator<T> {
    T createDefaultInstance(long id);
}
