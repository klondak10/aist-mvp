package im.aist.runtime.mvvm;

import im.aist.runtime.bser.BserObject;
import im.aist.runtime.storage.ListEngineItem;

public interface PlatformDisplayList<T extends BserObject & ListEngineItem> {

    void initCenter(long rid);

    void initTop();

    void initEmpty();
}
