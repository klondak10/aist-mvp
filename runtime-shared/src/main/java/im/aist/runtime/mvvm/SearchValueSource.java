package im.aist.runtime.mvvm;

import java.util.List;

import im.aist.runtime.function.Consumer;

public interface SearchValueSource<T> {
    void loadResults(String query, Consumer<List<T>> callback);
}
