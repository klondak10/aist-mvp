package im.aist.runtime.mvvm;

public interface ValueConverter<T, S> {
    S convert(T src);
}
