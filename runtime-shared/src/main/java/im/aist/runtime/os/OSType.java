package im.aist.runtime.os;

public enum OSType {
    ANDROID, IOS, WEB, OTHER
}
