package im.aist.runtime;

import im.aist.runtime.bser.BserCreator;
import im.aist.runtime.bser.BserObject;
import im.aist.runtime.files.FileSystemReference;
import im.aist.runtime.mvvm.BaseValueModel;
import im.aist.runtime.mvvm.PlatformDisplayList;
import im.aist.runtime.mvvm.MVVMCollection;
import im.aist.runtime.mvvm.ValueDefaultCreator;
import im.aist.runtime.mvvm.ValueModelCreator;
import im.aist.runtime.storage.KeyValueItem;
import im.aist.runtime.storage.KeyValueStorage;
import im.aist.runtime.storage.ListEngine;
import im.aist.runtime.storage.ListEngineItem;
import im.aist.runtime.storage.PreferencesStorage;

public class Storage {

    private static final StorageRuntime storageRuntime = new StorageRuntimeProvider();
    private static final EnginesRuntime enginesRuntime = new EnginesRuntimeProvider();
    private static final FileSystemRuntime fileSystemRuntime = new FileSystemRuntimeProvider();

    // Runtimes

    public static StorageRuntime getStorageRuntime() {
        return storageRuntime;
    }

    public static EnginesRuntime getEnginesRuntime() {
        return enginesRuntime;
    }

    public static FileSystemRuntime getFileSystemRuntime() {
        return fileSystemRuntime;
    }


    // Storage

    public static PreferencesStorage createPreferencesStorage() {
        return storageRuntime.createPreferencesStorage();
    }

    public static KeyValueStorage createKeyValue(String name) {
        return storageRuntime.createKeyValue(name);
    }

    public static <T extends BserObject & ListEngineItem> ListEngine<T> createList(String name, BserCreator<T> creator) {
        return enginesRuntime.createListEngine(storageRuntime.createList(name), creator);
    }

    public static <V extends BaseValueModel<T>, T extends BserObject & KeyValueItem> MVVMCollection<T, V> createKeyValue(String name,
                                                                                                                         ValueModelCreator<T, V> wrapperCreator,
                                                                                                                         BserCreator<T> creator) {
        return new MVVMCollection<>(storageRuntime.createKeyValue(name), wrapperCreator, creator);
    }

    public static <V extends BaseValueModel<T>, T extends BserObject & KeyValueItem> MVVMCollection<T, V> createKeyValue(String name,
                                                                                                                         ValueModelCreator<T, V> wrapperCreator,
                                                                                                                         BserCreator<T> creator,
                                                                                                                         ValueDefaultCreator<T> defaultCreator) {
        return new MVVMCollection<>(storageRuntime.createKeyValue(name), wrapperCreator, creator, defaultCreator);
    }

    public static <T extends BserObject & ListEngineItem> PlatformDisplayList<T> createDisplayList(ListEngine<T> engine,
                                                                                                   boolean isSharedInstance,
                                                                                                   String entityName) {
        return enginesRuntime.createDisplayList(engine, isSharedInstance, entityName);
    }

    public static void resetStorage() {
        storageRuntime.resetStorage();
    }

    // Files

    public static FileSystemReference createTempFile() {
        return fileSystemRuntime.createTempFile();
    }

    public static boolean isFsPersistent() {
        return fileSystemRuntime.isFsPersistent();
    }

    public static FileSystemReference fileFromDescriptor(String descriptor) {
        return fileSystemRuntime.fileFromDescriptor(descriptor);
    }

    public static FileSystemReference commitTempFile(FileSystemReference sourceFile, long fileId, String fileName) {
        return fileSystemRuntime.commitTempFile(sourceFile, fileId, fileName);
    }
}
