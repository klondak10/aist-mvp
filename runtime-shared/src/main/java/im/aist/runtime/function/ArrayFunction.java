package im.aist.runtime.function;

public interface ArrayFunction<T, R> {
    R apply(T[] t);
}