package im.aist.runtime.function;

public interface Supplier<T> {
    T get();
}
