package im.aist.runtime.function;

public interface Closable {
    void close();
}