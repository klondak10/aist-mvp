package im.aist.runtime.function;

public interface Cancellable {

    boolean isCancelled();

    void cancel();
}
