package im.aist.runtime.function;

public interface Function<T, R> {
    R apply(T t);
}
