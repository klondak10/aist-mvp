package im.aist.runtime.function;

public interface Constructor<T> {
    T create();
}
