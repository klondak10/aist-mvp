package im.aist.runtime.function;

public interface BiConsumer<T, U> {
    void accept(T t, U u);
}
