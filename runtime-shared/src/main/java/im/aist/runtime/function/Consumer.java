package im.aist.runtime.function;

public interface Consumer<T> {
    void apply(T t);
}
