package im.aist.runtime.intl.plurals;

public interface PluralEngine {
    int getPluralType(int value);
}
