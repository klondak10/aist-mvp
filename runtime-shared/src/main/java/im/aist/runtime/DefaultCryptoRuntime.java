package im.aist.runtime;

import im.aist.runtime.crypto.BlockCipher;
import im.aist.runtime.crypto.Digest;
import im.aist.runtime.crypto.primitives.aes.AESFastEngine;
import im.aist.runtime.crypto.primitives.digest.SHA256;

public abstract class DefaultCryptoRuntime implements CryptoRuntime {

    @Override
    public Digest SHA256() {
        return new SHA256();
    }

    @Override
    public BlockCipher AES128(byte[] key) {
        return new AESFastEngine(key);
    }
}
