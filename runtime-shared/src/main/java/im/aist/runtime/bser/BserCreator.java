/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.runtime.bser;

public interface BserCreator<T> {
    T createInstance();
}
