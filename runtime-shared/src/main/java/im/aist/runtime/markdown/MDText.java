package im.aist.runtime.markdown;

public abstract class MDText {

    public abstract String toMarkdown();
}