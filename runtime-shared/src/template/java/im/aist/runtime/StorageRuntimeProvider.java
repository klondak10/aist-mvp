package im.aist.runtime;

import im.aist.runtime.storage.KeyValueStorage;
import im.aist.runtime.storage.ListStorage;
import im.aist.runtime.storage.PreferencesStorage;

public class StorageRuntimeProvider implements StorageRuntime {

    @Override
    public PreferencesStorage createPreferencesStorage() {
        throw new RuntimeException("Dumb");
    }

    @Override
    public KeyValueStorage createKeyValue(String name) {
        throw new RuntimeException("Dumb");
    }

    @Override
    public ListStorage createList(String name) {
        throw new RuntimeException("Dumb");
    }

    @Override
    public void resetStorage() {
        throw new RuntimeException("Dumb");
    }
}
