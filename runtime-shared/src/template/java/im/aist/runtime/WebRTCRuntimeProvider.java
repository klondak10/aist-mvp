package im.aist.runtime;

import org.jetbrains.annotations.NotNull;

import im.aist.runtime.promise.Promise;
import im.aist.runtime.webrtc.WebRTCIceServer;
import im.aist.runtime.webrtc.WebRTCMediaStream;
import im.aist.runtime.webrtc.WebRTCPeerConnection;
import im.aist.runtime.webrtc.WebRTCSettings;

public class WebRTCRuntimeProvider implements WebRTCRuntime {

    @NotNull
    @Override
    public Promise<WebRTCPeerConnection> createPeerConnection(WebRTCIceServer[] webRTCIceServers, WebRTCSettings settings) {
        return Promise.failure(new RuntimeException("Dumb"));
    }

    @NotNull
    @Override
    public Promise<WebRTCMediaStream> getUserMedia(boolean isAudioEnabled, boolean isVideoEnabled) {
        return Promise.failure(new RuntimeException("Dumb"));
    }

    @Override
    public boolean supportsPreConnections() {
        throw new RuntimeException("Dumb");
    }
}
