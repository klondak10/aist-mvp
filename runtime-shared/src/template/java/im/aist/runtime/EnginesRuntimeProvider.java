package im.aist.runtime;

import im.aist.runtime.bser.BserCreator;
import im.aist.runtime.bser.BserObject;
import im.aist.runtime.mvvm.PlatformDisplayList;
import im.aist.runtime.storage.ListEngine;
import im.aist.runtime.storage.ListEngineItem;
import im.aist.runtime.storage.ListStorage;

public class EnginesRuntimeProvider implements EnginesRuntime {

    @Override
    public <T extends BserObject & ListEngineItem> ListEngine<T> createListEngine(ListStorage storage, BserCreator<T> creator) {
        throw new RuntimeException("Dumb");
    }

    @Override
    public <T extends BserObject & ListEngineItem> PlatformDisplayList<T> createDisplayList(ListEngine<T> listEngine, boolean isSharedInstance, String clazz) {
        throw new RuntimeException("Dumb");
    }
}
