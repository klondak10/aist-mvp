package im.aist.core.viewmodel;

import java.util.ArrayList;
import im.aist.runtime.mvvm.ValueModel;

public class GalleryVM {

    private ValueModel<ArrayList<String>> galleryMediaPath;

    public GalleryVM() {
        galleryMediaPath = new ValueModel<>("gallery.photo", new ArrayList<String>());
    }

    public ValueModel<ArrayList<String>> getGalleryMediaPath() {
        return galleryMediaPath;
    }
}
