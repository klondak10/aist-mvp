package im.aist.core.viewmodel;

public enum CallState {
    RINGING, CONNECTING, IN_PROGRESS, ENDED
}
