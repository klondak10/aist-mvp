package im.aist.core.viewmodel;

public interface FileEventCallback {
    void onDownloaded(long fileId);
}
