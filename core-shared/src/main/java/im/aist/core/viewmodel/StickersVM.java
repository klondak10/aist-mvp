package im.aist.core.viewmodel;

import java.util.ArrayList;

import im.aist.core.entity.StickerPack;
import im.aist.runtime.mvvm.ValueModel;

public class StickersVM {

    private ValueModel<ArrayList<StickerPack>> ownStickerPacks;

    public StickersVM() {
        ownStickerPacks = new ValueModel<>("stickers.own",new ArrayList<StickerPack>());
    }

    public ValueModel<ArrayList<StickerPack>> getOwnStickerPacks() {
        return ownStickerPacks;
    }
}
