/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity.content;

import im.aist.core.api.ApiServiceExUserJoined;
import im.aist.core.api.ApiServiceMessage;
import im.aist.core.entity.content.internal.ContentRemoteContainer;

public class ServiceGroupUserJoined extends ServiceContent {

    public static ServiceGroupUserJoined create() {
        return new ServiceGroupUserJoined(new ContentRemoteContainer(new ApiServiceMessage("User joined",
                new ApiServiceExUserJoined())));
    }

    public ServiceGroupUserJoined(ContentRemoteContainer contentContainer) {
        super(contentContainer);
    }
}
