package im.aist.core.entity;

public enum PeerSearchType {
    CONTACTS, GROUPS, PUBLIC
}
