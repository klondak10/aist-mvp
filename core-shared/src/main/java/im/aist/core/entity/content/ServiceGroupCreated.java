/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity.content;

import im.aist.core.api.ApiServiceExGroupCreated;
import im.aist.core.api.ApiServiceMessage;
import im.aist.core.entity.content.internal.ContentRemoteContainer;

public class ServiceGroupCreated extends ServiceContent {

    public static ServiceGroupCreated create() {
        return new ServiceGroupCreated(new ContentRemoteContainer(new ApiServiceMessage("Group created",
                new ApiServiceExGroupCreated())));
    }

    public ServiceGroupCreated(ContentRemoteContainer contentContainer) {
        super(contentContainer);
    }
}
