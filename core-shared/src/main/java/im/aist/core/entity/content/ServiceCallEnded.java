/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity.content;

import im.aist.core.entity.content.internal.ContentRemoteContainer;

public class ServiceCallEnded extends ServiceContent {


    public ServiceCallEnded(ContentRemoteContainer contentContainer) {
        super(contentContainer);
    }
}
