/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity.content;

import im.aist.core.api.ApiServiceExUserLeft;
import im.aist.core.api.ApiServiceMessage;
import im.aist.core.entity.content.internal.ContentRemoteContainer;

public class ServiceGroupUserLeave extends ServiceContent {

    public static ServiceGroupUserLeave create() {
        return new ServiceGroupUserLeave(new ContentRemoteContainer(new ApiServiceMessage("User leave",
                new ApiServiceExUserLeft())));
    }

    public ServiceGroupUserLeave(ContentRemoteContainer contentContainer) {
        super(contentContainer);
    }
}
