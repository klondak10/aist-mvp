/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity;

public enum ContactRecordType {
    PHONE, EMAIL, WEB, SOCIAL
}
