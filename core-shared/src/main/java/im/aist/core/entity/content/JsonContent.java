package im.aist.core.entity.content;

import im.aist.core.api.ApiJsonMessage;
import im.aist.core.entity.content.internal.ContentRemoteContainer;
import im.aist.runtime.json.JSONException;
import im.aist.runtime.json.JSONObject;

public class JsonContent extends AbsContent {

    private String rawJson;

    public JsonContent(ContentRemoteContainer contentRemoteContainer) {
        super(contentRemoteContainer);
        ApiJsonMessage json = ((ApiJsonMessage) contentRemoteContainer.getMessage());
        rawJson = json.getRawJson();
    }

    public String getRawJson() {
        return rawJson;
    }

    public String getContentDescription() {
        String res;
        try {
            JSONObject data = new JSONObject(getRawJson());
            res = data.getJSONObject("data").getString("text");
        } catch (JSONException e) {
            res = "";
        }
        return res;
    }
}
