/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity.content;

import java.io.IOException;

import im.aist.core.api.ApiDocumentExAnimation;
import im.aist.core.api.ApiDocumentExPhoto;
import im.aist.core.api.ApiDocumentExVideo;
import im.aist.core.api.ApiDocumentExVoice;
import im.aist.core.api.ApiDocumentMessage;
import im.aist.core.api.ApiJsonMessage;
import im.aist.core.api.ApiMessage;
import im.aist.core.api.ApiServiceEx;
import im.aist.core.api.ApiServiceExChangedAvatar;
import im.aist.core.api.ApiServiceExChangedTitle;
import im.aist.core.api.ApiServiceExChangedTopic;
import im.aist.core.api.ApiServiceExChangedAbout;
import im.aist.core.api.ApiServiceExContactRegistered;
import im.aist.core.api.ApiServiceExGroupCreated;
import im.aist.core.api.ApiServiceExPhoneCall;
import im.aist.core.api.ApiServiceExPhoneMissed;
import im.aist.core.api.ApiServiceExUserInvited;
import im.aist.core.api.ApiServiceExUserJoined;
import im.aist.core.api.ApiServiceExUserKicked;
import im.aist.core.api.ApiServiceExUserLeft;
import im.aist.core.api.ApiServiceMessage;
import im.aist.core.api.ApiStickerMessage;
import im.aist.core.api.ApiTextMessage;
import im.aist.core.entity.content.internal.AbsContentContainer;
import im.aist.core.entity.content.internal.AbsLocalContent;
import im.aist.core.entity.content.internal.ContentLocalContainer;
import im.aist.core.entity.content.internal.ContentRemoteContainer;
import im.aist.core.entity.content.internal.LocalAnimation;
import im.aist.core.entity.content.internal.LocalDocument;
import im.aist.core.entity.content.internal.LocalPhoto;
import im.aist.core.entity.content.internal.LocalVideo;
import im.aist.core.entity.content.internal.LocalVoice;
import im.aist.runtime.bser.BserParser;
import im.aist.runtime.bser.BserValues;
import im.aist.runtime.bser.BserWriter;
import im.aist.runtime.bser.DataInput;
import im.aist.runtime.bser.DataOutput;
import im.aist.runtime.json.JSONObject;

// Disabling Bounds checks for speeding up calculations

/*-[
#define J2OBJC_DISABLE_ARRAY_BOUND_CHECKS 1
]-*/

public abstract class AbsContent {

    int updatedCounter = 0;

    public static byte[] serialize(AbsContent content) throws IOException {
        DataOutput dataOutput = new DataOutput();
        BserWriter writer = new BserWriter(dataOutput);
        // Mark new layout
        writer.writeBool(32, true);
        // Write content
        writer.writeBytes(33, content.getContentContainer().buildContainer());
        return dataOutput.toByteArray();
    }

    public static AbsContent fromMessage(ApiMessage message) {
        return convertData(new ContentRemoteContainer(message));
    }

    public static AbsContent parse(byte[] data) throws IOException {
        BserValues reader = new BserValues(BserParser.deserialize(new DataInput(data)));
        AbsContentContainer container;
        // Is New Layout
        if (reader.getBool(32, false)) {
            container = AbsContentContainer.loadContainer(reader.getBytes(33));
        } else {
            throw new RuntimeException("Unsupported obsolete format");
        }
        return convertData(container);
    }

    protected static AbsContent convertData(AbsContentContainer container) {

        if (container instanceof ContentLocalContainer) {
            ContentLocalContainer localContainer = (ContentLocalContainer) container;
            AbsLocalContent content = ((ContentLocalContainer) container).getContent();
            if (content instanceof LocalPhoto) {
                return new PhotoContent(localContainer);
            } else if (content instanceof LocalVideo) {
                return new VideoContent(localContainer);
            } else if (content instanceof LocalVoice) {
                return new VoiceContent(localContainer);
            } else if (content instanceof LocalAnimation) {
                return new AnimationContent(localContainer);
            } else if (content instanceof LocalDocument) {
                return new DocumentContent(localContainer);
            } else {
                throw new RuntimeException("Unknown type");
            }
        } else if (container instanceof ContentRemoteContainer) {
            ContentRemoteContainer remoteContainer = (ContentRemoteContainer) container;
            ApiMessage content = ((ContentRemoteContainer) container).getMessage();
            try {
                if (content instanceof ApiDocumentMessage) {
                    ApiDocumentMessage d = (ApiDocumentMessage) content;
                    if (d.getExt() instanceof ApiDocumentExPhoto) {
                        return new PhotoContent(remoteContainer);
                    } else if (d.getExt() instanceof ApiDocumentExVideo) {
                        return new VideoContent(remoteContainer);
                    } else if (d.getExt() instanceof ApiDocumentExVoice) {
                        return new VoiceContent(remoteContainer);
                    } else if (d.getExt() instanceof ApiDocumentExAnimation) {
                        return new AnimationContent(remoteContainer);
                    } else {
                        return new DocumentContent(remoteContainer);
                    }
                } else if (content instanceof ApiTextMessage) {
                    return new TextContent(remoteContainer);
                } else if (content instanceof ApiServiceMessage) {
                    ApiServiceEx ext = ((ApiServiceMessage) content).getExt();
                    if (ext instanceof ApiServiceExContactRegistered) {
                        return new ServiceUserRegistered(remoteContainer);
                    } else if (ext instanceof ApiServiceExChangedTitle) {
                        return new ServiceGroupTitleChanged(remoteContainer);
                    } else if (ext instanceof ApiServiceExChangedTopic) {
                        return new ServiceGroupTopicChanged(remoteContainer);
                    } else if (ext instanceof ApiServiceExChangedAbout) {
                        return new ServiceGroupAboutChanged(remoteContainer);
                    } else if (ext instanceof ApiServiceExChangedAvatar) {
                        return new ServiceGroupAvatarChanged(remoteContainer);
                    } else if (ext instanceof ApiServiceExGroupCreated) {
                        return new ServiceGroupCreated(remoteContainer);
                    } else if (ext instanceof ApiServiceExUserInvited) {
                        return new ServiceGroupUserInvited(remoteContainer);
                    } else if (ext instanceof ApiServiceExUserKicked) {
                        return new ServiceGroupUserKicked(remoteContainer);
                    } else if (ext instanceof ApiServiceExUserLeft) {
                        return new ServiceGroupUserLeave(remoteContainer);
                    } else if (ext instanceof ApiServiceExUserJoined) {
                        return new ServiceGroupUserJoined(remoteContainer);
                    } else if (ext instanceof ApiServiceExPhoneCall) {
                        return new ServiceCallEnded(remoteContainer);
                    } else if (ext instanceof ApiServiceExPhoneMissed) {
                        return new ServiceCallMissed(remoteContainer);
                    } else {
                        return new ServiceContent(remoteContainer);
                    }
                } else if (content instanceof ApiJsonMessage) {
                    ApiJsonMessage json = (ApiJsonMessage) content;
                    JSONObject object = new JSONObject(json.getRawJson());
                    if (object.getString("dataType").equals("contact")) {
                        return new ContactContent(remoteContainer);
                    } else if (object.getString("dataType").equals("location")) {
                        return new LocationContent(remoteContainer);
                    } else {
                        return new JsonContent(remoteContainer);
                    }
                } else if (content instanceof ApiStickerMessage) {
                    return new StickerContent(remoteContainer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Fallback
            return new UnsupportedContent(remoteContainer);
        } else {
            throw new RuntimeException("Unknown type");
        }
    }

    private AbsContentContainer contentContainer;

    public AbsContent() {
        super();
    }

    public AbsContent(ContentRemoteContainer contentContainer) {
        this.contentContainer = contentContainer;
    }

    public AbsContent(ContentLocalContainer contentContainer) {
        this.contentContainer = contentContainer;
    }

    public AbsContentContainer getContentContainer() {
        return contentContainer;
    }

    protected void setContentContainer(AbsContentContainer contentContainer) {
        this.contentContainer = contentContainer;
    }

    public int getUpdatedCounter() {
        return updatedCounter;
    }

    public AbsContent incrementUpdatedCounter(int oldCounter) {
        updatedCounter = ++oldCounter;
        return this;
    }
}