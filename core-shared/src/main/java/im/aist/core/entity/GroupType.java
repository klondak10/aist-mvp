package im.aist.core.entity;

public enum GroupType {
    GROUP, CHANNEL, OTHER
}
