/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity;

import com.google.j2objc.annotations.Property;

import java.io.IOException;

import im.aist.core.entity.content.AbsContent;
import im.aist.core.entity.content.ContactContent;
import im.aist.core.entity.content.DocumentContent;
import im.aist.core.entity.content.JsonContent;
import im.aist.core.entity.content.LocationContent;
import im.aist.core.entity.content.PhotoContent;
import im.aist.core.entity.content.ServiceCallEnded;
import im.aist.core.entity.content.ServiceCallMissed;
import im.aist.core.entity.content.ServiceContent;
import im.aist.core.entity.content.ServiceGroupAvatarChanged;
import im.aist.core.entity.content.ServiceGroupAboutChanged;
import im.aist.core.entity.content.ServiceGroupTopicChanged;
import im.aist.core.entity.content.ServiceGroupCreated;
import im.aist.core.entity.content.ServiceGroupTitleChanged;
import im.aist.core.entity.content.ServiceGroupUserInvited;
import im.aist.core.entity.content.ServiceGroupUserJoined;
import im.aist.core.entity.content.ServiceGroupUserKicked;
import im.aist.core.entity.content.ServiceGroupUserLeave;
import im.aist.core.entity.content.ServiceUserRegistered;
import im.aist.core.entity.content.StickerContent;
import im.aist.core.entity.content.TextContent;
import im.aist.core.entity.content.VideoContent;
import im.aist.core.entity.content.VoiceContent;
import im.aist.runtime.bser.Bser;
import im.aist.runtime.bser.BserObject;
import im.aist.runtime.bser.BserValues;
import im.aist.runtime.bser.BserWriter;


public class ContentDescription extends BserObject {

    public static ContentDescription fromBytes(byte[] data) throws IOException {
        return Bser.parse(new ContentDescription(), data);
    }

    public static ContentDescription fromContent(AbsContent msg) {
        if (msg instanceof TextContent) {
            return new ContentDescription(ContentType.TEXT,
                    ((TextContent) msg).getText());
        } else if (msg instanceof PhotoContent) {
            return new ContentDescription(ContentType.DOCUMENT_PHOTO);
        } else if (msg instanceof VideoContent) {
            return new ContentDescription(ContentType.DOCUMENT_VIDEO);
        } else if (msg instanceof VoiceContent) {
            return new ContentDescription(ContentType.DOCUMENT_AUDIO);
        } else if (msg instanceof DocumentContent) {
            return new ContentDescription(ContentType.DOCUMENT);
        } else if (msg instanceof LocationContent) {
            return new ContentDescription(ContentType.LOCATION);
        } else if (msg instanceof ContactContent) {
            return new ContentDescription(ContentType.CONTACT);
        } else if (msg instanceof StickerContent) {
            return new ContentDescription(ContentType.STICKER);
        } else if (msg instanceof ServiceUserRegistered) {
            return new ContentDescription(ContentType.SERVICE_REGISTERED);
        } else if (msg instanceof ServiceCallEnded) {
            return new ContentDescription(ContentType.SERVICE_CALL_ENDED);
        } else if (msg instanceof ServiceCallMissed) {
            return new ContentDescription(ContentType.SERVICE_CALL_MISSED);
        } else if (msg instanceof ServiceGroupAvatarChanged) {
            if (((ServiceGroupAvatarChanged) msg).getNewAvatar() == null) {
                return new ContentDescription(ContentType.SERVICE_AVATAR_REMOVED);
            } else {
                return new ContentDescription(ContentType.SERVICE_AVATAR);
            }
        } else if (msg instanceof ServiceGroupTitleChanged) {
            return new ContentDescription(ContentType.SERVICE_TITLE,
                    ((ServiceGroupTitleChanged) msg).getNewTitle());
        } else if (msg instanceof ServiceGroupTopicChanged) {
            return new ContentDescription(ContentType.SERVICE_TOPIC,
                    ((ServiceGroupTopicChanged) msg).getNewTopic());
        } else if (msg instanceof ServiceGroupAboutChanged) {
            return new ContentDescription(ContentType.SERVICE_ABOUT,
                    ((ServiceGroupAboutChanged) msg).getNewAbout());
        } else if (msg instanceof ServiceGroupCreated) {
            return new ContentDescription(ContentType.SERVICE_CREATED);
        } else if (msg instanceof ServiceGroupUserInvited) {
            return new ContentDescription(ContentType.SERVICE_ADD, "",
                    ((ServiceGroupUserInvited) msg).getAddedUid(), false);
        } else if (msg instanceof ServiceGroupUserKicked) {
            return new ContentDescription(ContentType.SERVICE_KICK, "",
                    ((ServiceGroupUserKicked) msg).getKickedUid(), false);
        } else if (msg instanceof ServiceGroupUserLeave) {
            return new ContentDescription(ContentType.SERVICE_LEAVE, "",
                    0, true);
        } else if (msg instanceof ServiceGroupUserJoined) {
            return new ContentDescription(ContentType.SERVICE_JOINED, "",
                    0, false);
        } else if (msg instanceof ServiceContent) {
            return new ContentDescription(ContentType.SERVICE,
                    ((ServiceContent) msg).getCompatText(), 0, false);
        } else if (msg instanceof JsonContent) {
            return new ContentDescription(ContentType.TEXT, ((JsonContent) msg).getContentDescription());
        } else {
            return new ContentDescription(ContentType.UNKNOWN_CONTENT);
        }
    }

    @Property("readonly, nonatomic")
    private ContentType contentType;
    @Property("readonly, nonatomic")
    private String text;
    @Property("readonly, nonatomic")
    private int relatedUser;
    @Property("readonly, nonatomic")
    private boolean isSilent;

    public ContentDescription(ContentType contentType, String text, int relatedUser,
                              boolean isSilent) {
        this.contentType = contentType;
        this.text = text;
        this.relatedUser = relatedUser;
        this.isSilent = isSilent;
    }

    public ContentDescription(ContentType contentType, String text) {
        this(contentType, text, 0, false);
    }

    public ContentDescription(ContentType contentType) {
        this(contentType, "", 0, false);
    }

    private ContentDescription() {

    }

    public ContentType getContentType() {
        return contentType;
    }

    public String getText() {
        return text;
    }

    public int getRelatedUser() {
        return relatedUser;
    }

    public boolean isSilent() {
        return isSilent;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        contentType = ContentType.fromValue(values.getInt(1));
        text = values.getString(2);
        relatedUser = values.getInt(3);
        isSilent = values.getBool(4);
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        writer.writeInt(1, contentType.getValue());
        writer.writeString(2, text);
        writer.writeInt(3, relatedUser);
        writer.writeBool(4, isSilent);
    }
}
