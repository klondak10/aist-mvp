/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity;

import im.aist.core.api.ApiFastThumb;
import im.aist.core.api.ApiGroup;
import im.aist.core.api.ApiMessageState;
import im.aist.core.api.ApiPeer;
import im.aist.core.api.ApiPeerType;
import im.aist.core.entity.content.FastThumb;

public class EntityConverter {

    public static MessageState convert(ApiMessageState state) {
        if (state == null) {
            return MessageState.UNKNOWN;
        }
        switch (state) {
            case READ:
            case RECEIVED:
            case SENT:
                return MessageState.SENT;
            default:
                return MessageState.UNKNOWN;
        }
    }

    public static Group convert(ApiGroup group) {
        return new Group(group, null);
    }

    public static PeerType convert(ApiPeerType peerType) {
        switch (peerType) {
            case GROUP:
                return PeerType.GROUP;
            default:
            case PRIVATE:
                return PeerType.PRIVATE;
        }
    }

    public static Peer convert(ApiPeer peer) {
        return new Peer(convert(peer.getType()), peer.getId());
    }


    public static FastThumb convert(ApiFastThumb fastThumb) {
        if (fastThumb == null) {
            return null;
        }
        return new FastThumb(fastThumb.getW(), fastThumb.getH(), fastThumb.getThumb());
    }
}
