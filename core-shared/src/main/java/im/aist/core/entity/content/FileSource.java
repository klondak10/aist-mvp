/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.entity.content;

public abstract class FileSource {

    public abstract int getSize();

    public abstract String getFileName();
}
