/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core;

/**
 * Application type
 */
public enum PlatformType {
    GENERIC, ANDROID, IOS, WEB
}
