package im.aist.core;

public enum AutoJoinType {
    AFTER_INIT,
    IMMEDIATELY
}
