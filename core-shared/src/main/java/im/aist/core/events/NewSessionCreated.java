package im.aist.core.events;

import im.aist.runtime.eventbus.Event;

public class NewSessionCreated extends Event {

    public static final String EVENT = "new_session";

    @Override
    public String getType() {
        return EVENT;
    }
}
