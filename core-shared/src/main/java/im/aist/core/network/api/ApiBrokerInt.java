package im.aist.core.network.api;

import im.aist.core.network.ActorApiCallback;
import im.aist.core.network.AuthKeyStorage;
import im.aist.core.network.Endpoints;
import im.aist.runtime.actors.ActorInterface;
import im.aist.runtime.promise.Promise;

import static im.aist.runtime.actors.ActorSystem.system;

public class ApiBrokerInt extends ActorInterface {
    public ApiBrokerInt(final Endpoints endpoints, final AuthKeyStorage keyStorage, final ActorApiCallback callback,
                        final boolean isEnableLog, int id, final int minDelay,
                        final int maxDelay,
                        final int maxFailureCount) {
        setDest(system().actorOf("api/broker#" + id, () -> new ApiBroker(endpoints,
                keyStorage,
                callback,
                isEnableLog,
                minDelay,
                maxDelay,
                maxFailureCount)));
    }

    public Promise<Boolean> checkIsCurrentAuthId(long authId) {
        return ask(new CheckIsCurrentAuthId(authId));
    }


}
