/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.network;

import com.google.j2objc.annotations.ObjectiveCName;

import im.aist.core.network.parser.Response;

public interface RpcCallback<T extends Response> {
    @ObjectiveCName("onResult:")
    void onResult(T response);

    @ObjectiveCName("onError:")
    void onError(RpcException e);
}
