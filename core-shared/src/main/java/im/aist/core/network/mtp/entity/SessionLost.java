/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.network.mtp.entity;

import java.io.IOException;

import im.aist.runtime.bser.DataInput;
import im.aist.runtime.bser.DataOutput;

// Disabling Bounds checks for speeding up calculations

/*-[
#define J2OBJC_DISABLE_ARRAY_BOUND_CHECKS 1
]-*/

public class SessionLost extends ProtoStruct {
    public static final int HEADER = 0x10;

    public SessionLost(DataInput stream) throws IOException {
        super(stream);
    }

    public SessionLost() {
    }

    @Override
    protected byte getHeader() {
        return HEADER;
    }

    @Override
    protected void writeBody(DataOutput bs) throws IOException {

    }

    @Override
    protected void readBody(DataInput bs) throws IOException {

    }
}
