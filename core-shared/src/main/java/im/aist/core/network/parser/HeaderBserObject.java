/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.network.parser;

import im.aist.runtime.bser.BserObject;

public abstract class HeaderBserObject extends BserObject {
    public abstract int getHeaderKey();
}
