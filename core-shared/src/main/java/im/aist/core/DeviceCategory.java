/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core;

/**
 * Current device type
 */
public enum DeviceCategory {
    UNKNOWN, DESKTOP, TABLET, MOBILE
}