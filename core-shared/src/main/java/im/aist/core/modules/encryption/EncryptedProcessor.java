package im.aist.core.modules.encryption;

import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.sequence.processor.SequenceProcessor;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

public class EncryptedProcessor extends AbsModule implements SequenceProcessor {

    public EncryptedProcessor(ModuleContext context) {
        super(context);
    }

    @Override
    public Promise<Void> process(Update update) {
//        if (update instanceof UpdatePublicKeyGroupAdded) {
//            context().getEncryption().getKeyManager().send(new KeyManagerActor.PublicKeysGroupAdded(
//                    ((UpdatePublicKeyGroupAdded) update).getUid(),
//                    ((UpdatePublicKeyGroupAdded) update).getKeyGroup()
//            ));
//            return true;
//        } else if (update instanceof UpdatePublicKeyGroupRemoved) {
//            context().getEncryption().getKeyManager().send(new KeyManagerActor.PublicKeysGroupRemoved(
//                    ((UpdatePublicKeyGroupRemoved) update).getUid(),
//                    ((UpdatePublicKeyGroupRemoved) update).getKeyGroupId()
//            ));
//            return true;
//        } else if (update instanceof UpdateEncryptedPackage) {
//
//        }
        return null;
    }
}
