/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.groups;

import im.aist.core.api.updates.UpdateGroupAboutChanged;
import im.aist.core.api.updates.UpdateGroupAvatarChanged;
import im.aist.core.api.updates.UpdateGroupDeleted;
import im.aist.core.api.updates.UpdateGroupExtChanged;
import im.aist.core.api.updates.UpdateGroupFullExtChanged;
import im.aist.core.api.updates.UpdateGroupFullPermissionsChanged;
import im.aist.core.api.updates.UpdateGroupHistoryShared;
import im.aist.core.api.updates.UpdateGroupMemberAdminChanged;
import im.aist.core.api.updates.UpdateGroupMemberChanged;
import im.aist.core.api.updates.UpdateGroupMemberDiff;
import im.aist.core.api.updates.UpdateGroupMembersBecameAsync;
import im.aist.core.api.updates.UpdateGroupMembersCountChanged;
import im.aist.core.api.updates.UpdateGroupMembersUpdated;
import im.aist.core.api.updates.UpdateGroupOwnerChanged;
import im.aist.core.api.updates.UpdateGroupPermissionsChanged;
import im.aist.core.api.updates.UpdateGroupShortNameChanged;
import im.aist.core.api.updates.UpdateGroupTitleChanged;
import im.aist.core.api.updates.UpdateGroupTopicChanged;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.sequence.processor.SequenceProcessor;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

public class GroupsProcessor extends AbsModule implements SequenceProcessor {

    public GroupsProcessor(ModuleContext context) {
        super(context);
    }

    @Override
    public Promise<Void> process(Update update) {
        if (update instanceof UpdateGroupTitleChanged ||
                update instanceof UpdateGroupMemberChanged ||
                update instanceof UpdateGroupAvatarChanged ||
                update instanceof UpdateGroupPermissionsChanged ||
                update instanceof UpdateGroupDeleted ||
                update instanceof UpdateGroupExtChanged ||

                update instanceof UpdateGroupMembersUpdated ||
                update instanceof UpdateGroupMemberAdminChanged ||
                update instanceof UpdateGroupMemberDiff ||
                update instanceof UpdateGroupMembersBecameAsync ||
                update instanceof UpdateGroupMembersCountChanged ||

                update instanceof UpdateGroupShortNameChanged ||
                update instanceof UpdateGroupAboutChanged ||
                update instanceof UpdateGroupTopicChanged ||
                update instanceof UpdateGroupOwnerChanged ||
                update instanceof UpdateGroupHistoryShared ||
                update instanceof UpdateGroupFullPermissionsChanged ||
                update instanceof UpdateGroupFullExtChanged) {
            return context().getGroupsModule().getRouter().onUpdate(update);
        }
        return null;
    }
}