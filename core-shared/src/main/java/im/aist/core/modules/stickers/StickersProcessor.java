/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.stickers;

import java.util.List;

import im.aist.core.api.ApiStickerCollection;
import im.aist.core.api.updates.UpdateOwnStickersChanged;
import im.aist.core.api.updates.UpdateStickerCollectionsChanged;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.sequence.processor.SequenceProcessor;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

public class StickersProcessor extends AbsModule implements SequenceProcessor {

    public StickersProcessor(ModuleContext modules) {
        super(modules);
    }

    public void onOwnStickerCollectionsChanged(List<ApiStickerCollection> updated) {
        stickersActor().send(new StickersActor.OwnStickerCollectionsChanged(updated));
    }

    public void onStickerCollectionsChanged(List<ApiStickerCollection> updated) {
        stickersActor().send(new StickersActor.StickerCollectionsChanged(updated));
    }

    @Override
    public Promise<Void> process(Update update) {
        if (update instanceof UpdateOwnStickersChanged) {
            onOwnStickerCollectionsChanged(((UpdateOwnStickersChanged) update).getCollections());
            return Promise.success(null);
        } else if (update instanceof UpdateStickerCollectionsChanged) {
            onStickerCollectionsChanged(((UpdateStickerCollectionsChanged) update).getCollections());
            return Promise.success(null);
        }
        return null;
    }
}
