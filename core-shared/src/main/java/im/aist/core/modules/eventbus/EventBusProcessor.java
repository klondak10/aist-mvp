package im.aist.core.modules.eventbus;

import im.aist.core.api.updates.UpdateEventBusDeviceConnected;
import im.aist.core.api.updates.UpdateEventBusDeviceDisconnected;
import im.aist.core.api.updates.UpdateEventBusDisposed;
import im.aist.core.api.updates.UpdateEventBusMessage;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.sequence.processor.WeakProcessor;
import im.aist.core.network.parser.Update;

public class EventBusProcessor implements WeakProcessor {

    private ModuleContext context;

    public EventBusProcessor(ModuleContext context) {
        this.context = context;
    }

    @Override
    public boolean process(Update update, long date) {
        if (update instanceof UpdateEventBusMessage ||
                update instanceof UpdateEventBusDeviceConnected ||
                update instanceof UpdateEventBusDeviceDisconnected ||
                update instanceof UpdateEventBusDisposed) {
            context.getEventBus().onEventBusUpdate(update);
            return true;
        }

        return false;
    }
}