package im.aist.core.modules.sequence.processor;

import im.aist.core.network.parser.Update;

public interface WeakProcessor {
    boolean process(Update update, long date);
}
