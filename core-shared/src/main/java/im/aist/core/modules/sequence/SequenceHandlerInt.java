package im.aist.core.modules.sequence;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import im.aist.core.api.ApiGroup;
import im.aist.core.api.ApiGroupOutPeer;
import im.aist.core.api.ApiUser;
import im.aist.core.api.ApiUserOutPeer;
import im.aist.core.modules.sequence.internal.HandlerDifferenceUpdates;
import im.aist.core.modules.sequence.internal.HandlerSeqUpdate;
import im.aist.core.modules.sequence.internal.HandlerWeakUpdate;
import im.aist.core.modules.sequence.internal.HandlerRelatedResponse;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.ActorInterface;
import im.aist.runtime.actors.ActorRef;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

public class SequenceHandlerInt extends ActorInterface {

    public SequenceHandlerInt(ActorRef dest) {
        super(dest);
    }

    public Promise<Void> onSeqUpdate(Update update,
                                     @Nullable List<ApiUser> users,
                                     @Nullable List<ApiGroup> groups) {
        return ask(new HandlerSeqUpdate(update, users, groups));
    }

    public Promise<Void> onDifferenceUpdate(@NotNull List<ApiUser> users,
                                            @NotNull List<ApiGroup> groups,
                                            @NotNull List<ApiUserOutPeer> userOutPeers,
                                            @NotNull List<ApiGroupOutPeer> groupOutPeers,
                                            @NotNull List<Update> updates) {
        return ask(new HandlerDifferenceUpdates(users, groups, userOutPeers, groupOutPeers, updates));
    }

    public Promise<Void> onRelatedResponse(@NotNull List<ApiUser> users,
                                           @NotNull List<ApiGroup> groups) {
        return ask(new HandlerRelatedResponse(users, groups));
    }

    public void onWeakUpdate(Update update, long date) {
        send(new HandlerWeakUpdate(update, date));
    }
}
