package im.aist.core.modules.messaging.router.entity;

import java.util.List;

import im.aist.core.entity.Group;
import im.aist.core.entity.User;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterPeersChanged implements AskMessage<Void> {

    private List<User> users;
    private List<Group> groups;

    public RouterPeersChanged(List<User> users, List<Group> groups) {
        this.users = users;
        this.groups = groups;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Group> getGroups() {
        return groups;
    }
}
