/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.messaging.history;

import im.aist.core.api.rpc.RequestLoadArchived;
import im.aist.core.api.rpc.ResponseLoadArchived;
import im.aist.core.modules.api.ApiSupportConfiguration;
import im.aist.core.modules.ModuleContext;
import im.aist.core.network.RpcCallback;
import im.aist.core.network.RpcException;
import im.aist.core.modules.ModuleActor;
import im.aist.runtime.Log;

public class ArchivedDialogsActor extends ModuleActor {

    private static final String TAG = "ArchivedDialogsActor";

    private static final int LIMIT = 20;


    private byte[] nextOffset;

    private boolean isLoading = false;

    RpcCallback<ResponseLoadArchived> lastCallback;
    private long lastRequest = -1;

    public ArchivedDialogsActor(ModuleContext context) {
        super(context);
    }

    private void onLoadMore(boolean init, RpcCallback<ResponseLoadArchived> callback) {

        if (init || isLoading) {

            //
            // notify old callback replaced
            //
            if (lastCallback != null) {
                lastCallback.onError(new RpcException(TAG, 0, "callback replaced", false, null));
            }
        }
        lastCallback = callback;

        if (isLoading && !init) {
            return;
        }

        if (init) {
            if (lastRequest != -1) {
                cancelRequest(lastRequest);
            }
            nextOffset = null;
        }

        isLoading = true;

        Log.d(TAG, "Loading archived dialogs");
        api(new RequestLoadArchived(nextOffset, LIMIT, ApiSupportConfiguration.OPTIMIZATIONS))
                .chain(r -> updates().applyRelatedData(r.getUsers(), r.getGroups()))
                .chain(r -> updates().loadRequiredPeers(r.getUserPeers(), r.getGroupPeers()))
                .then(r -> onLoadedMore(r))
                .failure(e -> lastCallback.onError((RpcException) e));
    }

    private void onLoadedMore(ResponseLoadArchived responseLoadArchiveds) {
        isLoading = false;

        this.nextOffset = responseLoadArchiveds.getNextOffset();
        lastCallback.onResult(responseLoadArchiveds);
        Log.d(TAG, "Archived dialogs loaded");
    }

    // Messages

    @Override
    public void onReceive(Object message) {
        if (message instanceof LoadMore) {
            onLoadMore(((LoadMore) message).isInit(), ((LoadMore) message).getCallback());
        } else {
            super.onReceive(message);
        }
    }

    public static class LoadMore {
        RpcCallback<ResponseLoadArchived> callback;
        boolean init;

        public LoadMore(boolean init, RpcCallback<ResponseLoadArchived> callback) {
            this.callback = callback;
            this.init = init;
        }

        public boolean isInit() {
            return init;
        }

        public RpcCallback<ResponseLoadArchived> getCallback() {
            return callback;
        }
    }
}
