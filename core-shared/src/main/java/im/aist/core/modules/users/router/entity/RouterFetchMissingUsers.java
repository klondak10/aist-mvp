package im.aist.core.modules.users.router.entity;

import java.util.List;

import im.aist.core.api.ApiUserOutPeer;
import im.aist.runtime.actors.ask.AskMessage;

public class RouterFetchMissingUsers implements AskMessage<List<ApiUserOutPeer>> {

    private List<ApiUserOutPeer> sourcePeers;

    public RouterFetchMissingUsers(List<ApiUserOutPeer> sourcePeers) {
        this.sourcePeers = sourcePeers;
    }

    public List<ApiUserOutPeer> getSourcePeers() {
        return sourcePeers;
    }
}
