package im.aist.core.modules.messaging.router.entity;

import im.aist.core.entity.Message;
import im.aist.core.entity.Peer;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterOutgoingMessage implements AskMessage<Void>, RouterMessageOnlyActive {

    private Peer peer;
    private Message message;

    public RouterOutgoingMessage(Peer peer, Message message) {
        this.peer = peer;
        this.message = message;
    }

    public Peer getPeer() {
        return peer;
    }

    public Message getMessage() {
        return message;
    }
}
