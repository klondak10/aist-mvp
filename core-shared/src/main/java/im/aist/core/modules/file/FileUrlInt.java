package im.aist.core.modules.file;

import org.jetbrains.annotations.NotNull;

import im.aist.runtime.actors.ActorInterface;
import im.aist.runtime.actors.ActorRef;
import im.aist.runtime.promise.Promise;

public class FileUrlInt extends ActorInterface {

    public FileUrlInt(@NotNull ActorRef dest) {
        super(dest);
    }

    public Promise<String> askForUrl(long fileId, long accessHash) {
        return ask(new FileUrlLoader.AskUrl(fileId, accessHash));
    }
}
