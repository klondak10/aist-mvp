package im.aist.core.modules.messaging.router.entity;

import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterMessageUpdate implements AskMessage<Void>, RouterMessageOnlyActive {

    private Update update;

    public RouterMessageUpdate(Update update) {
        this.update = update;
    }

    public Update getUpdate() {
        return update;
    }
}
