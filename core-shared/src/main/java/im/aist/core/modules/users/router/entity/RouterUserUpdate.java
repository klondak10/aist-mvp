package im.aist.core.modules.users.router.entity;

import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterUserUpdate implements AskMessage<Void> {

    private Update update;

    public RouterUserUpdate(Update update) {
        this.update = update;
    }

    public Update getUpdate() {
        return update;
    }
}
