package im.aist.core.modules.messaging.router.entity;

import im.aist.core.entity.Peer;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterOutgoingError implements AskMessage<Void>, RouterMessageOnlyActive {

    private Peer peer;
    private long rid;

    public RouterOutgoingError(Peer peer, long rid) {
        this.peer = peer;
        this.rid = rid;
    }

    public Peer getPeer() {
        return peer;
    }

    public long getRid() {
        return rid;
    }
}
