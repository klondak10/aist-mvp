package im.aist.core.modules.search.sources;

import java.util.ArrayList;
import java.util.List;

import im.aist.core.entity.Group;
import im.aist.core.entity.PeerSearchEntity;
import im.aist.core.entity.PeerType;
import im.aist.core.entity.SearchEntity;
import im.aist.core.entity.SearchResult;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.viewmodel.UserVM;
import im.aist.runtime.function.Consumer;
import im.aist.runtime.mvvm.SearchValueSource;
import im.aist.runtime.storage.ListEngine;
import im.aist.runtime.storage.ListEngineDisplayExt;

public class GlobalSearchSource extends AbsModule implements SearchValueSource<SearchResult> {

    public GlobalSearchSource(ModuleContext context) {
        super(context);
    }

    @Override
    public void loadResults(String query, Consumer<List<SearchResult>> callback) {
        ListEngine<SearchEntity> searchList = context().getSearchModule().getSearchList();
        if (searchList instanceof ListEngineDisplayExt) {
            ((ListEngineDisplayExt<SearchEntity>) searchList).loadBackward(query, 20, (items, topSortKey, bottomSortKey) -> {
                ArrayList<SearchResult> localResults = new ArrayList<>();
                for (SearchEntity e : items) {
                    localResults.add(new SearchResult(e.getPeer(), e.getAvatar(), e.getTitle(),
                            null));
                }
                callback.apply(new ArrayList<>(localResults));
                if (query.length() > 3) {
                    loadGlobalResults(query, localResults, callback);
                }
            });
        } else {
            if (query.length() > 3) {
                loadGlobalResults(query, new ArrayList<>(), callback);
            } else {
                callback.apply(new ArrayList<>());
            }
        }
    }


    private void loadGlobalResults(String query, ArrayList<SearchResult> localResults, Consumer<List<SearchResult>> callback) {
        context().getSearchModule().findPeers(query).then(r -> {
            ArrayList<SearchResult> results = new ArrayList<>();
            outer:
            for (PeerSearchEntity peerSearch : r) {
                for (SearchResult l : localResults) {
                    if (peerSearch.getPeer().equals(l.getPeer())) {
                        continue outer;
                    }
                }
                if (peerSearch.getPeer().getPeerType() == PeerType.GROUP) {
                    Group group = context().getGroupsModule().getGroups().getValue(peerSearch.getPeer().getPeerId());
                    results.add(new SearchResult(peerSearch.getPeer(), group.getAvatar(), group.getTitle(),
                            peerSearch.getOptMatchString()));
                } else if (peerSearch.getPeer().getPeerType() == PeerType.PRIVATE) {
                    UserVM user = context().getUsersModule().getUsers().get(peerSearch.getPeer().getPeerId());
                    results.add(new SearchResult(peerSearch.getPeer(), user.getAvatar().get(), user.getName().get(),
                            peerSearch.getOptMatchString()));
                }
            }
            if (results.size() > 0) {
                ArrayList<SearchResult> combined = new ArrayList<>();
                combined.addAll(localResults);
                combined.addAll(results);
                callback.apply(combined);
            }
        });
    }
}
