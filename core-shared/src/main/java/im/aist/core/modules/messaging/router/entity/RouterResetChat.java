package im.aist.core.modules.messaging.router.entity;

import im.aist.core.entity.Peer;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterResetChat implements AskMessage<Void> {
    
    private Peer peer;

    public RouterResetChat(Peer peer) {
        this.peer = peer;
    }

    public Peer getPeer() {
        return peer;
    }
}
