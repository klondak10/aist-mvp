package im.aist.core.modules.encryption;

import im.aist.core.entity.encryption.PeerSession;
import im.aist.runtime.actors.ActorInterface;
import im.aist.runtime.actors.ActorRef;
import im.aist.runtime.promise.Promise;

public class SessionManagerInt extends ActorInterface {

    public SessionManagerInt(ActorRef dest) {
        super(dest);
    }

    public Promise<PeerSession> pickSession(int uid, int keyGroup) {
        return ask(new SessionManagerActor.PickSessionForEncrypt(uid, keyGroup));
    }

    public Promise<PeerSession> pickSession(int uid, int keyGroup, long ownKeyId, long theirKeyId) {
        return ask(new SessionManagerActor.PickSessionForDecrypt(uid, keyGroup, theirKeyId, ownKeyId));
    }
}
