package im.aist.core.modules.messaging.dialogs.entity;

import im.aist.core.entity.User;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class UserChanged implements AskMessage<Void> {

    private User user;

    public UserChanged(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
