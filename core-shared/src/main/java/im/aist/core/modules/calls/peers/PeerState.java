package im.aist.core.modules.calls.peers;

public enum PeerState {
    PENDING, CONNECTING, CONNECTED, ACTIVE, DISPOSED
}