/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.typing;

import java.util.HashMap;

import im.aist.core.entity.Peer;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.viewmodel.GroupTypingVM;
import im.aist.core.viewmodel.UserTypingVM;
import im.aist.runtime.actors.ActorRef;

public class TypingModule extends AbsModule {

    private ActorRef ownTypingActor;
    private ActorRef typingActor;
    private HashMap<Integer, UserTypingVM> uids = new HashMap<>();
    private HashMap<Integer, GroupTypingVM> groups = new HashMap<>();

    public TypingModule(final ModuleContext context) {
        super(context);

        this.ownTypingActor = OwnTypingActor.get(context);
        this.typingActor = TypingActor.get(context);
    }

    public GroupTypingVM getGroupTyping(int gid) {
        synchronized (groups) {
            if (!groups.containsKey(gid)) {
                groups.put(gid, new GroupTypingVM(gid));
            }
            return groups.get(gid);
        }
    }

    public UserTypingVM getTyping(int uid) {
        synchronized (uids) {
            if (!uids.containsKey(uid)) {
                uids.put(uid, new UserTypingVM(uid));
            }
            return uids.get(uid);
        }
    }

    public void onTyping(Peer peer) {
        ownTypingActor.send(new OwnTypingActor.Typing(peer));
    }

    public void onMessageSent(Peer peer) {
        ownTypingActor.send(new OwnTypingActor.MessageSent(peer));
    }

    public void resetModule() {
        // TODO: Implement
    }
}
