/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.presence;

import im.aist.core.modules.AbsModule;
import im.aist.core.modules.Modules;

import static im.aist.runtime.actors.ActorSystem.system;

public class PresenceModule extends AbsModule {

    public PresenceModule(final Modules modules) {
        super(modules);

        // Creating own presence actor
        system().actorOf("actor/presence/own", () -> new OwnPresenceActor(modules));

        // Creating users and groups presence actor
        PresenceActor.create(modules);
    }
}