/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.push;

import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.runtime.actors.ActorRef;

import static im.aist.runtime.actors.ActorSystem.system;

public class PushesModule extends AbsModule {

    private ActorRef pushRegisterActor;

    public PushesModule(ModuleContext modules) {
        super(modules);

        pushRegisterActor = system().actorOf("actor/push", () -> new PushRegisterActor(context()));
    }

    public void registerGooglePush(long projectId, String token) {
        pushRegisterActor.send(new PushRegisterActor.RegisterGooglePush(projectId, token));
    }

    public void registerApplePush(int apnsKey, String token) {
        pushRegisterActor.send(new PushRegisterActor.RegisterApplePush(apnsKey, token));
    }

    public void registerApplePushKit(int apnsKey, String token) {
        pushRegisterActor.send(new PushRegisterActor.RegisterAppleVoipPush(apnsKey, token));
    }

    public void registerActorPush(String endpoint) {
        pushRegisterActor.send(new PushRegisterActor.RegisterActorPush(endpoint));
    }
}
