package im.aist.core.modules.groups.router.entity;

import java.util.List;

import im.aist.core.api.ApiGroup;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterApplyGroups implements AskMessage<Void> {
    
    private List<ApiGroup> groups;

    public RouterApplyGroups(List<ApiGroup> groups) {
        this.groups = groups;
    }

    public List<ApiGroup> getGroups() {
        return groups;
    }
}
