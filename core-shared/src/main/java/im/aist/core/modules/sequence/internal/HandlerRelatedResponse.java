package im.aist.core.modules.sequence.internal;

import java.util.List;

import im.aist.core.api.ApiGroup;
import im.aist.core.api.ApiUser;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class HandlerRelatedResponse implements AskMessage<Void> {

    private List<ApiUser> relatedUsers;
    private List<ApiGroup> relatedGroups;

    public HandlerRelatedResponse(List<ApiUser> relatedUsers, List<ApiGroup> relatedGroups) {
        this.relatedUsers = relatedUsers;
        this.relatedGroups = relatedGroups;
    }

    public List<ApiUser> getRelatedUsers() {
        return relatedUsers;
    }

    public List<ApiGroup> getRelatedGroups() {
        return relatedGroups;
    }
}
