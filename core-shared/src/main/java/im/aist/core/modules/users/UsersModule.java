/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.users;

import java.util.ArrayList;
import java.util.List;

import im.aist.core.api.ApiUserOutPeer;
import im.aist.core.api.rpc.RequestBlockUser;
import im.aist.core.api.rpc.RequestEditAbout;
import im.aist.core.api.rpc.RequestEditName;
import im.aist.core.api.rpc.RequestEditNickName;
import im.aist.core.api.rpc.RequestEditUserLocalName;
import im.aist.core.api.rpc.RequestLoadBlockedUsers;
import im.aist.core.api.rpc.RequestUnblockUser;
import im.aist.core.api.updates.UpdateUserAboutChanged;
import im.aist.core.api.updates.UpdateUserBlocked;
import im.aist.core.api.updates.UpdateUserLocalNameChanged;
import im.aist.core.api.updates.UpdateUserNameChanged;
import im.aist.core.api.updates.UpdateUserNickChanged;
import im.aist.core.api.updates.UpdateUserUnblocked;
import im.aist.core.entity.Peer;
import im.aist.core.entity.PeerType;
import im.aist.core.entity.User;
import im.aist.core.events.PeerChatOpened;
import im.aist.core.events.PeerInfoOpened;
import im.aist.core.events.UserVisible;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.users.router.UserRouterInt;
import im.aist.core.viewmodel.UserVM;
import im.aist.runtime.Storage;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.eventbus.BusSubscriber;
import im.aist.runtime.eventbus.Event;
import im.aist.runtime.mvvm.MVVMCollection;
import im.aist.runtime.promise.Promise;
import im.aist.runtime.promise.PromisesArray;
import im.aist.runtime.storage.KeyValueEngine;

public class UsersModule extends AbsModule implements BusSubscriber {

    private UserRouterInt userRouter;
    private KeyValueEngine<User> users;
    private MVVMCollection<User, UserVM> collection;

    public UsersModule(ModuleContext context) {
        super(context);

        this.collection = Storage.createKeyValue(STORAGE_USERS, UserVM.CREATOR(context()), User.CREATOR);
        this.users = collection.getEngine();

        this.userRouter = new UserRouterInt(context);
    }

    public void run() {
        context().getEvents().subscribe(this, PeerChatOpened.EVENT);
        context().getEvents().subscribe(this, UserVisible.EVENT);
        context().getEvents().subscribe(this, PeerInfoOpened.EVENT);
    }

    // Model

    public KeyValueEngine<User> getUsersStorage() {
        return users;
    }

    public MVVMCollection<User, UserVM> getUsers() {
        return collection;
    }

    public UserRouterInt getUserRouter() {
        return userRouter;
    }

    // Actions

    public Promise<Void> editName(final int uid, final String name) {
        return getUsersStorage()
                .getValueAsync(uid)
                .map(User::getAccessHash)
                .flatMap(aLong -> api(new RequestEditUserLocalName(uid, aLong, name)))
                .flatMap(responseSeq -> updates().applyUpdate(
                        responseSeq.getSeq(),
                        responseSeq.getState(),
                        new UpdateUserLocalNameChanged(uid, name))
                );
    }

    public Promise<Void> editMyName(final String newName) {
        return api(new RequestEditName(newName))
                .flatMap(responseSeq -> updates().applyUpdate(
                        responseSeq.getSeq(),
                        responseSeq.getState(),
                        new UpdateUserNameChanged(myUid(), newName))
                );
    }

    public Promise<Void> editNick(final String nick) {
        return api(new RequestEditNickName(nick))
                .flatMap(responseSeq -> updates().applyUpdate(
                        responseSeq.getSeq(),
                        responseSeq.getState(),
                        new UpdateUserNickChanged(myUid(), nick))
                );
    }

    public Promise<Void> editAbout(final String about) {
        return api(new RequestEditAbout(about))
                .flatMap(responseSeq -> updates().applyUpdate(
                        responseSeq.getSeq(),
                        responseSeq.getState(),
                        new UpdateUserAboutChanged(myUid(), about))
                );
    }

    public Promise<List<User>> loadBlockedUsers() {
        return api(new RequestLoadBlockedUsers())
                .chain(response -> updates().loadRequiredPeers(response.getUserPeers(), new ArrayList<>()))
                .flatMap(responseLoadBlockedUsers ->
                        PromisesArray.of(responseLoadBlockedUsers.getUserPeers())
                                .map(apiUserOutPeer -> users().getValueAsync(apiUserOutPeer.getUid()))
                                .zip());
    }

    public Promise<Void> blockUser(final int uid) {
        return buildOutPeer(Peer.user(uid))
                .flatMap(apiOutPeer ->
                        api(new RequestBlockUser(new ApiUserOutPeer(apiOutPeer.getId(), apiOutPeer.getAccessHash()))))
                .flatMap(responseSeq ->
                        updates().applyUpdate(
                                responseSeq.getSeq(),
                                responseSeq.getState(),
                                new UpdateUserBlocked(uid)));
    }

    public Promise<Void> unblockUser(final int uid) {
        return buildOutPeer(Peer.user(uid))
                .flatMap(apiOutPeer ->
                        api(new RequestUnblockUser(new ApiUserOutPeer(apiOutPeer.getId(), apiOutPeer.getAccessHash()))))
                .flatMap(responseSeq ->
                        updates().applyUpdate(
                                responseSeq.getSeq(),
                                responseSeq.getState(),
                                new UpdateUserUnblocked(uid)));
    }

    public void resetModule() {
        users.clear();
    }

    @Override
    public void onBusEvent(Event event) {
        if (event instanceof PeerChatOpened) {
            Peer peer = ((PeerChatOpened) event).getPeer();
            if (peer.getPeerType() == PeerType.PRIVATE) {
                getUserRouter().onFullUserNeeded(peer.getPeerId());
            }
        } else if (event instanceof UserVisible) {
            getUserRouter().onFullUserNeeded(((UserVisible) event).getUid());
        } else if (event instanceof PeerInfoOpened) {
            Peer peer = ((PeerInfoOpened) event).getPeer();
            if (peer.getPeerType() == PeerType.PRIVATE) {
                getUserRouter().onFullUserNeeded(peer.getPeerId());
            }
        }
    }
}