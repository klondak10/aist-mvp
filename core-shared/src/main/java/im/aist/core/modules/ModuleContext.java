package im.aist.core.modules;

import im.aist.core.Configuration;
import im.aist.core.Messenger;
import im.aist.core.i18n.I18nEngine;
import im.aist.core.modules.api.ApiModule;
import im.aist.core.modules.auth.Authentication;
import im.aist.core.modules.eventbus.EventBusModule;
import im.aist.core.modules.sequence.Updates;
import im.aist.core.modules.calls.CallsModule;
import im.aist.core.modules.contacts.ContactsModule;
import im.aist.core.modules.conductor.DisplayLists;
import im.aist.core.modules.encryption.EncryptionModule;
import im.aist.core.modules.external.ExternalModule;
import im.aist.core.modules.file.FilesModule;
import im.aist.core.modules.groups.GroupsModule;
import im.aist.core.modules.mentions.MentionsModule;
import im.aist.core.modules.messaging.MessagesModule;
import im.aist.core.modules.notifications.NotificationsModule;
import im.aist.core.modules.presence.PresenceModule;
import im.aist.core.modules.profile.ProfileModule;
import im.aist.core.modules.push.PushesModule;
import im.aist.core.modules.search.SearchModule;
import im.aist.core.modules.security.SecurityModule;
import im.aist.core.modules.settings.SettingsModule;
import im.aist.core.modules.stickers.StickersModule;
import im.aist.core.modules.storage.StorageModule;
import im.aist.core.modules.typing.TypingModule;
import im.aist.core.modules.users.UsersModule;
import im.aist.core.modules.conductor.ConductorModule;
import im.aist.core.network.ActorApi;
import im.aist.runtime.eventbus.EventBus;
import im.aist.runtime.storage.PreferencesStorage;

public interface ModuleContext {

    // Messenger Configuration
    Configuration getConfiguration();

    // API Access
    ActorApi getActorApi();

    ApiModule getApiModule();

    StorageModule getStorageModule();

    void afterStorageReset();

    // Preferences
    PreferencesStorage getPreferences();

    // Built-In modules
    Authentication getAuthModule();

    // Event Bus
    EventBus getEvents();

    UsersModule getUsersModule();

    GroupsModule getGroupsModule();

    StickersModule getStickersModule();

    CallsModule getCallsModule();

    MessagesModule getMessagesModule();

    Updates getUpdatesModule();

    TypingModule getTypingModule();

    PresenceModule getPresenceModule();

    I18nEngine getI18nModule();

    ContactsModule getContactsModule();

    FilesModule getFilesModule();

    NotificationsModule getNotificationsModule();

    SettingsModule getSettingsModule();

    ProfileModule getProfileModule();

    PushesModule getPushesModule();

    SecurityModule getSecurityModule();

    SearchModule getSearchModule();

    ExternalModule getExternalModule();

    DisplayLists getDisplayListsModule();

    Messenger getMessenger();

    MentionsModule getMentions();

    EncryptionModule getEncryption();

    EventBusModule getEventBus();

    ConductorModule getConductor();
}
