package im.aist.core.modules.messaging.router.entity;

import java.util.List;

import im.aist.core.modules.messaging.history.entity.DialogHistory;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterApplyDialogsHistory implements AskMessage<Void>, RouterMessageOnlyActive {

    private List<DialogHistory> dialogs;

    public RouterApplyDialogsHistory(List<DialogHistory> dialogs) {
        this.dialogs = dialogs;
    }

    public List<DialogHistory> getDialogs() {
        return dialogs;
    }

}
