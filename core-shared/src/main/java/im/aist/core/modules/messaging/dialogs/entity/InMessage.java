package im.aist.core.modules.messaging.dialogs.entity;

import im.aist.core.entity.Message;
import im.aist.core.entity.Peer;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class InMessage implements AskMessage<Void> {

    private Peer peer;
    private Message message;
    private int counter;

    public InMessage(Peer peer, Message message, int counter) {
        this.peer = peer;
        this.message = message;
        this.counter = counter;
    }

    public Peer getPeer() {
        return peer;
    }

    public Message getMessage() {
        return message;
    }

    public int getCounter() {
        return counter;
    }
}
