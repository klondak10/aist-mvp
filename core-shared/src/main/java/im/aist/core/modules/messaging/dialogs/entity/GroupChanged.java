package im.aist.core.modules.messaging.dialogs.entity;

import im.aist.core.entity.Group;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class GroupChanged implements AskMessage<Void> {

    private Group group;

    public GroupChanged(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }
}
