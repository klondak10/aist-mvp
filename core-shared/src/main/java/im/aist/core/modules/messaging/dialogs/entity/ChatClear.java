package im.aist.core.modules.messaging.dialogs.entity;

import im.aist.core.entity.Peer;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class ChatClear implements AskMessage<Void> {

    private Peer peer;

    public ChatClear(Peer peer) {
        this.peer = peer;
    }

    public Peer getPeer() {
        return peer;
    }
}
