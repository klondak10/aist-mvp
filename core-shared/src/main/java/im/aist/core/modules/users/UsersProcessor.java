package im.aist.core.modules.users;

import im.aist.core.api.updates.UpdateContactRegistered;
import im.aist.core.api.updates.UpdateUserAboutChanged;
import im.aist.core.api.updates.UpdateUserAvatarChanged;
import im.aist.core.api.updates.UpdateUserBlocked;
import im.aist.core.api.updates.UpdateUserBotCommandsChanged;
import im.aist.core.api.updates.UpdateUserContactsChanged;
import im.aist.core.api.updates.UpdateUserExtChanged;
import im.aist.core.api.updates.UpdateUserFullExtChanged;
import im.aist.core.api.updates.UpdateUserLocalNameChanged;
import im.aist.core.api.updates.UpdateUserNameChanged;
import im.aist.core.api.updates.UpdateUserNickChanged;
import im.aist.core.api.updates.UpdateUserPreferredLanguagesChanged;
import im.aist.core.api.updates.UpdateUserTimeZoneChanged;
import im.aist.core.api.updates.UpdateUserUnblocked;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.sequence.processor.SequenceProcessor;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

public class UsersProcessor extends AbsModule implements SequenceProcessor {

    public UsersProcessor(ModuleContext context) {
        super(context);
    }

    @Override
    public Promise<Void> process(Update update) {
        if (update instanceof UpdateUserNameChanged ||
                update instanceof UpdateUserLocalNameChanged ||
                update instanceof UpdateUserNickChanged ||
                update instanceof UpdateUserAboutChanged ||
                update instanceof UpdateUserAvatarChanged ||
                update instanceof UpdateContactRegistered ||
                update instanceof UpdateUserTimeZoneChanged ||
                update instanceof UpdateUserPreferredLanguagesChanged ||
                update instanceof UpdateUserExtChanged ||
                update instanceof UpdateUserFullExtChanged ||
                update instanceof UpdateUserBotCommandsChanged ||
                update instanceof UpdateUserContactsChanged ||
                update instanceof UpdateUserBlocked ||
                update instanceof UpdateUserUnblocked) {
            return context().getUsersModule().getUserRouter().onUpdate(update);
        }
        return null;
    }
}
