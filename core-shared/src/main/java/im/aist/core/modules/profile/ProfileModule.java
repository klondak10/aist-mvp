/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.profile;

import im.aist.core.modules.AbsModule;
import im.aist.core.modules.Modules;
import im.aist.runtime.actors.ActorRef;
import im.aist.core.modules.profile.avatar.OwnAvatarChangeActor;
import im.aist.core.viewmodel.OwnAvatarVM;

import static im.aist.runtime.actors.ActorSystem.system;

public class ProfileModule extends AbsModule {

    private ActorRef avatarChangeActor;
    private OwnAvatarVM ownAvatarVM;

    public ProfileModule(final Modules modules) {
        super(modules);
        ownAvatarVM = new OwnAvatarVM();
        avatarChangeActor = system().actorOf("actor/avatar/my", () -> new OwnAvatarChangeActor(modules));
    }

    public OwnAvatarVM getOwnAvatarVM() {
        return ownAvatarVM;
    }

    public void changeAvatar(String descriptor) {
        avatarChangeActor.send(new OwnAvatarChangeActor.ChangeAvatar(descriptor));
    }

    public void removeAvatar() {
        avatarChangeActor.send(new OwnAvatarChangeActor.RemoveAvatar());
    }

    public void resetModule() {
        // TODO: Implement
    }
}
