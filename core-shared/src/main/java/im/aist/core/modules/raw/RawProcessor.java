package im.aist.core.modules.raw;


import im.aist.core.RawUpdatesHandler;
import im.aist.core.api.updates.UpdateRawUpdate;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.sequence.processor.SequenceProcessor;
import im.aist.core.network.parser.Update;
import im.aist.runtime.Log;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;
import im.aist.runtime.Runtime;

public class RawProcessor extends AbsModule implements SequenceProcessor {

    private final RawUpdatesHandler rawUpdatesHandler;

    public RawProcessor(ModuleContext context) {
        super(context);
        rawUpdatesHandler = context().getConfiguration().getRawUpdatesHandler();

    }

    @Override
    public Promise<Void> process(Update update) {
        if (update instanceof UpdateRawUpdate && rawUpdatesHandler != null) {
            return new Promise<>(resolver -> {
                Runtime.dispatch(() -> {
                    try {
                        Promise<Void> promise = rawUpdatesHandler.onRawUpdate((UpdateRawUpdate) update);
                        if (promise != null) {
                            promise.pipeTo(resolver);
                        }
                    } catch (Exception e) {
                        Log.e("RawUpdateHandler", e);
                    } finally {
                        resolver.result(null);
                    }
                });
            });
        }
        return null;
    }
}
