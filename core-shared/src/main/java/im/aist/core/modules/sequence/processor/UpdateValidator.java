package im.aist.core.modules.sequence.processor;

import java.util.Collection;
import java.util.HashSet;

import im.aist.core.api.ApiDialogGroup;
import im.aist.core.api.ApiDialogShort;
import im.aist.core.api.ApiMember;
import im.aist.core.api.ApiPeerType;
import im.aist.core.api.updates.UpdateChatGroupsChanged;
import im.aist.core.api.updates.UpdateContactRegistered;
import im.aist.core.api.updates.UpdateContactsAdded;
import im.aist.core.api.updates.UpdateContactsRemoved;
import im.aist.core.api.updates.UpdateGroupExtChanged;
import im.aist.core.api.updates.UpdateGroupFullExtChanged;
import im.aist.core.api.updates.UpdateGroupMemberAdminChanged;
import im.aist.core.api.updates.UpdateGroupMemberChanged;
import im.aist.core.api.updates.UpdateGroupMemberDiff;
import im.aist.core.api.updates.UpdateGroupMembersCountChanged;
import im.aist.core.api.updates.UpdateGroupMembersUpdated;
import im.aist.core.api.updates.UpdateGroupOwnerChanged;
import im.aist.core.api.updates.UpdateMessage;
import im.aist.core.api.updates.UpdateUserLocalNameChanged;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.network.parser.Update;
import im.aist.runtime.annotations.Verified;

public class UpdateValidator extends AbsModule {

    public UpdateValidator(ModuleContext context) {
        super(context);
    }

    public boolean isCausesInvalidation(Update update) {

        HashSet<Integer> users = new HashSet<>();
        HashSet<Integer> groups = new HashSet<>();

        if (update instanceof UpdateMessage) {
            UpdateMessage updateMessage = (UpdateMessage) update;
            users.add(updateMessage.getSenderUid());
            if (updateMessage.getPeer().getType() == ApiPeerType.GROUP) {
                groups.add(updateMessage.getPeer().getId());
            }
            if (updateMessage.getPeer().getType() == ApiPeerType.PRIVATE) {
                users.add(updateMessage.getPeer().getId());
            }
        } else if (update instanceof UpdateContactRegistered) {
            UpdateContactRegistered contactRegistered = (UpdateContactRegistered) update;
            users.add(contactRegistered.getUid());
        } else if (update instanceof UpdateContactsAdded) {
            users.addAll(((UpdateContactsAdded) update).getUids());
        } else if (update instanceof UpdateContactsRemoved) {
            users.addAll(((UpdateContactsRemoved) update).getUids());
        } else if (update instanceof UpdateUserLocalNameChanged) {
            UpdateUserLocalNameChanged localNameChanged = (UpdateUserLocalNameChanged) update;
            users.add(localNameChanged.getUid());
        } else if (update instanceof UpdateChatGroupsChanged) {
            UpdateChatGroupsChanged changed = (UpdateChatGroupsChanged) update;
            for (ApiDialogGroup group : changed.getDialogs()) {
                for (ApiDialogShort dialog : group.getDialogs()) {
                    if (dialog.getPeer().getType() == ApiPeerType.PRIVATE) {
                        users.add(dialog.getPeer().getId());
                    } else if (dialog.getPeer().getType() == ApiPeerType.GROUP) {
                        groups.add(dialog.getPeer().getId());
                    }
                }
            }
        } else if (update instanceof UpdateGroupMemberChanged) {
            UpdateGroupMemberChanged memberChanged = (UpdateGroupMemberChanged) update;
            groups.add(memberChanged.getGroupId());
        } else if (update instanceof UpdateGroupMemberDiff) {
            UpdateGroupMemberDiff diff = (UpdateGroupMemberDiff) update;
            groups.add(diff.getGroupId());
            for (Integer u : diff.getRemovedUsers()) {
                users.add(u);
            }
            for (ApiMember m : diff.getAddedMembers()) {
                users.add(m.getInviterUid());
                users.add(m.getUid());
            }
        } else if (update instanceof UpdateGroupMembersUpdated) {
            UpdateGroupMembersUpdated u = (UpdateGroupMembersUpdated) update;
            groups.add(u.getGroupId());
            for (ApiMember m : u.getMembers()) {
                users.add(m.getInviterUid());
                users.add(m.getUid());
            }
        } else if (update instanceof UpdateGroupMemberAdminChanged) {
            UpdateGroupMemberAdminChanged u = (UpdateGroupMemberAdminChanged) update;
            users.add(u.getUserId());
            groups.add(u.getGroupId());
        } else if (update instanceof UpdateGroupMembersCountChanged) {
            UpdateGroupMembersCountChanged countChanged = (UpdateGroupMembersCountChanged) update;
            groups.add(countChanged.getGroupId());
        } else if (update instanceof UpdateGroupOwnerChanged) {
            UpdateGroupOwnerChanged ownerChanged = (UpdateGroupOwnerChanged) update;
            groups.add(ownerChanged.getGroupId());
            users.add(ownerChanged.getUserId());
        } else if (update instanceof UpdateGroupFullExtChanged) {
            UpdateGroupFullExtChanged fullExtChanged = (UpdateGroupFullExtChanged) update;
            groups.add(fullExtChanged.getGroupId());
        } else if (update instanceof UpdateGroupExtChanged) {
            UpdateGroupExtChanged extChanged = (UpdateGroupExtChanged) update;
            groups.add(extChanged.getGroupId());
        }
        if (!hasUsers(users)) {
            return true;
        }

        if (!hasGroups(groups)) {
            return true;
        }

        return false;
    }

    @Verified
    public boolean hasUsers(Collection<Integer> uids) {
        for (Integer uid : uids) {
            if (users().getValue(uid) == null) {
                return false;
            }
        }
        return true;
    }

    @Verified
    public boolean hasGroups(Collection<Integer> gids) {
        for (Integer uid : gids) {
            if (groups().getValue(uid) == null) {
                return false;
            }
        }
        return true;
    }
}
