package im.aist.core.modules.messaging.dialogs.entity;

import im.aist.core.entity.Message;
import im.aist.core.entity.Peer;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class MessageDeleted implements AskMessage<Void> {

    private Peer peer;
    private Message topMessage;

    public MessageDeleted(Peer peer, Message topMessage) {
        this.peer = peer;
        this.topMessage = topMessage;
    }

    public Peer getPeer() {
        return peer;
    }

    public Message getTopMessage() {
        return topMessage;
    }
}
