package im.aist.core.modules.users.router.entity;

import java.util.List;

import im.aist.core.api.ApiUser;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterApplyUsers implements AskMessage<Void> {

    private List<ApiUser> users;

    public RouterApplyUsers(List<ApiUser> users) {
        this.users = users;
    }

    public List<ApiUser> getUsers() {
        return users;
    }
}
