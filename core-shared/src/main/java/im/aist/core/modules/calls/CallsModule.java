package im.aist.core.modules.calls;

import im.aist.core.entity.Peer;
import im.aist.core.viewmodel.CallVM;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.viewmodel.Command;
import im.aist.runtime.actors.ActorRef;

import static im.aist.runtime.actors.ActorSystem.system;

public class CallsModule extends AbsModule {

    public static final String TAG = "CALLS";

    private ActorRef callManager;
    private CallViewModels callViewModels;

    public CallsModule(ModuleContext context) {
        super(context);

        if (context().getConfiguration().isVoiceCallsEnabled()) {
            callViewModels = new CallViewModels(context());
        }
    }

    public void run() {
        if (context().getConfiguration().isVoiceCallsEnabled()) {
            callManager = system().actorOf("calls/manager", CallManagerActor.CONSTRUCTOR(context()));
        }
    }

    public CallViewModels getCallViewModels() {
        return callViewModels;
    }

    public CallVM getCall(long id) {
        return callViewModels.getCall(id);
    }

    public ActorRef getCallManager() {
        return callManager;
    }

    public void checkCall(long callId, int attempt) {
        callManager.send(new CallManagerActor.OnIncomingCallLocked(callId, attempt, im.aist.runtime.Runtime.makeWakeLock()));
    }

    public void probablyEndCall() {
        callManager.send(new CallManagerActor.ProbablyEndCall());
    }

    public Command<Long> makeCall(final Peer peer, boolean enableVideoCall) {
        return callback -> callManager.send(new CallManagerActor.DoCall(peer, callback, enableVideoCall));
    }

    public void muteCall(long callId) {
        callManager.send(new CallManagerActor.AudioDisable(callId));
    }

    public void unmuteCall(long callId) {
        callManager.send(new CallManagerActor.AudioEnable(callId));
    }

    public void disableVideo(long callId) {
        callManager.send(new CallManagerActor.DisableVideo(callId));
    }

    public void enableVideo(long callId) {
        callManager.send(new CallManagerActor.EnableVideo(callId));
    }

    public void endCall(long callId) {
        callManager.send(new CallManagerActor.DoEndCall(callId));
    }

    public void answerCall(long callId) {
        callManager.send(new CallManagerActor.DoAnswerCall(callId));
    }
}
