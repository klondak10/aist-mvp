/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.messaging;

import java.util.ArrayList;
import java.util.List;

import im.aist.core.api.ApiPeer;
import im.aist.core.api.updates.UpdateChatClear;
import im.aist.core.api.updates.UpdateChatDelete;
import im.aist.core.api.updates.UpdateChatDropCache;
import im.aist.core.api.updates.UpdateChatGroupsChanged;
import im.aist.core.api.updates.UpdateMessage;
import im.aist.core.api.updates.UpdateMessageContentChanged;
import im.aist.core.api.updates.UpdateMessageDelete;
import im.aist.core.api.updates.UpdateMessageRead;
import im.aist.core.api.updates.UpdateMessageReadByMe;
import im.aist.core.api.updates.UpdateMessageReceived;
import im.aist.core.api.updates.UpdateMessageSent;
import im.aist.core.api.updates.UpdateReactionsUpdate;
import im.aist.core.entity.Message;
import im.aist.core.entity.MessageState;
import im.aist.core.entity.Peer;
import im.aist.core.entity.content.AbsContent;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.sequence.processor.SequenceProcessor;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.annotations.Verified;
import im.aist.runtime.promise.Promise;

import static im.aist.core.entity.EntityConverter.convert;

public class MessagesProcessor extends AbsModule implements SequenceProcessor {

    public MessagesProcessor(ModuleContext context) {
        super(context);
    }


    //
    // Differences
    //

    @Verified
    public Promise<Void> onDifferenceStart() {
        return context().getMessagesModule().getRouter().onDifferenceStart();
    }

    @Verified
    public Promise<Void> onDifferenceMessages(ApiPeer _peer, List<UpdateMessage> messages) {

        Peer peer = convert(_peer);

        ArrayList<Message> nMessages = new ArrayList<>();
        for (UpdateMessage u : messages) {

            AbsContent msgContent = AbsContent.fromMessage(u.getMessage());

            nMessages.add(new Message(
                    u.getRid(),
                    u.getDate(),
                    u.getDate(),
                    u.getSenderUid(),
                    myUid() == u.getSenderUid() ? MessageState.SENT : MessageState.UNKNOWN,
                    msgContent));
        }


        return context().getMessagesModule().getRouter().onNewMessages(peer, nMessages);
    }

    @Verified
    public Promise<Void> onDifferenceEnd() {
        return context().getMessagesModule().getRouter().onDifferenceEnd();
    }


    //
    // Update Handling
    //

    @Override
    public Promise<Void> process(Update update) {
        if (update instanceof UpdateMessage ||
                update instanceof UpdateMessageRead ||
                update instanceof UpdateMessageReadByMe ||
                update instanceof UpdateMessageReceived ||
                update instanceof UpdateMessageDelete ||
                update instanceof UpdateMessageContentChanged ||
                update instanceof UpdateChatClear ||
                update instanceof UpdateChatDelete ||
                update instanceof UpdateChatDropCache ||
                update instanceof UpdateChatGroupsChanged ||
                update instanceof UpdateReactionsUpdate ||
                update instanceof UpdateMessageSent) {

            return context().getMessagesModule().getRouter().onUpdate(update);
        }
        return null;
    }
}
