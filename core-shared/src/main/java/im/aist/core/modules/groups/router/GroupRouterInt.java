package im.aist.core.modules.groups.router;

import java.util.List;

import im.aist.core.api.ApiGroup;
import im.aist.core.api.ApiGroupOutPeer;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.groups.router.entity.RouterApplyGroups;
import im.aist.core.modules.groups.router.entity.RouterFetchMissingGroups;
import im.aist.core.modules.groups.router.entity.RouterGroupUpdate;
import im.aist.core.modules.groups.router.entity.RouterLoadFullGroup;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.ActorInterface;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

import static im.aist.runtime.actors.ActorSystem.system;

public class GroupRouterInt extends ActorInterface {

    public GroupRouterInt(ModuleContext context) {
        setDest(system().actorOf("groups/router", () -> new GroupRouter(context)));
    }

    public Promise<Void> applyGroups(List<ApiGroup> groups) {
        return ask(new RouterApplyGroups(groups));
    }

    public Promise<List<ApiGroupOutPeer>> fetchPendingGroups(List<ApiGroupOutPeer> peers) {
        return ask(new RouterFetchMissingGroups(peers));
    }

    public Promise<Void> onUpdate(Update update) {
        return ask(new RouterGroupUpdate(update));
    }

    public void onFullGroupNeeded(int gid) {
        send(new RouterLoadFullGroup(gid));
    }
}
