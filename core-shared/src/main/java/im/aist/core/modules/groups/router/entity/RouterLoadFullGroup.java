package im.aist.core.modules.groups.router.entity;

public class RouterLoadFullGroup {

    private int gid;

    public RouterLoadFullGroup(int gid) {
        this.gid = gid;
    }

    public int getGid() {
        return gid;
    }
}
