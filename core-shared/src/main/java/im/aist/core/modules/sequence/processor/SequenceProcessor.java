package im.aist.core.modules.sequence.processor;

import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

public interface SequenceProcessor {
    Promise<Void> process(Update update);
}
