package im.aist.core.modules.sequence.internal;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import im.aist.core.api.ApiGroup;
import im.aist.core.api.ApiUser;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class HandlerSeqUpdate implements AskMessage<Void> {

    @NotNull
    private Update update;
    @Nullable
    private List<ApiUser> users;
    @Nullable
    private
    List<ApiGroup> groups;

    public HandlerSeqUpdate(@NotNull Update update, @Nullable List<ApiUser> users, @Nullable List<ApiGroup> groups) {
        this.update = update;
        this.users = users;
        this.groups = groups;
    }

    @NotNull
    public Update getUpdate() {
        return update;
    }

    public
    @Nullable
    List<ApiUser> getUsers() {
        return users;
    }

    public
    @Nullable
    List<ApiGroup> getGroups() {
        return groups;
    }
}
