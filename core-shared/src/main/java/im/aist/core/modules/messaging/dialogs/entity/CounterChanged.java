package im.aist.core.modules.messaging.dialogs.entity;

import im.aist.core.entity.Peer;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class CounterChanged implements AskMessage<Void> {

    private Peer peer;
    private int counter;

    public CounterChanged(Peer peer, int counter) {
        this.peer = peer;
        this.counter = counter;
    }

    public Peer getPeer() {
        return peer;
    }

    public int getCounter() {
        return counter;
    }
}
