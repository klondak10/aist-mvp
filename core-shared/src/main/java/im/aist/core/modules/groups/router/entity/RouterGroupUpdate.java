package im.aist.core.modules.groups.router.entity;

import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterGroupUpdate implements AskMessage<Void> {

    private Update update;

    public RouterGroupUpdate(Update update) {
        this.update = update;
    }

    public Update getUpdate() {
        return update;
    }
}
