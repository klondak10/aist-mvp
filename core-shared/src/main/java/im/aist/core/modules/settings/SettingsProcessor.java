/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.settings;

import im.aist.core.api.updates.UpdateParameterChanged;
import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.sequence.processor.SequenceProcessor;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

public class SettingsProcessor extends AbsModule implements SequenceProcessor {

    public SettingsProcessor(ModuleContext modules) {
        super(modules);
    }

    public void onSettingsChanged(String key, String value) {
        context().getSettingsModule().onUpdatedSetting(key, value);
        context().getSettingsModule().notifySettingsChanged();
    }

    @Override
    public Promise<Void> process(Update update) {
        if (update instanceof UpdateParameterChanged) {
            onSettingsChanged(
                    ((UpdateParameterChanged) update).getKey(),
                    ((UpdateParameterChanged) update).getValue());
            return Promise.success(null);
        }
        return null;
    }
}
