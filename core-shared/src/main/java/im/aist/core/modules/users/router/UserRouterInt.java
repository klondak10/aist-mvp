package im.aist.core.modules.users.router;

import java.util.List;

import im.aist.core.api.ApiUser;
import im.aist.core.api.ApiUserOutPeer;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.users.router.entity.RouterApplyUsers;
import im.aist.core.modules.users.router.entity.RouterFetchMissingUsers;
import im.aist.core.modules.users.router.entity.RouterLoadFullUser;
import im.aist.core.modules.users.router.entity.RouterUserUpdate;
import im.aist.core.network.parser.Update;
import im.aist.runtime.actors.ActorInterface;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

import static im.aist.runtime.actors.ActorSystem.system;

public class UserRouterInt extends ActorInterface {

    public UserRouterInt(ModuleContext context) {
        super(system().actorOf("users/router", () -> new UserRouter(context)));
    }

    public Promise<Void> applyUsers(List<ApiUser> users) {
        return ask(new RouterApplyUsers(users));
    }

    public Promise<List<ApiUserOutPeer>> fetchMissingUsers(List<ApiUserOutPeer> users) {
        return ask(new RouterFetchMissingUsers(users));
    }

    public Promise<Void> onUpdate(Update update) {
        return ask(new RouterUserUpdate(update));
    }

    public void onFullUserNeeded(int uid) {
        send(new RouterLoadFullUser(uid));
    }
}