package im.aist.core.modules.messaging.router.entity;

import im.aist.runtime.actors.ask.AskMessage;
import im.aist.runtime.actors.messages.Void;

public class RouterDifferenceEnd implements AskMessage<Void>, RouterMessageOnlyActive {

}
