package im.aist.core.modules.messaging.dialogs;

import java.util.List;

import im.aist.core.entity.Group;
import im.aist.core.entity.Message;
import im.aist.core.entity.Peer;
import im.aist.core.entity.User;
import im.aist.core.entity.content.AbsContent;
import im.aist.core.modules.ModuleContext;
import im.aist.core.modules.messaging.dialogs.entity.ChatClear;
import im.aist.core.modules.messaging.dialogs.entity.ChatDelete;
import im.aist.core.modules.messaging.dialogs.entity.CounterChanged;
import im.aist.core.modules.messaging.dialogs.entity.GroupChanged;
import im.aist.core.modules.messaging.dialogs.entity.HistoryLoaded;
import im.aist.core.modules.messaging.dialogs.entity.InMessage;
import im.aist.core.modules.messaging.dialogs.entity.MessageContentChanged;
import im.aist.core.modules.messaging.dialogs.entity.MessageDeleted;
import im.aist.core.modules.messaging.dialogs.entity.PeerReadChanged;
import im.aist.core.modules.messaging.dialogs.entity.PeerReceiveChanged;
import im.aist.core.modules.messaging.dialogs.entity.UserChanged;
import im.aist.core.modules.messaging.history.entity.DialogHistory;
import im.aist.runtime.actors.ActorInterface;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

import static im.aist.runtime.actors.ActorSystem.system;

public class DialogsInt extends ActorInterface {

    public DialogsInt(ModuleContext context) {
        setDest(system().actorOf("actor/dialogs", () -> new DialogsActor(context)));
    }

    public Promise<Void> onChatClear(Peer peer) {
        return ask(new ChatClear(peer));
    }

    public Promise<Void> onChatDelete(Peer peer) {
        return ask(new ChatDelete(peer));
    }

    public Promise<Void> onCounterChanged(Peer peer, int counter) {
        return ask(new CounterChanged(peer, counter));
    }

    public Promise<Void> onGroupChanged(Group group) {
        return ask(new GroupChanged(group));
    }

    public Promise<Void> onUserChanged(User user) {
        return ask(new UserChanged(user));
    }

    public Promise<Void> onHistoryLoaded(List<DialogHistory> history) {
        return ask(new HistoryLoaded(history));
    }

    public Promise<Void> onMessage(Peer peer, Message message, int counter) {
        return ask(new InMessage(peer, message, counter));
    }

    public Promise<Void> onMessageContentChanged(Peer peer, long rid, AbsContent content) {
        return ask(new MessageContentChanged(peer, rid, content));
    }

    public Promise<Void> onMessageDeleted(Peer peer, Message topMessage) {
        return ask(new MessageDeleted(peer, topMessage));
    }

    public Promise<Void> onPeerReadChanged(Peer peer, long date) {
        return ask(new PeerReadChanged(peer, date));
    }

    public Promise<Void> onPeerReceiveChanged(Peer peer, long date) {
        return ask(new PeerReceiveChanged(peer, date));
    }
}
