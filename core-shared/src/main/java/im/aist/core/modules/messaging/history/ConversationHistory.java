package im.aist.core.modules.messaging.history;

import im.aist.core.entity.Peer;
import im.aist.core.modules.ModuleContext;
import im.aist.runtime.actors.ActorInterface;
import im.aist.runtime.actors.messages.Void;
import im.aist.runtime.promise.Promise;

import static im.aist.runtime.actors.ActorSystem.system;

public class ConversationHistory extends ActorInterface {

    public ConversationHistory(Peer peer, ModuleContext context) {
        setDest(system().actorOf("history/" + peer, () -> {
            return new ConversationHistoryActor(peer, context);
        }));
    }

    public void loadMore() {
        send(new ConversationHistoryActor.LoadMore());
    }

    public Promise<Void> reset() {
        return ask(new ConversationHistoryActor.Reset());
    }
}
