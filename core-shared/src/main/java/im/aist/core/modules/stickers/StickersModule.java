/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.core.modules.stickers;

import im.aist.core.modules.AbsModule;
import im.aist.core.modules.ModuleContext;
import im.aist.core.viewmodel.StickersVM;
import im.aist.runtime.actors.ActorRef;

import static im.aist.runtime.actors.ActorSystem.system;

public class StickersModule extends AbsModule {

    private ActorRef stickersActor;
    private StickersVM stickersVM;

    public StickersModule(ModuleContext context) {
        super(context);
    }

    public void run() {
        this.stickersVM = new StickersVM();
        this.stickersActor = system().actorOf("actor/stickers", () -> new StickersActor(context()));
    }

    public ActorRef getStickersActor() {
        return stickersActor;
    }

    public StickersVM getStickersVM() {
        return stickersVM;
    }
}