package im.aist.core;

import im.aist.core.api.updates.UpdateRawUpdate;
import im.aist.runtime.promise.Promise;

public abstract class RawUpdatesHandler {

    public abstract Promise<im.aist.runtime.actors.messages.Void> onRawUpdate(UpdateRawUpdate update);

}
