package im.aist.core.api.updates;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;
import im.aist.core.network.parser.*;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public class UpdateGroupShortNameChanged extends Update {

    public static final int HEADER = 0xa44;
    public static UpdateGroupShortNameChanged fromBytes(byte[] data) throws IOException {
        return Bser.parse(new UpdateGroupShortNameChanged(), data);
    }

    private int groupId;
    private String shortName;

    public UpdateGroupShortNameChanged(int groupId, @Nullable String shortName) {
        this.groupId = groupId;
        this.shortName = shortName;
    }

    public UpdateGroupShortNameChanged() {

    }

    public int getGroupId() {
        return this.groupId;
    }

    @Nullable
    public String getShortName() {
        return this.shortName;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.groupId = values.getInt(1);
        this.shortName = values.optString(2);
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        writer.writeInt(1, this.groupId);
        if (this.shortName != null) {
            writer.writeString(2, this.shortName);
        }
    }

    @Override
    public String toString() {
        String res = "update GroupShortNameChanged{";
        res += "groupId=" + this.groupId;
        res += ", shortName=" + this.shortName;
        res += "}";
        return res;
    }

    @Override
    public int getHeaderKey() {
        return HEADER;
    }
}
