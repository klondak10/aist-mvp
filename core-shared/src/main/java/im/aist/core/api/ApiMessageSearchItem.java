package im.aist.core.api;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class ApiMessageSearchItem extends BserObject {

    private ApiMessageSearchResult result;

    public ApiMessageSearchItem(@NotNull ApiMessageSearchResult result) {
        this.result = result;
    }

    public ApiMessageSearchItem() {

    }

    @NotNull
    public ApiMessageSearchResult getResult() {
        return this.result;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.result = values.getObj(1, new ApiMessageSearchResult());
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        if (this.result == null) {
            throw new IOException();
        }
        writer.writeObject(1, this.result);
    }

    @Override
    public String toString() {
        String res = "struct MessageSearchItem{";
        res += "}";
        return res;
    }

}
