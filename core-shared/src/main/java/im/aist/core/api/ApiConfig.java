package im.aist.core.api;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;

import java.io.IOException;

public class ApiConfig extends BserObject {

    private int maxGroupSize;

    public ApiConfig(int maxGroupSize) {
        this.maxGroupSize = maxGroupSize;
    }

    public ApiConfig() {

    }

    public int getMaxGroupSize() {
        return this.maxGroupSize;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.maxGroupSize = values.getInt(1);
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        writer.writeInt(1, this.maxGroupSize);
    }

    @Override
    public String toString() {
        String res = "struct Config{";
        res += "maxGroupSize=" + this.maxGroupSize;
        res += "}";
        return res;
    }

}
