package im.aist.core.api.rpc;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;
import im.aist.core.network.parser.*;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import im.aist.core.api.*;

public class RequestFavouriteDialog extends Request<ResponseDialogsOrder> {

    public static final int HEADER = 0xe0;
    public static RequestFavouriteDialog fromBytes(byte[] data) throws IOException {
        return Bser.parse(new RequestFavouriteDialog(), data);
    }

    private ApiOutPeer peer;

    public RequestFavouriteDialog(@NotNull ApiOutPeer peer) {
        this.peer = peer;
    }

    public RequestFavouriteDialog() {

    }

    @NotNull
    public ApiOutPeer getPeer() {
        return this.peer;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.peer = values.getObj(1, new ApiOutPeer());
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        if (this.peer == null) {
            throw new IOException();
        }
        writer.writeObject(1, this.peer);
    }

    @Override
    public String toString() {
        String res = "rpc FavouriteDialog{";
        res += "peer=" + this.peer;
        res += "}";
        return res;
    }

    @Override
    public int getHeaderKey() {
        return HEADER;
    }
}
