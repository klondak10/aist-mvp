package im.aist.core.api;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public class ApiAdvertiseSelf extends ApiWebRTCSignaling {

    private ApiPeerSettings peerSettings;

    public ApiAdvertiseSelf(@Nullable ApiPeerSettings peerSettings) {
        this.peerSettings = peerSettings;
    }

    public ApiAdvertiseSelf() {

    }

    public int getHeader() {
        return 21;
    }

    @Nullable
    public ApiPeerSettings getPeerSettings() {
        return this.peerSettings;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.peerSettings = values.optObj(1, new ApiPeerSettings());
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        if (this.peerSettings != null) {
            writer.writeObject(1, this.peerSettings);
        }
    }

    @Override
    public String toString() {
        String res = "struct AdvertiseSelf{";
        res += "peerSettings=" + this.peerSettings;
        res += "}";
        return res;
    }

}
