package im.aist.core.api.updates;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;
import im.aist.core.network.parser.*;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import im.aist.core.api.*;

public class UpdateChatClear extends Update {

    public static final int HEADER = 0x2f;
    public static UpdateChatClear fromBytes(byte[] data) throws IOException {
        return Bser.parse(new UpdateChatClear(), data);
    }

    private ApiPeer peer;

    public UpdateChatClear(@NotNull ApiPeer peer) {
        this.peer = peer;
    }

    public UpdateChatClear() {

    }

    @NotNull
    public ApiPeer getPeer() {
        return this.peer;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.peer = values.getObj(1, new ApiPeer());
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        if (this.peer == null) {
            throw new IOException();
        }
        writer.writeObject(1, this.peer);
    }

    @Override
    public String toString() {
        String res = "update ChatClear{";
        res += "peer=" + this.peer;
        res += "}";
        return res;
    }

    @Override
    public int getHeaderKey() {
        return HEADER;
    }
}
