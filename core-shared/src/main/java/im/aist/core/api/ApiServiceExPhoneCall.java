package im.aist.core.api;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;
import im.aist.runtime.collections.*;

import java.io.IOException;

public class ApiServiceExPhoneCall extends ApiServiceEx {

    private int duration;

    public ApiServiceExPhoneCall(int duration) {
        this.duration = duration;
    }

    public ApiServiceExPhoneCall() {

    }

    public int getHeader() {
        return 16;
    }

    public int getDuration() {
        return this.duration;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.duration = values.getInt(1);
        if (values.hasRemaining()) {
            setUnmappedObjects(values.buildRemaining());
        }
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        writer.writeInt(1, this.duration);
        if (this.getUnmappedObjects() != null) {
            SparseArray<Object> unmapped = this.getUnmappedObjects();
            for (int i = 0; i < unmapped.size(); i++) {
                int key = unmapped.keyAt(i);
                writer.writeUnmapped(key, unmapped.get(key));
            }
        }
    }

    @Override
    public String toString() {
        String res = "struct ServiceExPhoneCall{";
        res += "duration=" + this.duration;
        res += "}";
        return res;
    }

}
