package im.aist.core.api;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;
import im.aist.runtime.collections.*;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public class ApiMember extends BserObject {

    private int uid;
    private int inviterUid;
    private long date;
    private Boolean isAdmin;

    public ApiMember(int uid, int inviterUid, long date, @Nullable Boolean isAdmin) {
        this.uid = uid;
        this.inviterUid = inviterUid;
        this.date = date;
        this.isAdmin = isAdmin;
    }

    public ApiMember() {

    }

    public int getUid() {
        return this.uid;
    }

    public int getInviterUid() {
        return this.inviterUid;
    }

    public long getDate() {
        return this.date;
    }

    @Nullable
    public Boolean isAdmin() {
        return this.isAdmin;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.uid = values.getInt(1);
        this.inviterUid = values.getInt(2);
        this.date = values.getLong(3);
        this.isAdmin = values.optBool(4);
        if (values.hasRemaining()) {
            setUnmappedObjects(values.buildRemaining());
        }
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        writer.writeInt(1, this.uid);
        writer.writeInt(2, this.inviterUid);
        writer.writeLong(3, this.date);
        if (this.isAdmin != null) {
            writer.writeBool(4, this.isAdmin);
        }
        if (this.getUnmappedObjects() != null) {
            SparseArray<Object> unmapped = this.getUnmappedObjects();
            for (int i = 0; i < unmapped.size(); i++) {
                int key = unmapped.keyAt(i);
                writer.writeUnmapped(key, unmapped.get(key));
            }
        }
    }

    @Override
    public String toString() {
        String res = "struct Member{";
        res += "uid=" + this.uid;
        res += ", inviterUid=" + this.inviterUid;
        res += ", date=" + this.date;
        res += ", isAdmin=" + this.isAdmin;
        res += "}";
        return res;
    }

}
