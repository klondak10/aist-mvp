package im.aist.core.api.rpc;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;
import im.aist.core.network.parser.*;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import im.aist.core.api.*;

public class RequestGetReferencedEntitites extends Request<ResponseGetReferencedEntitites> {

    public static final int HEADER = 0xa44;
    public static RequestGetReferencedEntitites fromBytes(byte[] data) throws IOException {
        return Bser.parse(new RequestGetReferencedEntitites(), data);
    }

    private List<ApiUserOutPeer> users;
    private List<ApiGroupOutPeer> groups;

    public RequestGetReferencedEntitites(@NotNull List<ApiUserOutPeer> users, @NotNull List<ApiGroupOutPeer> groups) {
        this.users = users;
        this.groups = groups;
    }

    public RequestGetReferencedEntitites() {

    }

    @NotNull
    public List<ApiUserOutPeer> getUsers() {
        return this.users;
    }

    @NotNull
    public List<ApiGroupOutPeer> getGroups() {
        return this.groups;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        List<ApiUserOutPeer> _users = new ArrayList<ApiUserOutPeer>();
        for (int i = 0; i < values.getRepeatedCount(1); i ++) {
            _users.add(new ApiUserOutPeer());
        }
        this.users = values.getRepeatedObj(1, _users);
        List<ApiGroupOutPeer> _groups = new ArrayList<ApiGroupOutPeer>();
        for (int i = 0; i < values.getRepeatedCount(2); i ++) {
            _groups.add(new ApiGroupOutPeer());
        }
        this.groups = values.getRepeatedObj(2, _groups);
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        writer.writeRepeatedObj(1, this.users);
        writer.writeRepeatedObj(2, this.groups);
    }

    @Override
    public String toString() {
        String res = "rpc GetReferencedEntitites{";
        res += "users=" + this.users.size();
        res += ", groups=" + this.groups.size();
        res += "}";
        return res;
    }

    @Override
    public int getHeaderKey() {
        return HEADER;
    }
}
