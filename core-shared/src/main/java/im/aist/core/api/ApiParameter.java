package im.aist.core.api;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class ApiParameter extends BserObject {

    private String key;
    private String value;

    public ApiParameter(@NotNull String key, @NotNull String value) {
        this.key = key;
        this.value = value;
    }

    public ApiParameter() {

    }

    @NotNull
    public String getKey() {
        return this.key;
    }

    @NotNull
    public String getValue() {
        return this.value;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.key = values.getString(1);
        this.value = values.getString(2);
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        if (this.key == null) {
            throw new IOException();
        }
        writer.writeString(1, this.key);
        if (this.value == null) {
            throw new IOException();
        }
        writer.writeString(2, this.value);
    }

    @Override
    public String toString() {
        String res = "struct Parameter{";
        res += "key=" + this.key;
        res += ", value=" + this.value;
        res += "}";
        return res;
    }

}
