package im.aist.core.api;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;
import im.aist.runtime.collections.*;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public class ApiServiceExChangedTopic extends ApiServiceEx {

    private String topic;

    public ApiServiceExChangedTopic(@Nullable String topic) {
        this.topic = topic;
    }

    public ApiServiceExChangedTopic() {

    }

    public int getHeader() {
        return 18;
    }

    @Nullable
    public String getTopic() {
        return this.topic;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.topic = values.optString(1);
        if (values.hasRemaining()) {
            setUnmappedObjects(values.buildRemaining());
        }
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        if (this.topic != null) {
            writer.writeString(1, this.topic);
        }
        if (this.getUnmappedObjects() != null) {
            SparseArray<Object> unmapped = this.getUnmappedObjects();
            for (int i = 0; i < unmapped.size(); i++) {
                int key = unmapped.keyAt(i);
                writer.writeUnmapped(key, unmapped.get(key));
            }
        }
    }

    @Override
    public String toString() {
        String res = "struct ServiceExChangedTopic{";
        res += "topic=" + this.topic;
        res += "}";
        return res;
    }

}
