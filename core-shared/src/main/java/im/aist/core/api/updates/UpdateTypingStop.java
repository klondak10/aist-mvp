package im.aist.core.api.updates;
/*
 *  Generated by the Actor API Scheme generator.  DO NOT EDIT!
 */

import im.aist.runtime.bser.*;
import im.aist.core.network.parser.*;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import im.aist.core.api.*;

public class UpdateTypingStop extends Update {

    public static final int HEADER = 0x51;
    public static UpdateTypingStop fromBytes(byte[] data) throws IOException {
        return Bser.parse(new UpdateTypingStop(), data);
    }

    private ApiPeer peer;
    private int uid;
    private ApiTypingType typingType;

    public UpdateTypingStop(@NotNull ApiPeer peer, int uid, @NotNull ApiTypingType typingType) {
        this.peer = peer;
        this.uid = uid;
        this.typingType = typingType;
    }

    public UpdateTypingStop() {

    }

    @NotNull
    public ApiPeer getPeer() {
        return this.peer;
    }

    public int getUid() {
        return this.uid;
    }

    @NotNull
    public ApiTypingType getTypingType() {
        return this.typingType;
    }

    @Override
    public void parse(BserValues values) throws IOException {
        this.peer = values.getObj(1, new ApiPeer());
        this.uid = values.getInt(2);
        this.typingType = ApiTypingType.parse(values.getInt(3));
    }

    @Override
    public void serialize(BserWriter writer) throws IOException {
        if (this.peer == null) {
            throw new IOException();
        }
        writer.writeObject(1, this.peer);
        writer.writeInt(2, this.uid);
        if (this.typingType == null) {
            throw new IOException();
        }
        writer.writeInt(3, this.typingType.getValue());
    }

    @Override
    public String toString() {
        String res = "update TypingStop{";
        res += "peer=" + this.peer;
        res += ", uid=" + this.uid;
        res += "}";
        return res;
    }

    @Override
    public int getHeaderKey() {
        return HEADER;
    }
}
