package com.notyteam.aist.android.ui.fragments.registration.enter_profile
import android.view.View
import com.bumptech.glide.Glide
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.activities.main.MainActivity
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.setting.categories.CategoriesFragment
import com.notyteam.aist.android.utils.openImagePicker
import com.notyteam.aist.android.utils.paper.PaperIO
import im.aist.sdk.aist.ActorSDK
import kotlinx.android.synthetic.main.fragment_enter_profile.*

class EnterProfileFragment : BaseFragment(), EnterProfileFragmentView {

    companion object {
        fun newInstance(): EnterProfileFragment {
            return EnterProfileFragment()
        }
    }

    private var presenter: EnterProfileFragmentPresenter = EnterProfileFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    val userId = PaperIO.getUserId()
    val user = ActorSDK.sharedActor().messenger.getUser(userId)

    override fun layout(): Int = R.layout.fragment_enter_profile

    override fun initialization(view: View, isFirstInit: Boolean) {
        getFirstTypeToolbar()
                ?.setLeftText(getString(R.string.back))
                ?.setRightText(getString(R.string.next))
                ?.setOnBackPressedListener {
                    onBackPressed()
                }
                ?.setOnRightTextListener {
                    MainActivity.start(baseContext)
                }

        if (isFirstInit) {
            initViews()
        }
    }

    override fun onFragmentResult(result: Any) {
        interests_edit_text?.setText(result as String)
    }

    private fun initViews() {
        rl_photo.setOnClickListener {
            openImagePicker(this) {
                Glide.with(baseContext)
                        .load(it)
                        .into(iv_avatar)
            }
        }

        interests_input_layout.setOnClickListener {
            val fragment = CategoriesFragment()
            prepareFragmentForResult(fragment)
            addFragment(fragment)
        }

        name_edit_text.setText(user.name.toString())




    }
}
