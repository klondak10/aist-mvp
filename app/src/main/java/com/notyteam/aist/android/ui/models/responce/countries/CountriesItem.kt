package com.notyteam.aist.android.ui.models.responce.countries

import com.google.gson.annotations.SerializedName

data class CountriesItem(

	@field:SerializedName("name")
	val fullCountryName: String = "",

	@field:SerializedName("dial_code")
	val dialCode: String = "",

	@field:SerializedName("code")
	val countryCode: String = ""
)