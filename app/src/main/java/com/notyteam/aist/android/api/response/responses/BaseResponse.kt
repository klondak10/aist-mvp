package com.notyteam.aist.android.api.response.responses

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.notyteam.aist.android.api.response.Error

@Generated("com.robohorse.robopojogenerator")
open class BaseResponse(
	@field:SerializedName("success")
	var successs: Boolean = false,

	@field:SerializedName("error")
	val error: Error = Error()
)