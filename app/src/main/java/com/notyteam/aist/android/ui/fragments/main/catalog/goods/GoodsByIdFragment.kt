package com.notyteam.aist.android.ui.fragments.main.catalog.goods


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.text.Html
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CategoriesListById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.ui.adapters.catalog.CategoriesAdapter
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.view.CatalogFragmentView
import com.notyteam.aist.android.ui.fragments.main.catalog.presenter.GoodsByIdFragmentPresenter
import com.notyteam.aist.android.utils.carouselview.ImageListener
import kotlinx.android.synthetic.main.fragment_goods.*
import io.paperdb.Paper
import android.content.Intent.ACTION_SEND
import android.os.Handler
import com.notyteam.aist.android.ui.actionsheet.ActionItem
import com.notyteam.aist.android.ui.actionsheet.ActionSheet
import com.notyteam.aist.android.utils.paper.PaperIO
import kotlinx.android.synthetic.main.fragment_goods.tabs


@Suppress("DEPRECATION")
class GoodsByIdFragment() : BaseFragment(), CatalogFragmentView, ActionSheet.ActionSheetListener {


    override fun onCatalogListSuccessPagePerPage(list: CatalogListResponse) {}
    override fun onCatalogListByIdSuccess(list: CategoriesListById) {}
    override fun onCatalogListSuccess(list: CatalogListResponse) { }
    override fun onFavoritesError(error: String) {}
    override fun onFavoritesSuccess(success: Boolean) {}

    var description:String = ""
    var add_to_cart_urls:String = ""

    @SuppressLint("SetTextI18n")
    override fun onGoodsByIdSuccess(data: GoodsResponseById) {
        showProgress(false)
        if(data.description==null){
            description = "Описание отсутствует"
        }else {
            description = data.description
        }
        if(data.add_to_cart_url.contains("_1")){
            add_to_cart_urls = data.add_to_cart_url
        }else{
            add_to_cart_urls = data.add_to_cart_url+"_1"
        }
        Paper.book().write("share_link",data.url)
        Paper.book().write("data.id",data.id)
        price_goods.text = data.price.toString()+".0 "+ Html.fromHtml(" &#x20bd")
        name_goods.text = data.name
        txt_size_image.text = "1/"+data.images.size.toString()

        carouselView.setImageListener(object : ImageListener {
            override fun setImageForPosition(position: Int, imageView: ImageView) {

                imageView.adjustViewBounds = true
                Glide.with(baseActivity)
                        .load(data.images[position])
                        .into(imageView);

            }
        })

        carouselView.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) { txt_size_image.text = (position+1).toString() + "/" + data.images.size.toString() }

        })

        carouselView.setPageCount(data.images.size);

        tabs.addTab(tabs.newTab().setText(getString(R.string.description)))
        tabs.addTab(tabs.newTab().setText(getString(R.string.specifications)))

        val firstFragment = GoodsDescriptionFragment.newInstance(description)
        val transaction = getChildFragmentManager()?.beginTransaction()
        transaction?.add(R.id.frame, firstFragment)
        transaction?.commitNow()

        tabs?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                if(tab.position==0){
                    val firstFragment = GoodsDescriptionFragment.newInstance(description)
                    val transaction = getChildFragmentManager()?.beginTransaction()
                    transaction?.replace(R.id.frame, firstFragment)
                    transaction?.commitNow()
                }else{
                    val firstFragment = GoodsSpecificationsFragment.newInstance(data.details.toString())
                    val transaction = getChildFragmentManager()?.beginTransaction()
                    transaction?.replace(R.id.frame, firstFragment)
                    transaction?.commitNow()
                }
        }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {
                if(tab.position==0){
                    val firstFragment = GoodsDescriptionFragment.newInstance(data.details.toString())
                    val transaction = getChildFragmentManager()?.beginTransaction()
                    transaction?.replace(R.id.frame, firstFragment)
                    transaction?.commitNow()
                }else{
                    val firstFragment = GoodsSpecificationsFragment.newInstance(description)
                    val transaction = getChildFragmentManager()?.beginTransaction()
                    transaction?.replace(R.id.frame, firstFragment)
                    transaction?.commitNow()
                }
            }
        })

        if(data.is_favorite!!){
            fab_favourite.setImageResource(R.drawable.ic_heart_active)
            Paper.book().write("fab_favourite"+data.id,data.id)
        }else{
            fab_favourite.setImageResource(R.drawable.ic_heart)
            Paper.book().delete("fab_favourite"+data.id)
        }

        fab_favourite.setOnClickListener {

            if(Paper.book().exist("fab_favourite"+data.id)){
                fab_favourite.setImageResource(R.drawable.ic_heart)
                Paper.book().delete("fab_favourite"+data.id)
                presenter.deleteFavorites(PaperIO.getUserId(),data.id)
            }else{
                fab_favourite.setImageResource(R.drawable.ic_heart_active)
                Paper.book().write("fab_favourite"+data.id,data.id)
                presenter.addFavorites(PaperIO.getUserId(),data.id)
            }

        }

        if(data.in_wish_list!!){
            getSecondTypeToolbar()?.toggleVertIcon(true)
            btn_i_want.visibility = View.GONE
            Paper.book().write("in_wish_list"+data.id,data.id)
        }else{
            btn_i_want.visibility = View.VISIBLE
            Paper.book().delete("in_wish_list"+data.id)
            getSecondTypeToolbar()?.toggleVertIcon(false)

        }

        btn_i_want.setOnClickListener {

            if(Paper.book().exist("in_wish_list"+data.id)){
                Paper.book().delete("in_wish_list"+data.id)
                getSecondTypeToolbar()?.toggleVertIcon(false)
                btn_i_want.visibility = View.VISIBLE
            }else{
                getSecondTypeToolbar()?.toggleVertIcon(true)
                btn_i_want.visibility = View.GONE
                Paper.book().write("in_wish_list"+data.id,data.id)
                presenter.addWish(PaperIO.getUserId(),data.id)
            }

        }

        btn_buy.setOnClickListener { addFragment(CustomWebview.newInstance(add_to_cart_urls,"www.ozon.ru")) }



    }

    override fun onActionButtonClick(actionSheet: ActionSheet?, index: Int) {
        ln_bottom.visibility = View.VISIBLE
        btn_i_want.visibility = View.VISIBLE
        getSecondTypeToolbar()?.toggleVertIcon(false)
        presenter.deleteWish(PaperIO.getUserId(),Paper.book().read("data.id"))
    }

    override fun onDismiss(p0: ActionSheet?, p1: Boolean) {
        ln_bottom.visibility = View.VISIBLE
    }

    companion object {
        private const val POSITION_KEY = "POSITION_KEY"
        fun newInstance(position:Int): GoodsByIdFragment {
            val f = GoodsByIdFragment ()
            // Pass index input as an argument.
            val args = Bundle()
            args.putInt(POSITION_KEY, position)
            f.setArguments(args)
            return f
        }
        fun getPosition(arguments: Bundle?) = arguments?.getInt(POSITION_KEY, 0)?:0
    }

    private var presenter = GoodsByIdFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }
    lateinit var adapter: CategoriesAdapter

    override fun layout(): Int = R.layout.fragment_goods

    override fun initialization(view: View, isFirstInit: Boolean) {

        activityBottomBarInterface.showBottomBar(false)

        getSecondTypeToolbar()
                ?.setLeftIcon(R.drawable.ic_btn_left)
                ?.setRightIcon(R.drawable.ic_share_default)
                ?.setOnLeftIconListener {
                      onBackPressed()
                }
                ?.setOnRightIconListener {
                    val i = Intent(ACTION_SEND)
                    i.setType("text/plain")
                    i.putExtra(Intent.EXTRA_TEXT, Paper.book().read<String>("share_link"))
                    startActivity(Intent.createChooser(i, ""))
                }
                ?.setOnVertIconListener {
                    val actionItems = arrayOf(
                            ActionItem(1, getString(R.string.dellete), true))
                    ActionSheet.createBuilder(baseActivity, baseActivity.supportFragmentManager)
                            .setCancelActionTitle(getString(R.string.cancel))
                            .setActionItems(actionItems)
                            .setCancelableOnTouchOutside(true)
                            .setListener(this).show();
                    ln_bottom.visibility = View.GONE
                }

        if (isFirstInit) {
            initViews()
        }

    }

    private fun initViews() {
        showProgress(true)
        presenter.getGoodsById(getPosition(arguments))
    }

    override fun onCatalogListError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        getSecondTypeToolbar()?.toggleVertIcon(false)
    }


}
