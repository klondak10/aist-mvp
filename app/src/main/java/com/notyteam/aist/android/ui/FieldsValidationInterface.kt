package com.notyteam.aist.android.ui

interface FieldsValidationInterface {
    fun isNameValid(name: String): Boolean
    fun isPasswordValid(password: String): Boolean
    fun isPasswordLongEnough(password: String): Boolean
    fun isPromoCodeValid(password: String): Boolean
    fun isPhoneValid(phone: String): Boolean
    fun isEmailValid(email: String): Boolean
}