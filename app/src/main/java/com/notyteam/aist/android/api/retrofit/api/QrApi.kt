package com.notyteam.aist.android.api.retrofit.api

import android.content.Context
import com.google.gson.JsonObject
import com.notyteam.aist.android.api.response.payment.BindQrResponse
import com.notyteam.aist.android.api.response.payment.FindQrResponse
import com.notyteam.aist.android.api.retrofit.CallbackWrapper
import com.notyteam.aist.android.api.retrofit.interfaces.PaymentApiInterface
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Retrofit
import java.lang.ref.WeakReference

class QrApi(private val context: WeakReference<Context>, private val retrofit: Retrofit) {

    private val paymentApiInterface: PaymentApiInterface by lazy {
        retrofit.create(PaymentApiInterface::class.java)
    }

    private fun <T> initRequestSingleForResult(request: Single<T>,
                                               onSuccess: (response: T) -> Unit,
                                               onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<T> {
        val callback = CallbackWrapper<T, T>(context, retrofit, onSuccess, onError)
        val requestSingle = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        callback.request = requestSingle
        return RequestHolder<T>(requestSingle, callback)
    }
    class RequestHolder<T>(private val request: Single<T>, private val callback: CallbackWrapper<T, T>){
        fun sendRequest() = request.subscribeWith(callback)
        fun getRequest() = request.doOnSuccess {callback.onSuccess(it)}.doOnError {callback.onError(it)}
    }

    fun findQr(filter:String,onSuccess: (response: FindQrResponse) -> Unit,
                 onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<FindQrResponse> {
        return initRequestSingleForResult(paymentApiInterface.findQr(filter), onSuccess, onError)
    }

    fun bindTheCardBuyer(id:String, data: RequestBody, onSuccess: (response: BindQrResponse) -> Unit,
                         onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<BindQrResponse> {
        return initRequestSingleForResult(paymentApiInterface.bindTheCardBuyer(id,data), onSuccess, onError)
    }

}