package com.notyteam.aist.android.utils

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.transition.Fade
import android.transition.Slide
import android.transition.Visibility
import android.view.Gravity
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.FragmentManagerInterface

class FragmentManager(@IdRes val containerId:Int, val supportFragmentManager: FragmentManager): FragmentManagerInterface {

    var bottomBarFragmentManager: BottomBarFragmentManager? = null

    override fun pushBottomBarFragment(fragment: Fragment) {
        getCurrentFragment()?.let {
            bottomBarFragmentManager?.addAdditionalTabFragment(fragment, containerId, true)
        }?:let {
            bottomBarFragmentManager?.addFirstTabFragment(fragment)
        }
    }

    override fun popBottomBarFragment() {
        bottomBarFragmentManager?.popFragment()
    }

    override fun replaceFragment(newFragment: Fragment) {
        var fragment = newFragment
        val fragmentTransaction = supportFragmentManager?.beginTransaction()
        val oldFragment = supportFragmentManager?.findFragmentByTag(fragment::class.simpleName)

        val currentFragment = getCurrentFragment()
        setFragmentFadeTransition(currentFragment)

        if (oldFragment != null) {
            fragment = oldFragment
        }

        fragmentTransaction?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_out, R.anim.fade_in)
        fragmentTransaction?.replace(containerId, fragment, fragment::class.simpleName)
        fragmentTransaction?.commit()
    }

    override fun replaceFragment(newFragment: Fragment, animate: Boolean) {
        var fragment = newFragment
        val fragmentTransaction = supportFragmentManager?.beginTransaction()
        val oldFragment = supportFragmentManager?.findFragmentByTag(fragment::class.simpleName)

        val currentFragment = getCurrentFragment()
        setFragmentFadeTransition(currentFragment)

        if (oldFragment != null) {
            fragment = oldFragment
        }

        if (animate) fragmentTransaction?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out,
                R.anim.fade_out, R.anim.fade_in)
        fragmentTransaction?.replace(containerId, fragment, fragment::class.simpleName)
        fragmentTransaction?.commit()
    }

    override fun addFragment(newFragment: Fragment) {
        var fragment = newFragment
        val fragmentTransaction = supportFragmentManager?.beginTransaction()
        val oldFragment = supportFragmentManager?.findFragmentByTag(fragment::class.simpleName)
        val currentFragment = getCurrentFragment()

        if (oldFragment != null) {
            fragment = oldFragment
        }

        setFragmentFadeTransition(currentFragment)
        setFragmentSlideInOutTransition(fragment)

//        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
//                        R.anim.enter_from_left, R.anim.exit_to_right)
        fragmentTransaction?.replace(containerId, fragment, fragment::class.simpleName)
        fragmentTransaction?.addToBackStack(fragment::class.simpleName)
        fragmentTransaction?.commit()
    }

    override fun addFragmentWithAdd(newFragment: Fragment) {
        var fragment = newFragment
        val fragmentTransaction = supportFragmentManager?.beginTransaction()
        val oldFragment = supportFragmentManager?.findFragmentByTag(fragment::class.simpleName)
        val currentFragment = getCurrentFragment()

        if (oldFragment != null) {
            fragment = oldFragment
        }

        setFragmentFadeTransition(currentFragment)
        setFragmentSlideInOutTransition(fragment)

//        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
//                        R.anim.enter_from_left, R.anim.exit_to_right)

        try {
            fragmentTransaction?.add(containerId, fragment, fragment::class.simpleName)
            fragmentTransaction?.addToBackStack(fragment::class.simpleName)
            fragmentTransaction?.commit()
        }
        catch (e: IllegalStateException){}
    }

    fun setFragmentFadeTransition(fragment: Fragment?){
        val exitFade = Fade()
        exitFade.duration = 250
        fragment?.exitTransition = exitFade
        fragment?.enterTransition = exitFade
        fragment?.returnTransition = exitFade
    }

    fun setFragmentSlideInOutTransition(fragment: Fragment){
        val slideIn = Slide()
        slideIn.slideEdge = Gravity.RIGHT
        slideIn.mode = Visibility.MODE_IN
        slideIn.duration = 250

        val slideReturn = Slide()
        slideReturn.duration = 250
        slideReturn.slideEdge = Gravity.RIGHT
        slideReturn.mode = Visibility.MODE_OUT

        fragment.enterTransition = slideIn
        fragment.returnTransition = slideReturn
    }

    override fun popBackStackTillEntry(entryIndex: Int) {
        if (supportFragmentManager.backStackEntryCount <= entryIndex) {
            return
        }
        val entry = supportFragmentManager.getBackStackEntryAt(entryIndex)
        if (entry != null) {
            supportFragmentManager?.popBackStackImmediate(entry.id,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }


    private fun getCurrentFragment(): Fragment?{
        return supportFragmentManager.findFragmentById(containerId)
    }
}