package com.notyteam.aist.android.ui.fragments.registration.sms

import android.util.Log
import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter
import com.notyteam.aist.android.utils.paper.PaperIO
import im.aist.core.api.ApiAuthSession
import im.aist.core.api.rpc.ResponseAuth
import im.aist.core.entity.AuthRes
import im.aist.core.entity.Sex
import im.aist.core.network.RpcException
import im.aist.core.network.api.ApiBroker
import im.aist.core.viewmodel.CommandCallback
import im.aist.sdk.aist.ActorSDK
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.concurrent.TimeUnit

class SmsFragmentPresenter(view: SmsFragmentView?) : BasePresenter<SmsFragmentView>(view) {

    private var timerDisposable: Disposable? = null
    val timerDuration = 60L

    override fun detachView() {
        super.detachView()
        timerDisposable?.dispose()
    }

    private fun getSessionId() {
        ActorSDK.sharedActor().messenger.loadSessions().start(
                object : CommandCallback<List<ApiAuthSession>> {
                    override fun onResult(res: List<ApiAuthSession>) {
                        val session: ApiAuthSession
                        val filteredList = res.filter { it.authHolder.name == "THISDEVICE"}
                        if (filteredList.isNotEmpty())
                            session = filteredList.first()
                        else
                        session = res.first()
                        PaperIO.setSessionId(session.id)
                        PaperIO.setAuthId(session.authHolder.value)

                        sendUserAuthParameters()
                    }
                    override fun onError(e: java.lang.Exception) {
                        view?.onValidateError(e)
                    }
                })
    }

    fun startSmsResendTimer() {
        timerDisposable?.dispose()
        timerDisposable = Observable.interval(1, TimeUnit.SECONDS, Schedulers.io())
                .take(timerDuration)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { view?.onResendTimerTick(it) }, { },
                        { view?.onResendTimerCompleted() }
                )
    }

    fun stopTimer() {
        timerDisposable?.dispose()
        view?.onResendTimerCompleted()
    }

    fun validateCode(hash: String, code: String) {
        ActorSDK.sharedActor().messenger.doValidateCode(code, hash).then { authCodeRes ->
            if (authCodeRes.isNeedToSignup) signUp(authCodeRes.transactionHash)
            else completeAuth(authCodeRes.result)

        }.failure {
            checkError(it)

        }
    }

    private fun signUp(hash: String) {
        ActorSDK.sharedActor().messenger.doSignup("AIST", Sex.UNKNOWN, hash).then { authRes ->
            completeAuth(authRes)
        }.failure {
            checkError(it)
        }

    }

    private fun completeAuth(authRes: AuthRes) {
        ActorSDK.sharedActor().messenger.doCompleteAuth(authRes).then { res ->
            saveProfileData(authRes)
        }.failure {
            checkError(it)

            Log.d("myLogs","error "+ it.message);
        }
    }

    private fun checkError(e: Exception) {
        (e as RpcException).apply {
            if (tag == "EMAIL_CODE_INVALID" || tag == "PHONE_CODE_INVALID") {
                view?.onInvalidCode(this)
            } else if (tag == "EMAIL_CODE_EXPIRED" || tag == "PHONE_CODE_EXPIRED") {
                view?.onCodeExpired(this)
            } else view?.onValidateError(this)
        }
    }

    private fun sendUserAuthParameters(){
        requests.add(aistApi.authUser({
            view?.onValidateSuccess()
        }, { message, code ->
            view?.onCompleteAuthError(message)
        }).sendRequest())
    }

    private fun saveProfileData(authRes: AuthRes) {
        val auth: ResponseAuth
        try {
            auth = ResponseAuth.fromBytes(authRes.data)
            PaperIO.setAuthData(auth)
            getSessionId()
        } catch (e: IOException) {
            e.printStackTrace()
            return
        }
    }
}