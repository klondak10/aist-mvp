package com.notyteam.aist.android.api.response.catalog

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.notyteam.aist.android.api.response.interest.all_interests.DataItem
import com.notyteam.aist.android.api.response.responses.BaseResponse

@Generated("com.robohorse.robopojogenerator")
open class CatalogListResponse(
		@field:SerializedName("data")
		val data: ArrayList<CategoriesItem> = arrayListOf()
): BaseResponse()