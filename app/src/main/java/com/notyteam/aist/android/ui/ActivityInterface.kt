package com.notyteam.aist.android.ui

import android.support.annotation.DrawableRes

interface ActivityInterface {
    fun setVisibleToolbar(isVisible: Boolean)

    fun setLeftButtonEnabled(isEnabled: Boolean)

    fun setRightButtonEnabled(isEnabled: Boolean)

    fun setSearchButtonEnabled(isEnabled: Boolean)

    fun setLeftIconListener(body: () -> Unit)

    fun setRightIconListener(body: () -> Unit)

    fun setLeftIcon(@DrawableRes res: Int)

    fun setRightIcon(@DrawableRes res: Int)

    fun setTransparentActionBar(isTransaprent: Boolean)

    fun setBarTitle(title: String)

    fun setTopMarginEnabled(isVisible: Boolean)

    companion object {
        val empty: ActivityInterface = object : ActivityInterface {
            override fun setVisibleToolbar(isVisible: Boolean) {

            }

            override fun setTransparentActionBar(isTransaprent: Boolean) {
            }

            override fun setBarTitle(title: String) {
            }

            override fun setLeftIconListener(body: () -> Unit) {

            }

            override fun setLeftButtonEnabled(isEnabled: Boolean) {
            }

            override fun setRightButtonEnabled(isEnabled: Boolean) {
            }

            override fun setRightIconListener(body: () -> Unit) {
            }

            override fun setLeftIcon(res: Int) {
            }

            override fun setRightIcon(res: Int) {
            }

            override fun setSearchButtonEnabled(isEnabled: Boolean){

            }

            override fun setTopMarginEnabled(isVisible: Boolean){

            }
        }
    }
}