package com.notyteam.aist.android.mvp

interface PresenterDetachInterface {
    fun presenterDetach()
}