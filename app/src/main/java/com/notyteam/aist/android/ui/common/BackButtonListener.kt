package com.notyteam.aist.android.ui.common

interface BackButtonListener {
    fun onBackPressed(): Boolean
}