package com.notyteam.aist.android

import android.support.v4.app.Fragment
import com.notyteam.aist.android.ui.fragments.BaseFragment


import com.notyteam.aist.android.ui.fragments.container.*


import ru.terrakok.cicerone.android.support.SupportAppScreen


object Screens {
    enum class MainScreens {
        DESIRES_SCREEN,
        NOTIFICATIONS_SCREEN,
        DIALOGUES_SCREEN,
        PAYMENT_SCREEN,
        CATALOG_SCREEN
    }

    class TabScreen(val tabScreen: MainScreens) : SupportAppScreen() {

        override fun getFragment(): Fragment {
            when(tabScreen){
                MainScreens.DESIRES_SCREEN -> return DesiresFragmentContainer.getNewInstance(tabScreen.name)
                MainScreens.NOTIFICATIONS_SCREEN -> return NotificationsFragmentContainer.getNewInstance(tabScreen.name)
                MainScreens.DIALOGUES_SCREEN -> return DialoguesFragmentContainer.getNewInstance(tabScreen.name)
                MainScreens.PAYMENT_SCREEN -> return PaymentFragmentContainer.getNewInstance(tabScreen.name)
                MainScreens.CATALOG_SCREEN -> return CatalogFragmentContainer.getNewInstance(tabScreen.name)
                else -> return DesiresFragmentContainer.getNewInstance(tabScreen.name)
            }
        }
    }

    class FragmentScreen<T: BaseFragment>(var fragmento : T): SupportAppScreen() {
        override fun getFragment(): Fragment {
            return fragmento
        }
    }
}