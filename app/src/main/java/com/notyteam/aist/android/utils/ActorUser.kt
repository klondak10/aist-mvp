package com.notyteam.aist.android.utils

import im.aist.core.viewmodel.UserVM
import im.aist.sdk.util.ActorSDKMessenger
import java.lang.Exception

object ActorUser {
    fun getActorUser(): UserVM? {
        return try {
            ActorSDKMessenger.messenger().getUser(ActorSDKMessenger.messenger().myUid())
        }
        catch (e: Exception){
            null
        }
    }
}