package com.notyteam.aist.android.ui.adapters.catalog

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView

import com.bumptech.glide.Glide
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import com.notyteam.aist.android.ui.models.ui.header.ContentItem
import com.notyteam.aist.android.ui.models.ui.header.Header
import com.notyteam.aist.android.ui.models.ui.header.ListItem

class GoodsAdapterWithHeader(internal var context: Context,
                             internal var list: List<ListItem>,
                             var onClick :(data: ContentItem ) -> Unit = {}) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        if (viewType == TYPE_HEADER) {
            val v = inflater.inflate(R.layout.item_header_goods_catalog, parent, false)
            return VHHeader(v)
        } else {
            val v = inflater.inflate(R.layout.item_goods_catalog, parent, false)
            return VHItem(v)
        }

    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is VHHeader) {
            // VHHeader VHheader = (VHHeader)holder;
            val currentItem = list[position] as Header
            holder.txtTitle.text = currentItem.header
        } else if (holder is VHItem) {


            val currentItem = list[position] as ContentItem
            holder.name_goods.text = currentItem.getName()
            holder.price_goods.text = currentItem.getPrice().toString() + ".0 " + Html.fromHtml(" &#x20bd")
            Glide.with(context)
                    .load(currentItem.getImages()[0])
                    .into(holder.image_goods)

            if (currentItem.is_favorite) {
                holder.fav_check.setImageResource(R.drawable.bottom_wish)
                holder.fav_check.setColorFilter(R.color.colorBlack)
            } else {
                holder.fav_check.setImageResource(R.drawable.ic_heart)
            }

            holder.main_goods.setOnClickListener {
                if (position >= 0) {
                    onClick(currentItem)
                }
            }


        }

    }


    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) TYPE_HEADER else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {

        return list[position] is Header

    }

    override fun getItemCount(): Int {
        return list.size
    }

    internal inner class VHHeader(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView

        init {
            this.txtTitle = itemView.findViewById<View>(R.id.txtHeader) as TextView
        }
    }

    internal inner class VHItem(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name_goods: TextView
        var price_goods: TextView
        var image_goods: ImageView
        var fav_check: ImageView
        var main_goods: LinearLayout
        init {

            this.name_goods = itemView.findViewById(R.id.name_goods)
            this.price_goods = itemView.findViewById(R.id.price_goods)
            this.image_goods = itemView.findViewById(R.id.image_goods)
            this.fav_check = itemView.findViewById(R.id.fav_check)
            this.main_goods = itemView.findViewById(R.id.main_goods)



        }
    }

    companion object {
        private val TYPE_HEADER = 0
        private val TYPE_ITEM = 1
    }
}