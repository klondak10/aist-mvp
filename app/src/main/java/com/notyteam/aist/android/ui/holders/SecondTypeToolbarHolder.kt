package com.notyteam.aist.android.ui.holders

import android.support.annotation.DrawableRes
import android.view.View
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.toolbar.view.*

class SecondTypeToolbarHolder(override val containerView: View?): LayoutContainer {

    fun showSecondTypeToolbar():SecondTypeToolbarHolder  {
        containerView?.visibility = View.VISIBLE
        containerView?.toolbar_layout_1?.visibility = View.GONE
        containerView?.toolbar_layout_3?.visibility = View.GONE
        containerView?.toolbar_layout_2?.visibility = View.VISIBLE
        toggleLeftIcon(isVisible = true)
        toggleRightIcon(isVisible = true)
        setTitle("")
        return this
    }

    fun setTitle(title: String):SecondTypeToolbarHolder {
        containerView?.screen_name_2?.text = title
        return this
    }

    fun setOnLeftIconListener(body: () -> Unit):SecondTypeToolbarHolder {
        containerView?.button_left_2?.setOnClickListener { body() }
        return this
    }

    fun setLeftIcon(res: Int):SecondTypeToolbarHolder {
        containerView?.button_left_2?.setImageResource(res)
        return this
    }

    fun toggleLeftIcon(isVisible: Boolean):SecondTypeToolbarHolder {
        containerView?.button_left_2?.visibility = if (isVisible) View.VISIBLE else View.GONE
        return this
    }

    fun setOnRightIconListener(body: () -> Unit):SecondTypeToolbarHolder {
        containerView?.button_right_2?.setOnClickListener { body() }
        return this
    }

    fun setRightIcon(res: Int):SecondTypeToolbarHolder {
        containerView?.button_right_2?.setImageResource(res)
        return this
    }

    fun toggleRightIcon(isVisible: Boolean):SecondTypeToolbarHolder {
        containerView?.button_right_2?.visibility = if (isVisible) View.VISIBLE else View.GONE
        return this
    }

    fun toggleVertIcon(isVisible: Boolean):SecondTypeToolbarHolder {
        containerView?.button_right_3?.visibility = if (isVisible) View.VISIBLE else View.GONE
        return this
    }

    fun setOnVertIconListener(body: () -> Unit):SecondTypeToolbarHolder {
        containerView?.button_right_3?.setOnClickListener { body() }
        return this
    }
}