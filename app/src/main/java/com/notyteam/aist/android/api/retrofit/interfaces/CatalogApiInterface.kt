package com.notyteam.aist.android.api.retrofit.interfaces

import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CatalogListResponseById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import io.reactivex.Single
import retrofit2.http.*

interface CatalogApiInterface {

    @GET("catalog/categories")
    fun getCategoriesList(): Single<CatalogListResponse>

    @GET("catalog/categories/{category_id}")
    fun getCategoriesListById(@Path("category_id") categoryId: Int): Single<CatalogListResponseById>

    @GET("catalog/categories/{category_id}/products?page=1&per_page=500")
    fun getCategoriesListByIdPagePerPage(@Path("category_id") categoryId: Int): Single<CatalogListResponse>

    @GET("catalog/products/{product_id}")
    fun getGoodsById(@Path("product_id") categoryId: Int): Single<GoodsResponseById>

    @GET("catalog/products/search"+"?page=1&per_page=500")
    fun getSearchList( @Query("query") subject: String): Single<CatalogListResponse>

}