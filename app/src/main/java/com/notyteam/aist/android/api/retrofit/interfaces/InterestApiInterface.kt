package com.notyteam.aist.android.api.retrofit.interfaces

import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.*

interface InterestApiInterface {

    @GET("interests")
    fun getInterestsList(): Single<InterestsListResponse>

    @GET("users/{user_id}/interests")
    fun getUserInterestsList(@Path("user_id") userId: Int): Single<InterestsListResponse>

    @POST("users/{user_id}/interests")
    fun setUserInterestsList(@Path("user_id") userId: Int,
                             @Body data: RequestBody): Single<InterestsListResponse>

    @GET("interests/{interest_id}/products")
    fun getUserGoodsList(@Path("interest_id") interest_id: Int): Single<CatalogListResponse>

}