package com.notyteam.aist.android.ui.fragments.registration.phone

import com.notyteam.aist.android.mvp.PresenterDetachInterface
import im.aist.core.entity.AuthStartRes
import im.aist.runtime.promise.Promise

interface PhoneFragmentView: PresenterDetachInterface {
    fun onPhoneResult(hash: String)
    fun onPhoneError(e: Exception)
}