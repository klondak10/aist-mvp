package com.notyteam.aist.android.ui.fragments.main.catalog.goods


import android.os.Bundle
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CategoriesListById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.ui.adapters.catalog.CategoriesAdapter
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.view.CatalogFragmentView
import com.notyteam.aist.android.ui.fragments.main.catalog.presenter.CatalogByIdFragmentPresenter
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import com.notyteam.aist.android.utils.recyclerViewVertical

import kotlinx.android.synthetic.main.fragment_dialogs_main.*

class CategoryListGoodsByIdFragment() : BaseFragment(), CatalogFragmentView {

    override fun onFavoritesError(error: String) {}
    override fun onFavoritesSuccess(success: Boolean) {}
    override fun onGoodsByIdSuccess(data: GoodsResponseById) {}
    override fun onCatalogListSuccessPagePerPage(list: CatalogListResponse) {}
    override fun onCatalogListSuccess(list: CatalogListResponse) { }

    override fun onCatalogListByIdSuccess(list: CategoriesListById) {
        showProgress(false)
        val mappedList = list.sub_categories.flatMap { category ->
            arrayListOf( UICategoriesCatalog(category.is_favorite,category.in_wish_list,category.name,category.has_sub_categories, category.id) )
        } as ArrayList
        adapter.clear()
        adapter.addAll(mappedList)
    }



    companion object {
        private const val POSITION_KEY = "POSITION_KEY"
        private const val NAME_CATEGORY_KEY = "NAME_CATEGORY_KEY"
        private const val NAME_KEY = "NAME_KEY"
        fun newInstance(position:Int,nameCategory:String,name:String): CategoryListGoodsByIdFragment {
            val f = CategoryListGoodsByIdFragment ()
            // Pass index input as an argument.
            val args = Bundle()
            args.putInt(POSITION_KEY, position)
            args.putString(NAME_CATEGORY_KEY, nameCategory)
            args.putString(NAME_KEY, name)
            f.setArguments(args)
            return f
        }

        fun getPosition(arguments: Bundle?) = arguments?.getInt(POSITION_KEY, 0)?:0
        fun getNameCategory(arguments: Bundle?) = arguments?.getString(NAME_CATEGORY_KEY)?:""
        fun getName(arguments: Bundle?) = arguments?.getString(NAME_KEY)?:""
    }

    private var presenter = CatalogByIdFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }
    lateinit var adapter: CategoriesAdapter

    override fun layout(): Int = R.layout.fragment_dialogs_main


    override fun initialization(view: View, isFirstInit: Boolean) {

        if(getNameCategory(arguments).length<=20){
            getThirdTypeToolbar()
                    ?.setLeftText(getName(arguments))
                    ?.setTitle(getNameCategory(arguments))
                    ?.setOnBackPressedListener{
                        onBackPressed()
                    }
        }else{
            getThirdTypeToolbar()
                    ?.setLeftText("")
                    ?.setTitle(getNameCategory(arguments))
                    ?.setOnBackPressedListener{
                        onBackPressed()
                    }
        }
        activityBottomBarInterface.showBottomBar(true)

        if (isFirstInit) {
            initViews()
        }

    }

    private fun initViews() {
        showProgress(true)
        presenter.getCategoryListById(getPosition(arguments))
        recyclerViewVertical(rv_dialogs)
        adapter = CategoriesAdapter(arrayListOf(),
            onClick = {
                if(it.has_sub_categories==true){
                    addFragment(newInstance(it.id,it.name,""))
                }else{
                    addFragment(GoodsListFragment.newInstance(it.id,it.name,getString(R.string.back)))
                }
            }
        )
        rv_dialogs.adapter = adapter
    }

    override fun onCatalogListError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }

}
