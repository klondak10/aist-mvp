package com.notyteam.aist.android.api.retrofit.api

import android.content.Context
import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CatalogListResponseById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.api.response.responses.TokenResponse
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.api.response.responses.BaseResponse
import com.notyteam.aist.android.api.retrofit.CallbackWrapper
import com.notyteam.aist.android.api.retrofit.interfaces.*
import com.notyteam.aist.android.utils.paper.PaperIO
import io.paperdb.Paper
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Retrofit
import java.lang.ref.WeakReference

class AistApi(private val context: WeakReference<Context>, private val retrofit: Retrofit) {

    private val wishApiInterface: WishApiInterface by lazy {
        retrofit.create(WishApiInterface::class.java)
    }

    private val authApiInterface: AuthApiInterface by lazy {
        retrofit.create(AuthApiInterface::class.java)
    }

    private val favoritesApiInterface: FavoritesApiInterface by lazy {
        retrofit.create(FavoritesApiInterface::class.java)
    }

    private val interestApiInterface: InterestApiInterface by lazy {
        retrofit.create(InterestApiInterface::class.java)
    }

    private val tokenApiInterface: TokenApiInterface by lazy {
        retrofit.create(TokenApiInterface::class.java)
    }


    private val catalogApiInterface: CatalogApiInterface by lazy {
        retrofit.create(CatalogApiInterface::class.java)
    }


    fun authUser(onSuccess: (response: BaseResponse) -> Unit,
                 onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<BaseResponse> {
        val userId = PaperIO.getUserId()
        val authId = PaperIO.getAuthId()
        val sessionId = PaperIO.getSessionId()
        return initRequestSingleForResult(authApiInterface.authUser(userId, authId, sessionId), onSuccess, onError)
    }

    fun setToken(onSuccess: (response: TokenResponse) -> Unit,
                 onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<TokenResponse> {

        val  token = Paper.book().read<String>("token")
        return initRequestSingleForResult(tokenApiInterface.setToken(token), onSuccess, onError)

    }

    fun getAllInterestsList(onSuccess: (response: InterestsListResponse) -> Unit,
                            onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<InterestsListResponse> {
        return initRequestSingleForResult(interestApiInterface.getInterestsList(), onSuccess, onError)
    }

    fun getUserInterestsList(onSuccess: (response: InterestsListResponse) -> Unit,
                         onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<InterestsListResponse> {
        return initRequestSingleForResult(interestApiInterface.getUserInterestsList(PaperIO.getUserId()), onSuccess, onError)
    }

    fun setUserInterestsList(interestIds: ArrayList<Int>,
                             onSuccess: (response: InterestsListResponse) -> Unit,
                             onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<InterestsListResponse> {
        val data: HashMap<String, Any> = hashMapOf("interest_ids" to JSONArray(interestIds))
        return initRequestSingleForResult(interestApiInterface.setUserInterestsList(PaperIO.getUserId(), getJson(data)), onSuccess, onError)
    }

    private fun <T> initRequestSingleForResult(request: Single<T>,
                                                  onSuccess: (response: T) -> Unit,
                                                  onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<T> {
        val callback = CallbackWrapper<T, T>(context, retrofit, onSuccess, onError)
        val requestSingle = request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        callback.request = requestSingle
        return RequestHolder<T>(requestSingle, callback)
    }

    fun zip(requests: ArrayList<Single<*>>, onLoadingEnded: () -> Unit){
        Single.zip(requests) { result ->  }.subscribe({onLoadingEnded()}, {})
    }

    private fun getJson(data: HashMap<String, Any>): RequestBody {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"),
                (JSONObject(data)).toString())
    }

    class RequestHolder<T>(private val request: Single<T>, private val callback: CallbackWrapper<T, T>){
        fun sendRequest() = request.subscribeWith(callback)
        fun getRequest() = request.doOnSuccess {callback.onSuccess(it)}.doOnError {callback.onError(it)}
    }

    fun getCategoriesList(onSuccess: (response: CatalogListResponse) -> Unit,
                            onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<CatalogListResponse> {
        return initRequestSingleForResult(catalogApiInterface.getCategoriesList(), onSuccess, onError)
    }

    fun getCategoriesListById(id:Int, onSuccess: (response: CatalogListResponseById) -> Unit,
                              onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<CatalogListResponseById> {
        return initRequestSingleForResult(catalogApiInterface.getCategoriesListById(id), onSuccess, onError)
    }

    fun getCategoriesListPagePerPage(id:Int,onSuccess: (response: CatalogListResponse) -> Unit,
                          onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<CatalogListResponse> {
        return initRequestSingleForResult(catalogApiInterface.getCategoriesListByIdPagePerPage(id), onSuccess, onError)
    }

    fun getGoodsById(id:Int, onSuccess: (response: GoodsResponseById) -> Unit,
                     onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<GoodsResponseById> {
        return initRequestSingleForResult(catalogApiInterface.getGoodsById(id), onSuccess, onError)
    }

    fun getSearchList(query:String,onSuccess: (response: CatalogListResponse) -> Unit,
                                     onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<CatalogListResponse> {
        return initRequestSingleForResult(catalogApiInterface.getSearchList(query), onSuccess, onError)
    }

    fun getFavoritesList(user_id:Int,onSuccess: (response: CatalogListResponse) -> Unit,
                                     onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<CatalogListResponse> {
        return initRequestSingleForResult(favoritesApiInterface.getFavoritesList(user_id), onSuccess, onError)
    }

    fun addFavorites(user_id:Int,product_id:Int,onSuccess: (response: BaseResponse) -> Unit,
                         onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<BaseResponse> {
        return initRequestSingleForResult(favoritesApiInterface.addFavorites(user_id,product_id), onSuccess, onError)
    }
    fun deleteFavorites(user_id:Int,favorite_id:Int,onSuccess: (response: BaseResponse) -> Unit,
                        onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<BaseResponse> {
        return initRequestSingleForResult(favoritesApiInterface.deleteFavorites(user_id,favorite_id), onSuccess, onError)
    }


    fun getWishList(user_id:Int,onSuccess: (response: CatalogListResponse) -> Unit,
                         onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<CatalogListResponse> {
        return initRequestSingleForResult(wishApiInterface.getWishList(user_id), onSuccess, onError)
    }

    fun addWish(user_id:Int,product_id:Int,onSuccess: (response: BaseResponse) -> Unit,
                     onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<BaseResponse> {
        return initRequestSingleForResult(wishApiInterface.addWish(user_id,product_id), onSuccess, onError)
    }
    fun deleteWish(user_id:Int,wish_id:Int,onSuccess: (response: BaseResponse) -> Unit,
                        onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<BaseResponse> {
        return initRequestSingleForResult(wishApiInterface.deleteWish(user_id,wish_id), onSuccess, onError)
    }

    fun getGoodsListForMe(interest_id:Int, onSuccess: (response: CatalogListResponse) -> Unit,
                              onError: (errorMessage: String, errorCode: Int) -> Unit): RequestHolder<CatalogListResponse> {
        return initRequestSingleForResult(interestApiInterface.getUserGoodsList(interest_id), onSuccess, onError)
    }

}