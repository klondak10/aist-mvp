package com.notyteam.aist.android.ui.fragments.main.catalog.goods

import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CategoriesListById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.ui.adapters.catalog.GoodsAdapterWithHeader
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.view.CatalogFragmentView
import com.notyteam.aist.android.ui.fragments.main.catalog.presenter.GoodsListForMeFragmentPresenter
import com.notyteam.aist.android.ui.models.ui.header.ContentItem
import com.notyteam.aist.android.ui.models.ui.header.Header
import com.notyteam.aist.android.ui.models.ui.header.ListItem
import com.notyteam.aist.android.ui.sticky_recycle_view.Section.HEADER
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_dialogs_main.*

class ForMeGoodsFragment : BaseFragment(), CatalogFragmentView {

    val arrayList = arrayListOf<ListItem>()

    override fun onCatalogListError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }
    override fun onFavoritesError(error: String) {}
    override fun onFavoritesSuccess(success: Boolean) {}
    override fun onGoodsByIdSuccess(data: GoodsResponseById) {}
    override fun onCatalogListByIdSuccess(list: CategoriesListById) {}
    override fun onCatalogListSuccess(list: CatalogListResponse) {}

    override fun onCatalogListSuccessPagePerPage(list: CatalogListResponse) {

        showProgress(false)

        val mappedList = list.data.flatMap { category ->
            arrayListOf( ContentItem(category.name,category.images,category.is_favorite,category.price,category.id) )
        } as ArrayList
        arrayList.clear()
        var header  = Header()
        header.setHeader("Гаджеты");
        arrayList.add(header);
        arrayList.addAll(mappedList)

        adapter = GoodsAdapterWithHeader(baseActivity, arrayList,
                onClick = {
                    addFragment(GoodsByIdFragment.newInstance(it.id))
                }
        )
        try { rv_dialogs.adapter = adapter }catch (e: IllegalStateException){}

   }

    private var presenter = GoodsListForMeFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }
    lateinit var adapter: GoodsAdapterWithHeader
    companion object {
        fun newInstance(): ForMeGoodsFragment {
            return ForMeGoodsFragment()
        }
    }

    override fun layout(): Int = R.layout.fragment_dialogs_main

    override fun initialization(view: View, isFirstInit: Boolean) {

        initViews()
    }

    private fun initViews() {
        showProgress(true)

        //var  InterestsList = Paper.book().read("interestsList")  as ArrayList<DataItem>


        /*InterestsList.forEach {

            var header  = Header()
            header.setHeader(it.translatedName);
            arrayList.add(header);
        }*/
        presenter.getGoodsListForMe(2)

        val glm = GridLayoutManager(context, 2)
        glm.setSpanSizeLookup(object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                   val type = adapter.getItemViewType(position)
                return if (type == HEADER)
                    2
                else
                    1
            }
        })
        rv_dialogs.setLayoutManager(glm)



    }


}
