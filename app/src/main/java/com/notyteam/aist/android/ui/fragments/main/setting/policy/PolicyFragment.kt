package com.notyteam.aist.android.ui.fragments.main.setting.policy

import android.graphics.Bitmap
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import im.delight.android.webview.AdvancedWebView
import kotlinx.android.synthetic.main.fragment_goods_add_to_cart.*
import kotlinx.android.synthetic.main.fragment_policy.*
import kotlinx.android.synthetic.main.fragment_policy.mWebView

class PolicyFragment : BaseFragment(), PolicyFragmentView, AdvancedWebView.Listener {
    companion object {
        fun newInstance(): PolicyFragment {
            return PolicyFragment()
        }
    }

    override fun onPageFinished(url: String?) {
        showProgress(false)
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        showProgress(false)
        showErrorSnack(description.toString())
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {}

    override fun onExternalPageRequest(url: String?) {}

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
        showProgress(true)
    }

    override fun layout(): Int = R.layout.fragment_policy

    override fun initialization(view: View, isFirstInit: Boolean) {
        getFirstTypeToolbar()
                ?.setLeftText(getString(R.string.back))
                ?.setOnBackPressedListener {
                    onBackPressed()
                }
        if (isFirstInit) {
            initViews()
        }
    }
    private var presenter: PolicyFragmentPresenter = PolicyFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    private fun initViews() {
        mWebView.settings.javaScriptEnabled = true
        mWebView.webChromeClient = WebChromeClient()
        mWebView.webViewClient = WebViewClient()
        mWebView.setListener(baseActivity, this);
        mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mWebView.loadUrl("http://aist.im/privacy")
    }

}
