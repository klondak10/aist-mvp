package com.notyteam.aist.android.ui.activities.auth

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatDelegate
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.activities.BaseActivity
import com.notyteam.aist.android.ui.fragments.registration.phone.PhoneFragment
import com.notyteam.aist.android.ui.fragments.tutorial.TutorialFragment
import com.notyteam.aist.android.ui.holders.ToolbarHolder
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : BaseActivity() {

    companion object {
        const val OPEN_LOGIN = "OPEN_LOGIN"
        fun start(c: Context, shouldOpenLogin: Boolean = false) {
            val intent = Intent(c, AuthActivity::class.java)
            intent.putExtra(OPEN_LOGIN, shouldOpenLogin)
            c.startActivity(intent)
        }
    }

    override fun layout(): Int = R.layout.activity_auth

    override fun initialization() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        toolbarHolder = ToolbarHolder(toolbar_layout)
        val shouldOpenLogin = intent?.getBooleanExtra(OPEN_LOGIN, false)?:false
        replaceFragment(if (!shouldOpenLogin) TutorialFragment() else PhoneFragment())
    }

}