package com.notyteam.aist.android.ui.models.responce.countries

import com.google.gson.annotations.SerializedName

data class CountiesResponse(

	@field:SerializedName("countries")
	val countries: List<CountriesItem> = arrayListOf()
)