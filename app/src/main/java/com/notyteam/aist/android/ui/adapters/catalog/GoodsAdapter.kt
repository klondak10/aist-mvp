package com.notyteam.aist.android.ui.adapters.catalog

import android.text.Html
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.adapters.BaseAdapter
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import com.notyteam.aist.android.utils.getColorResource
import kotlinx.android.extensions.LayoutContainer

class GoodsAdapter(list: MutableList<UICategoriesCatalog>,
                   var onClick :(data: UICategoriesCatalog) -> Unit = {}) :
        BaseAdapter<UICategoriesCatalog, GoodsAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int): ViewHolder = ViewHolder(view)

    override fun getItemViewType(position: Int): Int = R.layout.item_goods_catalog

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {

        init {
            itemView.setOnClickListener {
                if (adapterPosition >= 0) {
                    onClick(list[adapterPosition])
                }
            }
        }

        override val containerView: View? get() = itemView

        override fun bind(pos: Int) {
            val name_goods = itemView.findViewById<TextView>(R.id.name_goods)
            name_goods.text = list[pos].name

            val price_goods = itemView.findViewById<TextView>(R.id.price_goods)
            price_goods.text = list[pos].price.toString()+".0 "+ Html.fromHtml(" &#x20bd")

            val image_goods = itemView.findViewById<ImageView>(R.id.image_goods)

            val fav_check = itemView.findViewById<ImageView>(R.id.fav_check)

            Glide.with(image_goods.context)
                    .load(list[pos].images[0])
                    .into(image_goods);
            if(list[pos].is_favorite!!){
                fav_check.setImageResource(R.drawable.bottom_wish)
                fav_check.setColorFilter(R.color.colorBlack)
            }else{
                fav_check.setImageResource(R.drawable.ic_heart)
            }

        }
    }
}