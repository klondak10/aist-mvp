package com.notyteam.aist.android.ui

interface FragmentInterface {
    fun setVisibleToolbar(isVisible: Boolean)

    fun setBarTitle(title: String)

    fun setTopMarginEnabled(isVisible: Boolean)



    fun setLeftButtonEnabled(isEnabled: Boolean)

    fun setClearButtonEnabled(isEnabled: Boolean)

    fun setShareButtonEnabled(isEnabled: Boolean)

    fun setBasketButtonEnabled(isEnabled: Boolean)

    fun setSearchButtonEnabled(isEnabled: Boolean)

    fun setScanButtonEnabled(isEnabled: Boolean)



    fun setLeftButtonListener(body: () -> Unit)

    fun setClearButtonListener(body: () -> Unit)

    fun setShareButtonListener(body: () -> Unit)

    fun setBasketButtonistener(body: () -> Unit)

    fun setSearchButtonListener(body: () -> Unit)

    fun setScanButtonListener(body: () -> Unit)

    companion object {
        val empty: FragmentInterface = object : FragmentInterface {
            override fun setVisibleToolbar(isVisible: Boolean) {

            }

            override fun setBarTitle(title: String) {

            }

            override fun setTopMarginEnabled(isVisible: Boolean) {

            }

            override fun setLeftButtonEnabled(isEnabled: Boolean) {

            }

            override fun setLeftButtonListener(body: () -> Unit) {

            }

            override fun setClearButtonEnabled(isEnabled: Boolean) {

            }

            override fun setClearButtonListener(body: () -> Unit) {

            }

            override fun setShareButtonEnabled(isEnabled: Boolean) {

            }

            override fun setShareButtonListener(body: () -> Unit) {

            }

            override fun setBasketButtonistener(body: () -> Unit) {

            }

            override fun setBasketButtonEnabled(isEnabled: Boolean) {

            }

            override fun setSearchButtonListener(body: () -> Unit) {

            }

            override fun setSearchButtonEnabled(isEnabled: Boolean) {

            }

            override fun setScanButtonListener(body: () -> Unit) {

            }

            override fun setScanButtonEnabled(isEnabled: Boolean) {

            }
        }
    }
}