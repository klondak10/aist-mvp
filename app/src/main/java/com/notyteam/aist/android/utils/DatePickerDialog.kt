package com.notyteam.aist.android.utils

import android.app.Activity
import android.app.Dialog
import android.view.View
import android.view.Window
import android.widget.DatePicker
import android.widget.TextView
import com.noty.aist.android.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DatePickerDialog{

    private const val requestDatePattern = "yyyy-MM-dd"
    private const val uiDatePattern = "dd.MM.yyyy"
    private const val timePattern = "H:mm"
    private val requestDateFormat = SimpleDateFormat(requestDatePattern, getLocale())
    private val uiDateFormat = SimpleDateFormat(uiDatePattern, getLocale())
    private val timeFormat = SimpleDateFormat(timePattern, getLocale())

    fun getDateWithTime(date: String, time: String): String {
        var result = getRequestDate(date) + " " + time + ":00"
        return result
    }

    fun getCalendarFromTimeString(time: String): Calendar {
        var convertedTime = Date()
        try {
            convertedTime = timeFormat.parse(time)
        } catch (e: ParseException) {
        }
        val calendar = Calendar.getInstance(getLocale())
        calendar.time = convertedTime
        return calendar
    }

    fun getTimeStringFromCalendar(calendar: Calendar): String {
        var timeString = ""
        try {
            timeString = timeFormat.format(calendar.time)
        } catch (e: ParseException) {
        }
        return timeString
    }

    fun getCalendarFromString(date: String): Calendar {
        var convertedDate = Date()
        try {
            convertedDate = requestDateFormat.parse(date)
        } catch (e: ParseException) {
            try {
                convertedDate = uiDateFormat.parse(date)
            } catch (e: ParseException) {
            }
        }
        val calendar = Calendar.getInstance(getLocale())
        calendar.time = convertedDate
        return calendar
    }

    fun getRequestDate(calendar: Calendar): String {
        var dateString = ""
        try {
            dateString = requestDateFormat.format(calendar.time)
        } catch (e: ParseException) {
        }
        return dateString
    }

    fun getUiDate(calendar: Calendar): String {
        var dateString = ""
        try {
            dateString = uiDateFormat.format(calendar.time)
        } catch (e: ParseException) {
        }
        return dateString
    }

    fun getRequestDate(date: String): String {
        var convertedDate = Date()
        try {
            convertedDate = requestDateFormat.parse(date)
        } catch (e: ParseException) {
            try {
                convertedDate = uiDateFormat.parse(date)
            } catch (e: ParseException) {
            }
        }
        return requestDateFormat.format(convertedDate)
    }

    fun getUiDate(date: String): String {
        var convertedDate = Date()
        try {
            convertedDate = requestDateFormat.parse(date)
        } catch (e: ParseException) {
            try {
                convertedDate = uiDateFormat.parse(date)
            } catch (e: ParseException) {
            }
        }
        return uiDateFormat.format(convertedDate)
    }

    fun show(dateString: String, activity: Activity, onDateSelected: (calendar: Calendar, picker: Dialog) -> Unit) {
        val dialogDateBirth = Dialog(activity, R.style.DatePickerDialogTheme)
        dialogDateBirth.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogDateBirth.setContentView(R.layout.fragment_date_picker)

        val datePicker = dialogDateBirth.findViewById<View>(R.id.datePicker) as DatePicker
        val dialogButtonCancel = dialogDateBirth.findViewById<View>(R.id.cancel_layout) as TextView
        val dialogButtonOk = dialogDateBirth.findViewById<View>(R.id.ok_layout) as TextView
        dialogButtonCancel.setOnClickListener { _ -> dialogDateBirth.dismiss() }

        val currentCalendar = getCalendarFromString(dateString)
        datePicker.updateDate(
                currentCalendar.get(Calendar.YEAR),
                currentCalendar.get(Calendar.MONTH),
                currentCalendar.get(Calendar.DAY_OF_MONTH))

        dialogButtonOk.setOnClickListener { _ ->
            //            val month = datePicker.month + 1
//            val day = datePicker.dayOfMonth
//            val newMonth: String
//            val newDay: String
//            newMonth = if (month.toString().length == 1) {
//                "0$month"
//            } else {
//                month.toString()
//            }
//
//            newDay = if (day.toString().length == 1) {
//                "0$day"
//            } else {
//                day.toString()
//            }

            val selectedCalendar = Calendar.getInstance()
            selectedCalendar.set(datePicker.year, datePicker.month, datePicker.dayOfMonth)
//            val requestDateString = datePicker.year.toString() + "-" + newMonth + "-" + newDay
//            val uiDateString = newDay + "." + newMonth + "." + datePicker.year
            onDateSelected(selectedCalendar, dialogDateBirth)
        }
        dialogDateBirth.show()
    }
}