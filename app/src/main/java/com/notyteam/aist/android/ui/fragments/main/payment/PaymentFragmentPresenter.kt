package com.notyteam.aist.android.ui.fragments.main.payment

import com.google.gson.JsonObject
import com.notyteam.aist.android.AistApp
import com.notyteam.aist.android.AistApp.Companion.qrApi
import com.notyteam.aist.android.mvp.BasePresenter
import okhttp3.RequestBody
import org.json.JSONObject

class PaymentFragmentPresenter(view: PaymentFragmentView?): BasePresenter<PaymentFragmentView>(view) {

    fun findQr(filter:String) {
        requests.add(qrApi.findQr(filter,{
            view?.onFindQrSuccess(it)
        }, { errorMessage, errorCode ->
            view?.onFindQrError(errorMessage)
        }).sendRequest())
    }
    fun bindTheCardBuyer(id:String,data: RequestBody) {
        requests.add(qrApi.bindTheCardBuyer(id,data,{
            view?.onBindTheCardBuyerSuccess(it)
        }, { errorMessage, errorCode ->
            view?.onFindQrError(errorMessage)
        }).sendRequest())
    }

}