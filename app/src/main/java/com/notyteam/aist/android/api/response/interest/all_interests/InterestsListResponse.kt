package com.notyteam.aist.android.api.response.interest.all_interests

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.notyteam.aist.android.api.response.responses.BaseResponse

@Generated("com.robohorse.robopojogenerator")
open class InterestsListResponse(
		@field:SerializedName("data")
		val data: ArrayList<DataItem> = arrayListOf()
): BaseResponse()