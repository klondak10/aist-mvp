package com.notyteam.aist.android.ui.holders

import android.support.annotation.DrawableRes
import android.view.Gravity
import android.view.View
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.toolbar.view.*

class ThirdTypeToolbarHolder(override val containerView: View?): LayoutContainer {

    fun showThirdTypeToolbar():ThirdTypeToolbarHolder  {
        containerView?.visibility = View.VISIBLE
        containerView?.toolbar_layout_1?.visibility = View.GONE
        containerView?.toolbar_layout_2?.visibility = View.GONE
        containerView?.toolbar_layout_3?.visibility = View.VISIBLE
        setTitle("")
        setLeftText("")
        return this
    }

    fun setTitle(title: String):ThirdTypeToolbarHolder {
        containerView?.screen_name_3?.text = title
        return this
    }

    fun setLeftText(title: String):ThirdTypeToolbarHolder  {
        containerView?.previous_screen_name_3?.text = title
        return this
    }

    fun setOnLeftIconListener(body: () -> Unit):ThirdTypeToolbarHolder {
        containerView?.button_left_3?.setOnClickListener { body() }
        return this
    }

    fun setLeftIcon(res: Int):ThirdTypeToolbarHolder {
        containerView?.button_left_3?.setImageResource(res)
        return this
    }

    fun setOnBackPressedListener(body: () -> Unit):ThirdTypeToolbarHolder {
        containerView?.toolbar_layout_back_3?.setOnClickListener { body() }
        return this
    }



}