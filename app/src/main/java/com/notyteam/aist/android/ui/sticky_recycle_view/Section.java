package com.notyteam.aist.android.ui.sticky_recycle_view;

public interface Section {
    int HEADER = 0;
    int ITEM = 1;
    int CUSTOM_HEADER = 2;

    int type();

    int sectionPosition();

    String country();

    String code();

    String letter();

    String countryCode();
}
