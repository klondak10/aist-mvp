package com.notyteam.aist.android.api.retrofit.interfaces

import com.notyteam.aist.android.api.response.responses.AuthResponse
import com.notyteam.aist.android.api.response.responses.BaseResponse
import io.reactivex.Single
import retrofit2.http.*

interface AuthApiInterface {

    @POST("auth/users")
    fun authUser(@Query("user_id") userId: Int,
                 @Query("auth_id") authId: Int,
                 @Query("session_id") sessionId: Int): Single<BaseResponse>

    @PUT("users/{user_id}")
    fun setUserAuthParams(@Path("user_id") userId: Int,
                           @Query("auth_id") authId: Int,
                           @Query("session_id") sessionId: Int): Single<AuthResponse>
}