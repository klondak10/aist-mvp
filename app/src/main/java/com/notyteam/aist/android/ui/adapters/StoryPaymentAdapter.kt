package com.notyteam.aist.android.ui.adapters

import android.view.View
import com.noty.aist.android.R
import kotlinx.android.extensions.LayoutContainer

class StoryPaymentAdapter(list: MutableList<String>,
                          var onClick :(position: String) -> Unit = {}) :
        BaseAdapter<String, StoryPaymentAdapter.ViewHolder>(list) {


    override fun createViewHolder(view: View, layoutId: Int): ViewHolder = ViewHolder(view)

    override fun getItemViewType(position: Int): Int = R.layout.item_story_payment

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {

        init {
            itemView.setOnClickListener {
                if (adapterPosition >= 0) {
                    onClick(list[adapterPosition])
                }
            }
        }

        override val containerView: View? get() = itemView

        override fun bind(pos: Int) {

            //val tv_get_check = itemView.findViewById<TextView>(R.id.tv_get_check)
            //title.text = list[pos].name

        }
    }
}