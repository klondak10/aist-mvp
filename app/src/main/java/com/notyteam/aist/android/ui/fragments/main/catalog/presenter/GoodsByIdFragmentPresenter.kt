package com.notyteam.aist.android.ui.fragments.main.catalog.presenter

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.CategoryListGoodsByIdFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.GoodsByIdFragment

class GoodsByIdFragmentPresenter(view: GoodsByIdFragment?): BasePresenter<GoodsByIdFragment>(view) {


    fun getGoodsById(id:Int){
        requests.add(aistApi.getGoodsById(id,{
            view?.onGoodsByIdSuccess(it.data)
        }, { errorMessage, errorCode ->
            view?.onCatalogListError(errorMessage)
        }).sendRequest())
    }

    fun addFavorites(user_id:Int,product_id:Int){
        requests.add(aistApi.addFavorites(user_id,product_id,{
            view?.onFavoritesSuccess(it.successs)
        }, { errorMessage, errorCode ->
            view?.onFavoritesError(errorMessage)
        }).sendRequest())
    }

    fun deleteFavorites(user_id:Int,favorite_id:Int){
        requests.add(aistApi.deleteFavorites(user_id,favorite_id,{
            view?.onFavoritesSuccess(it.successs)
        }, { errorMessage, errorCode ->
            view?.onFavoritesError(errorMessage)
        }).sendRequest())
    }

    fun addWish(user_id:Int,product_id:Int){
        requests.add(aistApi.addWish(user_id,product_id,{
            view?.onFavoritesSuccess(it.successs)
        }, { errorMessage, errorCode ->
            view?.onFavoritesError(errorMessage)
        }).sendRequest())
    }

    fun deleteWish(user_id:Int,wish_id:Int){
        requests.add(aistApi.deleteWish(user_id,wish_id,{
            view?.onFavoritesSuccess(it.successs)
        }, { errorMessage, errorCode ->
            view?.onFavoritesError(errorMessage)
        }).sendRequest())
    }

}