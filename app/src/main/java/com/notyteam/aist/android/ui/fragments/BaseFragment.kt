package com.notyteam.aist.android.ui.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.noty.aist.android.R
import com.notyteam.aist.android.Screens
import com.notyteam.aist.android.mvp.BaseViewInterface
import com.notyteam.aist.android.mvp.PresenterDetachInterface
import com.notyteam.aist.android.ui.ActivityBottomBarInterface
import com.notyteam.aist.android.ui.activities.BaseActivity
import com.notyteam.aist.android.ui.common.BackButtonListener
import com.notyteam.aist.android.ui.common.RouterProvider
import com.notyteam.aist.android.ui.holders.ToolbarHolder
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_progress.*
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router


abstract class BaseFragment : im.aist.sdk.controllers.BaseFragment(), BaseViewInterface, BackButtonListener {

    protected var activityBottomBarInterface: ActivityBottomBarInterface = ActivityBottomBarInterface.empty

    protected var rootView: View? = null
    var isVisible: (fragment: Fragment) -> Boolean = { true }
    var presenterDetachInterface: PresenterDetachInterface? = null
    var presenterDetachInterfacesList: ArrayList<PresenterDetachInterface?> = arrayListOf()

    protected class ResultContainer(var result: Any = "")
    private var fragmentPreviousCallback = ResultContainer()
    private var fragmentNextCallback = ResultContainer()
    private var startedForResult = false
//    private var startedForReturn = false

    open fun onFragmentResult(result: Any){}

    fun setResult(result: Any) {
//        startedForReturn = true
        fragmentNextCallback.result = result
    }

    fun prepareFragmentForResult(nextFragment: BaseFragment) {
        startedForResult = true
        fragmentPreviousCallback = nextFragment.fragmentNextCallback
    }

    lateinit var requests: CompositeDisposable
    protected lateinit var baseContext: Context
    lateinit var baseActivity: BaseActivity

    var toolbarHolder: ToolbarHolder? = null

//    @Inject
//    lateinit var joyApi: JoyApi

    @LayoutRes
    protected abstract fun layout(): Int

    protected abstract fun initialization(view: View, isFirstInit: Boolean)

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is ActivityBottomBarInterface)
            activityBottomBarInterface = context
        baseActivity = (context as BaseActivity)
        baseContext = baseActivity.applicationContext
        toolbarHolder = context.toolbarHolder
        requests = CompositeDisposable()
        val cicerone = Cicerone.create()

//        try {
//            (context as BaseActivity).fragmentComponent.inject(this)
//        } catch (e: UninitializedPropertyAccessException) {
//            (context as BaseActivity).initFragmentComponent()
//            context.fragmentComponent.inject(this)
//        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = if (layout() != 0)
            trueView(inflater, container)
        else
            super.onCreateView(inflater, container, savedInstanceState)
        return if (rootView == null) view else rootView
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initialization(view, rootView == null)
        rootView = view
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        try {
            if (startedForResult) {
                startedForResult = false
                onFragmentResult(fragmentPreviousCallback.result)
            }
        }
        catch (e: Exception){}
    }

    private fun trueView(inflater: LayoutInflater, container: ViewGroup?): View? {
        val view = inflater.inflate(layout(), container, false)
        return addProgressView(view)
    }

    private fun addProgressView(rootView: View): View {
        val rootLayout = FrameLayout(baseContext)
        val layoutparams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER)
        rootLayout.layoutParams = layoutparams
        rootLayout.addView(rootView)

        val progressLayout = baseActivity.layoutInflater.inflate(R.layout.dialog_progress, null)
        rootLayout.addView(progressLayout)
        return rootLayout
    }

    fun showProgress(show: Boolean){
        progress_layout?.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun setColorProgres(color: Int){
        avi?.setIndicatorColor(color)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        baseActivity.toggleKeyboard(false)
        hideProgressDialog()
    }

    override fun onDestroy() {
        super.onDestroy()
        requests.clear()
        rootView = null
        presenterDetachInterface?.presenterDetach()
        presenterDetachInterfacesList.forEach { it?.presenterDetach() }
    }

    override fun showProgressDialog() {
        if (isVisible(this)) baseActivity.showProgressDialog()
    }

    override fun hideProgressDialog() {
        if (isVisible(this)) baseActivity.hideProgressDialog()
    }

    fun getScreenRouter(): Router {
        return (parentFragment as RouterProvider).getRouter()
    }

    fun replaceFragment(fragment: BaseFragment) {
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().replaceScreen(Screens.FragmentScreen(fragment))
        else baseActivity.replaceFragment(fragment)
    }

    fun addFragment(fragment: BaseFragment) {
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().navigateTo(Screens.FragmentScreen(fragment))
        else baseActivity.addFragment(fragment)
    }

    fun newRootFragment(fragment: BaseFragment){
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().newRootScreen(Screens.FragmentScreen(fragment))
        else baseActivity.newRootFragment(fragment)
    }

    fun getFirstTypeToolbar() = toolbarHolder?.firstTypeToolbar?.showFirstTypeToolbar()

    fun getSecondTypeToolbar() = toolbarHolder?.secondTypeToolbar?.showSecondTypeToolbar()

    fun getThirdTypeToolbar() = toolbarHolder?.thirdTypeToolbar?.showThirdTypeToolbar()

    fun getFirstTypeToolbarInstance() = toolbarHolder?.firstTypeToolbar

    fun getSecondTypeToolbarInstance() = toolbarHolder?.secondTypeToolbar

    fun hideToolbar(){
        toolbarHolder?.hideToolbar()
    }

    override fun onBackPressed(): Boolean {
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().exit()
        else baseActivity.exit()
        return true
    }

    fun finish() {
        baseActivity.finish()
    }

    fun setHyperLink(textView: TextView, messageHyperTextStringId: Int, onClick: (textView: View) -> Unit) {
        baseActivity.setHyperLink(textView, messageHyperTextStringId, onClick)
    }

    fun showErrorSnack(message: String) {
        val snackbar = view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG)
        }
        context?.let {
            ContextCompat.getColor(it, R.color.red_text)
        }?.let {
            snackbar?.view?.setBackgroundColor(it)
        }
        snackbar?.show()
    }

    fun showSuccessSnack(message: String) {
        val snackbar = view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG)
        }
        context?.let {
            ContextCompat.getColor(it, R.color.green)
        }?.let {
            snackbar?.view?.setBackgroundColor(it)
        }
        snackbar?.show()
    }
}