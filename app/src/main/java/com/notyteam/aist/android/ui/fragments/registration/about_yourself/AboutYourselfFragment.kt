package com.notyteam.aist.android.ui.fragments.registration.about_yourself

import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.activities.main.MainActivity
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.setting.profile.ProfileFragment
import com.notyteam.aist.android.ui.fragments.registration.enter_profile.EnterProfileFragment
import kotlinx.android.synthetic.main.frament_about_yourself.*

class AboutYourselfFragment : BaseFragment(), AboutYourselfFragmentView {
    companion object {
        fun newInstance(): AboutYourselfFragment {
            return AboutYourselfFragment()
        }
    }

    override fun layout(): Int = R.layout.frament_about_yourself

    override fun initialization(view: View, isFirstInit: Boolean) {
        hideToolbar()
        if (isFirstInit) {
            initViews()
        }
    }

    private var presenter: AboutYourselfFragmentPresenter = AboutYourselfFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    override fun onBackPressed(): Boolean {
        finish()
        return true
    }

    private fun initViews() {
        button_next.setOnClickListener{
            val profile = ProfileFragment()
            profile.isFirstInit = true
            addFragment(profile)
        }

        tv_skip.setOnClickListener {
            MainActivity.start(baseContext)
        }

    }

}
