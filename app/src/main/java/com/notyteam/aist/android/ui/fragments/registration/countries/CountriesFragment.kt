package com.notyteam.aist.android.ui.fragments.registration.countries

import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import com.google.gson.Gson
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.models.responce.countries.CountiesResponse
import com.notyteam.aist.android.ui.models.responce.countries.CountriesItem
import com.notyteam.aist.android.ui.sticky_recycle_view.Section
import com.notyteam.aist.android.ui.sticky_recycle_view.SectionAdapter
import com.notyteam.aist.android.ui.sticky_recycle_view.SectionHeader
import com.notyteam.aist.android.ui.sticky_recycle_view.SectionItem
import com.notyteam.aist.android.utils.dpToPx
import com.notyteam.aist.android.utils.fromJson
import com.notyteam.aist.android.utils.inputStreamToString
import com.shuhart.stickyheader.StickyHeaderItemDecorator
import kotlinx.android.synthetic.main.fragment_countries.*

class CountriesFragment : BaseFragment(), CountriesFragmentView {
    companion object {
        fun newInstance(): CountriesFragment {
            return CountriesFragment()
        }
    }

    private var presenter: CountriesFragmentPresenter = CountriesFragmentPresenter(this)
    var sectionCode: Section = SectionItem(0, "", "", "", "")
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    override fun layout(): Int = R.layout.fragment_countries

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        getFirstTypeToolbar()
                ?.setTitle(getString(R.string.phone_country))
                ?.setLeftText(getString(R.string.back))
                ?.setOnBackPressedListener {
                    onBackPressed()
                }
        if (isFirstInit) {
            initViews()
        }
    }

    private fun calculateCountries(countries: List<CountriesItem>) {
        rv_countries.layoutManager = LinearLayoutManager(baseContext)
//        rv_countries.setIndexBarColor(R.color.blue)
//        rv_countries.setIndexbarMargin(15f)

        val sections = arrayListOf<Section>()
        var section = 0
        var headerCounter = 0
        var letter = countries[0].fullCountryName[0].toString()

        sections.add(SectionHeader(section, letter))
        countries.forEachIndexed { index, locale ->
            val country = locale.fullCountryName[0].toString()
            if (letter != country) {
                letter = country
                headerCounter ++
                section = index + headerCounter

                sections.add(SectionHeader(section, letter))
            }
            sections.add(SectionItem(section, locale.fullCountryName, locale.dialCode, country, locale.countryCode))

        }

        val adapter = SectionAdapter(sections, object: SectionAdapter.OnItemClickListener{
            override fun onItemClick(section: Section) {
                setResult(section)
                onBackPressed()
            }
        })

        rv_countries.adapter = adapter
        val decorator = StickyHeaderItemDecorator(adapter)
        decorator.attachToRecyclerView(rv_countries)
        adapter.items = sections
        adapter.notifyDataSetChanged()

        if (sectionCode.countryCode().isNotEmpty()){
            var indexSection = 0
            sections.forEachIndexed { index, item ->
                if (item.countryCode() == sectionCode.countryCode()){
                    indexSection = index
                    return@forEachIndexed
                }
            }
            rv_countries.viewTreeObserver.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener{
                override fun onGlobalLayout() {
                    rv_countries.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    (rv_countries.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(indexSection, dpToPx(baseContext, 35))
                }
            })
        }
    }

    private fun initViews() {
        initRecycleView()
        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val list = countries.countries.filter { it.fullCountryName.toLowerCase().contains(s.toString().toLowerCase()) }
                if (list.isNotEmpty())
                    calculateCountries(list)
                else
                    calculateCountries(countries.countries)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }

    private lateinit var countries: CountiesResponse

    private fun initRecycleView() {
        val myJson = inputStreamToString(baseContext.resources.openRawResource(R.raw.countries))
        countries = Gson().fromJson<CountiesResponse>(myJson)
        calculateCountries(countries.countries)
    }

}
