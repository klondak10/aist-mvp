package com.notyteam.aist.android.subnavigation

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router

class LocalCiceroneHolder() {
    private val containers: HashMap<String, Cicerone<Router>> = hashMapOf()

    fun getCicerone(containerTag: String): Cicerone<Router> {
        if (!containers.containsKey(containerTag)) {
            containers[containerTag] = Cicerone.create()
        }
        return containers[containerTag]?:Cicerone.create()
    }
}