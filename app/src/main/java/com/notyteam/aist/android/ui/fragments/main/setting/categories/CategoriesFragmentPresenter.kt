package com.notyteam.aist.android.ui.fragments.main.setting.categories

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter

class CategoriesFragmentPresenter(view: CategoriesFragmentView?): BasePresenter<CategoriesFragmentView>(view) {

    fun getInterestsList(){
        requests.add(aistApi.getAllInterestsList({
            view?.onInterestsListSuccess(it)
        }, { errorMessage, errorCode ->
            view?.onInterestsListError(errorMessage)
        }).sendRequest())
    }

}