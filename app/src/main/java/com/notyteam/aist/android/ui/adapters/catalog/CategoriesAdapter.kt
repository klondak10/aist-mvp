package com.notyteam.aist.android.ui.adapters.catalog

import android.view.View
import android.widget.TextView
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.adapters.BaseAdapter
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import kotlinx.android.extensions.LayoutContainer

class CategoriesAdapter(list: MutableList<UICategoriesCatalog>,
                        var onClick :(data: UICategoriesCatalog) -> Unit = {}) :
        BaseAdapter<UICategoriesCatalog, CategoriesAdapter.ViewHolder>(list) {


    override fun createViewHolder(view: View, layoutId: Int): ViewHolder = ViewHolder(view)

    override fun getItemViewType(position: Int): Int = R.layout.item_category_catalog

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {

        init {
            itemView.setOnClickListener {
                if (adapterPosition >= 0) {
                    onClick(list[adapterPosition])
                }
            }
        }

        override val containerView: View? get() = itemView

        override fun bind(pos: Int) {
            val name_category_catalog = itemView.findViewById<TextView>(R.id.name_category_catalog)
            try {
                name_category_catalog.text = list[pos].name
            }catch (e : IllegalStateException){}
        }
    }
}