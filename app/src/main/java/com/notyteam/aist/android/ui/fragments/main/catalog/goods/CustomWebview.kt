package com.notyteam.aist.android.ui.fragments.main.catalog.goods

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import im.delight.android.webview.AdvancedWebView
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_goods_add_to_cart.*
import kotlinx.android.synthetic.main.fragment_goods_description.*


class CustomWebview : BaseFragment(),AdvancedWebView.Listener {
    override fun onPageFinished(url: String?) {
        showProgress(false)
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        showProgress(false)
        showErrorSnack(description.toString())
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {}

    override fun onExternalPageRequest(url: String?) {}

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
        showProgress(true)
    }

    companion object {
        private const val URL = "URL"
        private const val SITE = "SITE"
        fun newInstance(url:String,site:String): CustomWebview {
            val f = CustomWebview ()
            // Pass index input as an argument.
            val args = Bundle()
            args.putString(URL, url)
            args.putString(SITE, site)
            f.setArguments(args)
            return f
        }
        fun getAddToCartUrl(arguments: Bundle?) = arguments?.getString(URL)?:""
        fun getTitle(arguments: Bundle?) = arguments?.getString(SITE)?:""
    }

    override fun layout(): Int = R.layout.fragment_goods_add_to_cart

    override fun initialization(view: View, isFirstInit: Boolean) {
        getSecondTypeToolbar()
                ?.setTitle(getTitle(arguments))
                ?.setLeftIcon(R.drawable.ic_btn_left)
                ?.setRightIcon(R.drawable.ic_refresh_black_24dp)
                ?.setOnLeftIconListener {
                   onBackPressed()
                }
                ?.setOnRightIconListener {
                    mWebView.loadUrl(mWebView.url.toString())
                }

        activityBottomBarInterface.showBottomBar(false)

        initViews()


     }

    private fun initViews() {
        mWebView.settings.javaScriptEnabled = true
        mWebView.setListener(baseActivity, this);
        mWebView.loadUrl(getAddToCartUrl(arguments));
    }

    @SuppressLint("NewApi")
    override fun onResume() {
        super.onResume()
        mWebView.onResume()
    }

    @SuppressLint("NewApi")
    override fun onPause() {
        mWebView.onPause()
        super.onPause()
    }


}
