package com.notyteam.aist.android.ui.fragments.main.catalog.goods

import android.os.Bundle
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_goods_description.*


class GoodsDescriptionFragment : BaseFragment() {

    companion object {
        private const val DESCRIPTION_KEY = "DESCRIPTION_KEY"
        fun newInstance(description:String): GoodsDescriptionFragment {
            val f = GoodsDescriptionFragment ()
            // Pass index input as an argument.
            val args = Bundle()
            args.putString(DESCRIPTION_KEY, description)
            f.setArguments(args)
            return f
        }
        fun getDescription(arguments: Bundle?) = arguments?.getString(DESCRIPTION_KEY)?:""
    }

    override fun layout(): Int = R.layout.fragment_goods_description

    override fun initialization(view: View, isFirstInit: Boolean) {

        if (isFirstInit) {
            initViews()
        }
    }

    private fun initViews() {
        txt_description.text = getDescription(arguments)
    }

}
