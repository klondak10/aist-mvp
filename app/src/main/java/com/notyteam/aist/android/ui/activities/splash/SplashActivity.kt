package com.notyteam.aist.android.ui.activities.splash

import android.Manifest
import android.app.ActivityManager
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import com.noty.aist.android.BuildConfig
import com.noty.aist.android.R
import com.notyteam.aist.android.AistApp
import com.notyteam.aist.android.ui.activities.BaseActivity
import com.notyteam.aist.android.ui.activities.auth.AuthActivity
import com.notyteam.aist.android.ui.activities.main.MainActivity
import im.aist.runtime.android.AndroidContext
import im.aist.sdk.aist.ActorSDK
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.concurrent.TimeUnit

class SplashActivity : BaseActivity() {

    override fun layout(): Int = R.layout.activity_splash

    companion object {
        const val CONTACTS_PERMISSION_REQUEST = 666
        const val SYSTEM_APP_SETTINGS_REQUEST = 667
        val permissions = arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS)

        const val OPEN_LOGIN = "OPEN_LOGIN"
        fun start(c: Context, shouldOpenLogin: Boolean = false) {
            val intent = Intent(c, SplashActivity::class.java)
            intent.putExtra(OPEN_LOGIN, shouldOpenLogin)
            c.startActivity(intent)
        }
    }

    override fun initialization() {

        AndroidContext.setContext(AistApp.getMyApplicationContext())

        setTransparentStatusBar(isTransparent = true, isDarkTexts = true)

        avi.show()
        checkPermissions()

    }

    private fun checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, CONTACTS_PERMISSION_REQUEST)
        }
        else initActorsSdk()
    }

    private fun showRequestPermissionDialog(){
        AlertDialog.Builder(this, R.style.AlertDialogLight)
                .setCancelable(false)
                .setTitle(getString(R.string.permissions_dialog_title))
                .setMessage(getString(R.string.permissions_dialog_message))
                .setPositiveButton(getString(R.string.permissions_dialog_ok)){ dialog , _ ->
                    openAppSystemSettings(SYSTEM_APP_SETTINGS_REQUEST)
                    dialog.dismiss()
                }
                .setNeutralButton(getString(R.string.permissions_dialog_cancel)){ dialog, _ ->
                    dialog.dismiss()
                    finish()
                }
                .create().show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SYSTEM_APP_SETTINGS_REQUEST){
            checkPermissions()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CONTACTS_PERMISSION_REQUEST &&
                permissions.contains(Manifest.permission.READ_CONTACTS) &&
                permissions.contains(Manifest.permission.WRITE_CONTACTS) ) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) initActorsSdk()
            else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!shouldShowRequestPermissionRationale(permissions.last())) {
                        showRequestPermissionDialog()
                    }
                    else requestPermissions(permissions, CONTACTS_PERMISSION_REQUEST)
                }
                else checkPermissions()
            }
        }
    }

    private fun initActorsSdk(){
        val id = android.os.Process.myPid()
        var myProcessName = packageName
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (procInfo in activityManager.runningAppProcesses) {
            if (id == procInfo.pid) {
                myProcessName = procInfo.processName
            }
        }

        // Protection on double start
        if (!myProcessName.endsWith(":actor_push")) {
            ActorSDK.sharedActor().createActor(application)
            onConfigureActorSDK()
        }

        ActorSDK.sharedActor().waitForReady()
        startWaitTimer()
    }

    private fun onConfigureActorSDK() {
        ActorSDK.sharedActor().endpoints = arrayOf(
                BuildConfig.END_POINT
        )

        ActorSDK.sharedActor().isFastShareEnabled = true
        ActorSDK.sharedActor().creatEndPointPush()
        val style = ActorSDK.sharedActor().style
        style.accentColor = R.color.colorAccent
        ActorSDK.sharedActor().style = style

    }

    private fun startWaitTimer() {
        requests.add(Observable
                .timer(2000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({},{},::openActivity))
    }

    private fun openActivity() {
        if (!ActorSDK.sharedActor().messenger.isLoggedIn) {
            val shouldOpenLogin = intent?.getBooleanExtra(OPEN_LOGIN, false)?:false
            AuthActivity.start(this, shouldOpenLogin)
            finish()
        }
        else {
//            ActorSDK.sharedActor().startMessagingApp(this)
//            ActorSDK.sharedActor().startAuthActivity(this)
//            finish()
            MainActivity.start(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        avi.hide()
    }
}