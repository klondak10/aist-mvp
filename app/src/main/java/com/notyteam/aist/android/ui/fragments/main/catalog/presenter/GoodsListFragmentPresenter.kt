package com.notyteam.aist.android.ui.fragments.main.catalog.presenter

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.GoodsListFragment

class GoodsListFragmentPresenter(view: GoodsListFragment?): BasePresenter<GoodsListFragment>(view) {


    fun getCategoryListPagePerPage(id:Int){
        requests.add(aistApi.getCategoriesListPagePerPage(id,{
            view?.onCatalogListSuccessPagePerPage(it)
        }, { errorMessage, errorCode ->
            view?.onCatalogListError(errorMessage)
        }).sendRequest())
    }

    fun getSearchList(query:String){
        requests.add(aistApi.getSearchList(query,{
            view?.onCatalogListSuccessPagePerPage(it)
        }, { errorMessage, errorCode ->
            view?.onCatalogListError(errorMessage)
        }).sendRequest())
    }


}