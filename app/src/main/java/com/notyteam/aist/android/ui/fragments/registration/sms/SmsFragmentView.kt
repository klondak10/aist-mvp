package com.notyteam.aist.android.ui.fragments.registration.sms

import com.notyteam.aist.android.mvp.PresenterDetachInterface
import im.aist.core.network.RpcException

interface SmsFragmentView: PresenterDetachInterface {
    fun onValidateSuccess()
    fun onValidateError(e: Exception)
    fun onInvalidCode(e: RpcException)
    fun onCodeExpired(e: RpcException)
    fun onCompleteAuthError(errorMessage: String)

    fun onResendTimerTick(time: Long)
    fun onResendTimerCompleted()
}