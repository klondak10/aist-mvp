package com.notyteam.aist.android.ui.fragments.tutorial

import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.robertlevonyan.components.picker.set
import kotlinx.android.synthetic.main.fragment_pager_tutorial.*

class TutorialPagerFragment : BaseFragment() {
    companion object {
        fun newInstance(): TutorialPagerFragment {
            return TutorialPagerFragment()
        }
    }

    var title_tut = ""
    var description = ""
    var page = 0

    override fun layout(): Int = R.layout.fragment_pager_tutorial

    override fun initialization(view: View, isFirstInit: Boolean) {
        if (isFirstInit) {
            initViews()
        }
    }

    private fun initViews() {
        tutorial_title.text = title_tut
        tutorial_description.text = description
        tutorial_image.set(when(page){
            0 -> R.drawable.tutorial2
            1 -> R.drawable.tutorial3
            2 -> R.drawable.tutorial1
            else -> R.drawable.tutorial1
        })
    }

}
