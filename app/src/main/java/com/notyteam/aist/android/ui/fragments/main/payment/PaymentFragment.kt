package com.notyteam.aist.android.ui.fragments.main.payment

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.content.Context.VIBRATOR_SERVICE
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Vibrator
import android.support.v4.content.ContextCompat
import android.text.Html
import android.text.TextUtils
import android.util.Log
import android.view.SurfaceHolder
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.zxing.Result
import com.noty.aist.android.BuildConfig
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.payment.BindQrResponse
import com.notyteam.aist.android.api.response.payment.FindQrResponse
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.CustomWebview
import com.notyteam.aist.android.ui.fragments.main.payment.barcodescanner.camera.CameraManager
import com.notyteam.aist.android.ui.fragments.main.payment.barcodescanner.decode.CaptureActivityHandler
import com.notyteam.aist.android.ui.fragments.main.payment.barcodescanner.decode.DecodeManager
import com.notyteam.aist.android.ui.fragments.main.payment.barcodescanner.decode.InactivityTimer
import com.notyteam.aist.android.ui.fragments.main.setting.settings.StoryPaymentFragment
import com.notyteam.aist.android.utils.getColorResource
import com.robertlevonyan.components.picker.set
import kotlinx.android.synthetic.main.fragment_payment.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.IOException
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class PaymentFragment : BaseFragment(), PaymentFragmentView,SurfaceHolder.Callback {
    override fun onBindTheCardBuyerSuccess(response: BindQrResponse) {
        showProgress(false)
        addFragment(CustomWebview.newInstance(response.data.attributes.bindingPaymentFormUrl,"Привязка карты"))
    }

    override fun onFindQrSuccess(response: FindQrResponse) {

        if(response.data.get(0).attributes.price !=null){
            edSumm!!.visibility = View.GONE
            txtResult!!.set("Описание товара/услуги:"+"\n"+response.data.get(0).attributes.summary+"\n\n"+ response.data.get(0).attributes.price+ Html.fromHtml(" &#x20bd"))
        }else{
            txtResult!!.visibility = View.GONE
        }

        yesBtn!!.setOnClickListener {

            if(TextUtils.isEmpty(edSumm!!.text.toString())&&edSumm!!.visibility == View.VISIBLE){
                showErrorSnack("Введите сумму оплаты")
            }else{
                restartPreview()
                dialog!!.dismiss()
                showErrorSnack("Сервис временно недоступен")
            }
        }

        noBtn!!.setOnClickListener {
            restartPreview()
            dialog!!.dismiss()

        }
    }

    override fun onFindQrError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }

    var txtResult: TextView? = null;
    var edSumm: EditText? = null;
    var  yesBtn:Button? = null;
    var  noBtn:Button? = null;
    var dialog:Dialog? = null;

    private var mCaptureActivityHandler: CaptureActivityHandler? = null
    private var mHasSurface: Boolean = false
    private var mPermissionOk: Boolean = false
    private var mInactivityTimer: InactivityTimer? = null

    private val mDecodeManager = DecodeManager()

    private val BEEP_VOLUME = 0.10f
    private val VIBRATE_DURATION = 200L
    private var mMediaPlayer: MediaPlayer? = null
    private var mPlayBeep: Boolean = false
    private var mVibrate: Boolean = false
    private var mNeedFlashLightOpen = true

    private var mQrCodeExecutor: Executor? = null


    companion object {
        fun newInstance(): PaymentFragment {
            return PaymentFragment()
        }
    }

    override fun layout(): Int = R.layout.fragment_payment


    override fun initialization(view: View, isFirstInit: Boolean) {
        getSecondTypeToolbar()
                ?.setTitle(getString(R.string.bottom_bar_payment))
                ?.toggleLeftIcon(isVisible = false)
                ?.toggleRightIcon(isVisible = false)

        if (isFirstInit) {
            initViews()
        }
    }

    private var presenter = PaymentFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    private fun initViews() {

        mHasSurface = false
        initData()

        container_flash.setOnClickListener {

            if (mNeedFlashLightOpen) {
                turnFlashlightOn()
            }
            else {
                turnFlashLightOff()
           }
        }


        bind_card.setOnClickListener {

            val type = JSONObject()
            val data = JSONObject()
            type.put("type","cards")
            data.put("data",type)

            val JSON = MediaType.parse("application/json; charset=utf-8")
            // put your json here
            val body = RequestBody.create(JSON, data.toString())

            presenter.bindTheCardBuyer(BuildConfig.ID_BUYER,body)

            setColorProgres(resources.getColor(R.color.red_text))
            showProgress(true)

        }

        tvStoryPayment.setOnClickListener { addFragment(StoryPaymentFragment()) }
    }


    private fun checkPermission() {
        val hasHardware = checkCameraHardWare(baseActivity)
        if (hasHardware) {
            hasCameraPermission()

        } else {
            mPermissionOk = false
            baseActivity.finish()
        }
    }


    private fun initData() {
        CameraManager.init(baseActivity)
        mInactivityTimer = InactivityTimer(baseActivity)
        mQrCodeExecutor = Executors.newSingleThreadExecutor()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == 101) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    qr_code_view_finder!!.setVisibility(View.GONE)
                    mPermissionOk = false
            }
            else {
                   mPermissionOk = true
            }
        }

    }

    private fun hasCameraPermission() {

            if (Build.VERSION.SDK_INT >= 23) {
                if (ContextCompat.checkSelfPermission(baseActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA),
                            101)

                }
            }

    }

    override fun onResume() {
        super.onResume()
        checkPermission()

        val surfaceHolder = qr_code_preview_view!!.getHolder()
        turnFlashLightOff()
        if (mHasSurface) {
            initCamera(surfaceHolder)
        } else {
            surfaceHolder.addCallback(this)
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
        }

        mPlayBeep = true
        val audioService = baseActivity.getSystemService(AUDIO_SERVICE) as AudioManager
        if (audioService.ringerMode != AudioManager.RINGER_MODE_NORMAL) {
            mPlayBeep = false
        }
        initBeepSound()
        mVibrate = true

    }

    override fun onPause() {
        super.onPause()
        if (mCaptureActivityHandler != null) {
            mCaptureActivityHandler!!.quitSynchronously()
            mCaptureActivityHandler = null
        }
        CameraManager.get().closeDriver()
    }

    override fun onDestroy() {
        if (null != mInactivityTimer) {
            mInactivityTimer!!.shutdown()
        }
        super.onDestroy()
    }


    fun handleDecode(result: Result?) {
        mInactivityTimer!!.onActivity()
        playBeepSoundAndVibrate()
        if (null == result) {
            mDecodeManager.showCouldNotReadQrCodeFromScanner(activity, object : DecodeManager.OnRefreshCameraListener {
                override fun refresh() {
                    restartPreview()
                }
            })
        } else {
            val resultString = result.text

            handleResult(resultString)

        }
    }

    private fun initCamera(surfaceHolder: SurfaceHolder) {
        try {
            CameraManager.get().openDriver(surfaceHolder)
        } catch (e: IOException) {
            Toast.makeText(activity, getString(R.string.qr_code_camera_not_found), Toast.LENGTH_SHORT).show()
            baseActivity.finish()
            return
        } catch (re: RuntimeException) {
            re.printStackTrace()
            return
        }

        qr_code_view_finder.setVisibility(View.VISIBLE)
        qr_code_preview_view.setVisibility(View.VISIBLE)

        if (mCaptureActivityHandler == null) {
            mCaptureActivityHandler = CaptureActivityHandler(this)
        }
    }

    private fun restartPreview() {
        if (null != mCaptureActivityHandler) {
            mCaptureActivityHandler!!.restartPreviewAndDecode()
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {

    }

    private fun checkCameraHardWare(context: Context): Boolean {
        val packageManager = context.packageManager
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        if (!mHasSurface) {
            mHasSurface = true
            initCamera(holder)
        }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        mHasSurface = false
    }

    fun getCaptureActivityHandler(): CaptureActivityHandler? {
        return mCaptureActivityHandler
    }

    private fun initBeepSound() {
        if (mPlayBeep && mMediaPlayer == null) {
            // The volume on STREAM_SYSTEM is not adjustable, and users found it too loud,
            // so we now play on the music stream.
            baseActivity.volumeControlStream = AudioManager.STREAM_MUSIC
            mMediaPlayer = MediaPlayer()
            mMediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mMediaPlayer!!.setOnCompletionListener(mBeepListener)

            val file = resources.openRawResourceFd(R.raw.beep)
            try {
                mMediaPlayer!!.setDataSource(file.fileDescriptor, file.startOffset, file.length)
                file.close()
                mMediaPlayer!!.setVolume(BEEP_VOLUME, BEEP_VOLUME)
                mMediaPlayer!!.prepare()
            } catch (e: IOException) {
                mMediaPlayer = null
            }

        }
    }

    private fun playBeepSoundAndVibrate() {
        if (mPlayBeep && mMediaPlayer != null) {
            mMediaPlayer!!.start()
        }
        if (mVibrate) {
            val vibrator = baseActivity.getSystemService(VIBRATOR_SERVICE) as Vibrator
            vibrator.vibrate(VIBRATE_DURATION)
        }
    }

    /**
     * When the beep has finished playing, rewind to queue up another one.
     */
    private val mBeepListener = MediaPlayer.OnCompletionListener { mediaPlayer -> mediaPlayer.seekTo(0) }


    private fun turnFlashlightOn() {
        mNeedFlashLightOpen = false
        qr_code_iv_flash_light.setBackgroundResource(R.drawable.flash_on)
        tv_flash.text = getString(R.string.flash_on)
        CameraManager.get().setFlashLight(true)
    }

    private fun turnFlashLightOff() {
        mNeedFlashLightOpen = true
        qr_code_iv_flash_light.setBackgroundResource(R.drawable.flash_off)
        tv_flash.text = getString(R.string.flash_off)
        CameraManager.get().setFlashLight(false)
    }

    private fun handleResult(resultString: String) {
        if (TextUtils.isEmpty(resultString)) {
            mDecodeManager.showCouldNotReadQrCodeFromScanner(activity) { restartPreview() }
        } else {

            dialog = Dialog(context)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
            dialog!!.setCancelable(false)
            dialog!!.setContentView(R.layout.dialog_result)

            edSumm = dialog!!.findViewById(R.id.ed_summ) as EditText
            txtResult = dialog!!.findViewById(R.id.txt_result) as TextView
            yesBtn = dialog!!.findViewById(R.id.yesBtn) as Button
            noBtn = dialog!!.findViewById(R.id.noBtn) as Button

            yesBtn!!.text = "оплатить"
            noBtn!!.text = "отменить"

            presenter.findQr(resultString)

            dialog!!.show()


        }
    }


}
