package com.notyteam.aist.android.ui.common

import ru.terrakok.cicerone.Router

interface RouterProvider {
    fun getRouter(): Router
}