package com.notyteam.aist.android.ui.fragments.main.payment.barcodescanner.decode;

import com.google.zxing.Result;

public interface DecodeImageCallback {

    void decodeSucceed(Result result);

    void decodeFail(int type, String reason);
}
