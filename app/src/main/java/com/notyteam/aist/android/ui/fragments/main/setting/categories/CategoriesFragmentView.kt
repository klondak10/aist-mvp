package com.notyteam.aist.android.ui.fragments.main.setting.categories

import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.mvp.PresenterDetachInterface

interface CategoriesFragmentView: PresenterDetachInterface {
    fun onInterestsListSuccess(list: InterestsListResponse)
    fun onInterestsListError(errorMessage: String)

}