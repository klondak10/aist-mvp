package com.notyteam.aist.android.api.retrofit

import android.content.Context
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.responses.BaseResponse
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import retrofit2.Retrofit
import java.lang.ref.WeakReference
import java.net.UnknownHostException

class CallbackWrapper<T, V>(
        private val contextWeakReference: WeakReference<Context>? = null,
        private val retrofit: Retrofit,
        private var onSuccessResponse:(response: V) -> Unit,
        private var onError:(errorMessage: String, errorCode: Int) -> Unit) : DisposableSingleObserver<T>(){

    companion object {
        const val ERROR_INTERNET_CONNECTION_CODE = 600
        const val ERROR_CONVERTING_DATA_CODE = 601
        const val ERROR_UNEXPECTED_CODE = 666
    }

    var request: Single<T>? = null
    private var repeatingUnauthorizedRequestCount = 0

    override fun onSuccess(response: T) {
        if (response is BaseResponse) {
            onSuccessResponse(response as V)
        }
        else onError(contextWeakReference?.get()?.getString(R.string.error_converting_data)?:"", ERROR_CONVERTING_DATA_CODE)
//        else if (response is ResponseBody){
//            try {
//                val responseString = response.string()
//                val jsonData = Gson().fromJson<BaseResponse>(responseString)
//                if (jsonData.success) {
//                    onSuccessResponse(responseString as V)
//                }
//                else {
//                    onError(jsonData.error.message, jsonData.error.code)
//                }
//            }
//            catch (e: JSONException){
//                onError(contextWeakReference?.get()?.getString(R.string.error_converting_data)?:"", ERROR_CONVERTING_DATA_CODE)
//            }
//        }



//        val jsonData = Gson().toJson(response)
//        try {
//            val json = JSONObject(jsonData)
//            if (json.has("response")) {
//                val success: Boolean = json.getBoolean("response")
//                if (!success) {
//                    val error = Gson().fromJson<BaseResponse>(jsonData)
//                    onError(error.error.message, error.error.code)
//                }
//                else onSuccessResponse(response)
//            }
//            else onError(contextWeakReference?.get()?.getString(R.string.error_converting_data)?:"Ошибка преобразования данных", ERROR_CONVERTING_DATA_CODE)
//        }
//        catch (e: JSONException){
//            onError(contextWeakReference?.get()?.getString(R.string.error_converting_data)?:"Ошибка преобразования данных", ERROR_CONVERTING_DATA_CODE)
//        }
    }

    override fun onError(t: Throwable) {
        val errorMessage: String
        val errorCode: Int
        if (t is IOException) {
            errorMessage = contextWeakReference?.get()?.getString(R.string.error_no_intenet_connection)?:"Проверьте интернет соединение"
            errorCode = ERROR_INTERNET_CONNECTION_CODE
        } else if (t is HttpException) {
            val errorWrapper = ErrorWrapper()
            val error = errorWrapper.parseError(retrofit, t.response())
//            if (!error.success) {
//                errorMessage = error.error.message
//                errorCode = error.error.code
//            }
//            else
//            {
                errorMessage = contextWeakReference?.get()?.getString(R.string.error_unexpected)?:"Непредвиденная ошибка"
                errorCode = ERROR_UNEXPECTED_CODE
//            }
        } else if (t is IllegalStateException) {
            errorMessage = contextWeakReference?.get()?.getString(R.string.error_converting_data)?:"Ошибка преобразования данных"
            errorCode = ERROR_CONVERTING_DATA_CODE
        } else {
            errorMessage = contextWeakReference?.get()?.getString(R.string.error_unexpected)?:"Непредвиденная ошибка"
            errorCode = ERROR_UNEXPECTED_CODE
        }
        onError(errorMessage, errorCode)
    }

    private inner class ErrorWrapper {
        fun parseError(retrofit: Retrofit, response: Response<*>): BaseResponse {
            val converter: Converter<ResponseBody, BaseResponse> = retrofit
                    .responseBodyConverter(BaseResponse::class.java, emptyArray())
            try {
                response.errorBody()?.let {
                    return converter.convert(it)
                }
            } catch (e: Exception) {
                return BaseResponse()
            }
            return BaseResponse()
        }
    }
}