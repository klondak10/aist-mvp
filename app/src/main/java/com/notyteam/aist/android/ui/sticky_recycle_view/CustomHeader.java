package com.notyteam.aist.android.ui.sticky_recycle_view;

import android.support.v7.widget.RecyclerView;

public class CustomHeader implements Section {
    @Override
    public int type() {
        return CUSTOM_HEADER;
    }
    private int section;
    private String country;
    private String code,letter;
    @Override
    public int sectionPosition() {
        return RecyclerView.NO_POSITION;
    }

    public CustomHeader(int section, String letter) {
        this.section = section;
        this.letter = letter;
    }

    @Override
    public String country() {
        return null;
    }

    @Override
    public String code() {
        return null;
    }

    @Override
    public String letter() {
        return letter;
    }

    @Override
    public String countryCode() {
        return null;
    }
}
