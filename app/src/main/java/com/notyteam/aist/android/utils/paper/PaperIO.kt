package com.notyteam.aist.android.utils.paper

import im.aist.core.api.rpc.ResponseAuth
import io.paperdb.Paper

class PaperIO {
    companion object {
        private const val BEARER_TOKEN = "BEARER_TOKEN"
        private const val REFRESH_TOKEN = "REFRESH_TOKEN"

        private const val IS_AUTHORIZATION = "IS_AUTHORIZATION"
        private const val FIRST_LAUNCH = "FIRST_LAUNCH"
        private const val SELECTED_LOCATION = "SELECTED_LOCATION"
        private const val AUTH_RESULT = "AUTH_RESULT"
        private const val USER_ID = "USER_ID"
        private const val SESSION_ID = "SESSION_ID"
        private const val AUTH_ID = "AUTH_ID"

        fun setAuthData(result: ResponseAuth){
            Paper.book().write(AUTH_RESULT,result)
            val userId = getAuthData().user.id
            Paper.book().write(USER_ID, userId)
        }
        fun getAuthData():ResponseAuth{
            return Paper.book().read(AUTH_RESULT, ResponseAuth())
        }
        fun getUserId():Int{
            return Paper.book().read(USER_ID, 0)
        }

        fun setSessionId(result: Int){
            Paper.book().write(SESSION_ID, result)
        }
        fun getSessionId():Int{
            return Paper.book().read(SESSION_ID, 0)
        }

        fun setAuthId(result: Int){
            Paper.book().write(AUTH_ID,result)
        }
        fun getAuthId():Int{
            return Paper.book().read(AUTH_ID, 0)
        }

        fun clearData(){
            Paper.book().destroy()
        }
    }
}