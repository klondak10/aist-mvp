package com.notyteam.aist.android.api.response

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Error(

	@field:SerializedName("code")
	val code: Int = 0,

	@field:SerializedName("message")
	val message: String = ""
)