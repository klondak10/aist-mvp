package com.notyteam.aist.android.ui

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.LocaleList
import java.util.*

class ContextWrapper(base: Context) : android.content.ContextWrapper(base) {
    companion object {

        @TargetApi(Build.VERSION_CODES.N)
        fun wrap(context: Context, newLocale: Locale): ContextWrapper {
            var context = context

            val res = context.resources
            val configuration = res.configuration

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                configuration.setLocale(newLocale)

                val localeList = LocaleList(newLocale)
                LocaleList.setDefault(localeList)
                configuration.locales = localeList

                context = context.createConfigurationContext(configuration)

            } else {
                configuration.setLocale(newLocale)
                context = context.createConfigurationContext(configuration)

            }

            return ContextWrapper(context)
        }
    }
}