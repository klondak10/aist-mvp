package com.notyteam.aist.android.mvp
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<V>(protected var view: V?): PresenterDetachInterface {

    var requests: CompositeDisposable = CompositeDisposable()

    protected open fun detachView() {
        requests.clear()
        view = null
    }

    override fun presenterDetach() {
        detachView()
    }
}