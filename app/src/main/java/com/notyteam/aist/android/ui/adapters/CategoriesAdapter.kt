package com.notyteam.aist.android.ui.adapters

import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.models.ui.UICategories
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_category.*

class CategoriesAdapter(list: MutableList<UICategories>,
                        val onClick: (data: UICategories) -> Unit = {}) :
        BaseAdapter<UICategories, CategoriesAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int): ViewHolder = ViewHolder(view)

    override fun getItemViewType(position: Int): Int = R.layout.item_category
    fun getCheckedItems(): List<UICategories> {
        return list.filter { it.isChecked }
    }
    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {
        init {
            itemView.setOnClickListener {
                list[adapterPosition].isChecked = !list[adapterPosition].isChecked
                notifyItemChanged(adapterPosition)
            }
        }

        override val containerView: View? get() = itemView

        override fun bind(pos: Int) {
            val iv_check = itemView.findViewById<View>(R.id.iv_check)
            val title = itemView.findViewById<TextView>(R.id.title)
            iv_check.visibility = if (list[pos].isChecked) View.VISIBLE else View.GONE
            title.text = list[pos].name
        }
    }
}