package com.notyteam.aist.android.utils

import android.os.SystemClock
import android.view.View

abstract class DebouncedOnClickListener(var defaultInterval: Int = 500) : View.OnClickListener {

    private var lastTimeClicked: Long = 0;

    override fun onClick(p0: View?) {
        if ((SystemClock.elapsedRealtime() - lastTimeClicked) < defaultInterval) {
            return;
        }
        lastTimeClicked = SystemClock.elapsedRealtime();
        if(p0 != null) performClick(p0);
    }


    //This is the method to call on click, must implement at child class
     abstract fun performClick(v: View);
}