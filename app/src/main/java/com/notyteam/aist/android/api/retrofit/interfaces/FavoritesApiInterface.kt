package com.notyteam.aist.android.api.retrofit.interfaces

import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CatalogListResponseById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.api.response.responses.BaseResponse
import io.reactivex.Single
import retrofit2.http.*

interface FavoritesApiInterface {

    @GET("users/{user_id}/favorites")
    fun getFavoritesList(@Path("user_id") user_id: Int): Single<CatalogListResponse>

    @FormUrlEncoded
    @POST("users/{user_id}/favorites")
    fun addFavorites(@Path("user_id") user_id: Int,@Field("product_id") product_id: Int): Single<BaseResponse>

    @DELETE("users/{user_id}/favorites/{favorite_id}")
    fun deleteFavorites(@Path("user_id") user_id: Int,@Path("favorite_id") favorite_id: Int): Single<BaseResponse>


}