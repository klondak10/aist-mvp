package com.notyteam.aist.android.ui.fragments.main.desires

import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.setting.settings.SettingsFragment
import com.notyteam.aist.android.ui.fragments.main.setting.settings.StoryPaymentFragment

class DesiresFragment : BaseFragment(), DesiresFragmentView {
    companion object {
        fun newInstance(): DesiresFragment {
            return DesiresFragment()
        }
    }

    override fun layout(): Int = R.layout.fragment_desires

    override fun initialization(view: View, isFirstInit: Boolean) {
        getSecondTypeToolbar()
                ?.setLeftIcon(R.drawable.settings)
                ?.setTitle(getString(R.string.bottom_bar_wishes))
                ?.toggleRightIcon(isVisible = false)
                ?.setOnLeftIconListener {
                    addFragment(SettingsFragment())
                }

        if (isFirstInit) {
            initViews()
        }
    }

    private var presenter: DesiresFragmentPresenter = DesiresFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    private fun initViews() {

    }
}
