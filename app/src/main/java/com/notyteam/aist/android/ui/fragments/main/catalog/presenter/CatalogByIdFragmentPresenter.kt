package com.notyteam.aist.android.ui.fragments.main.catalog.presenter

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.CategoryListGoodsByIdFragment

class CatalogByIdFragmentPresenter(view: CategoryListGoodsByIdFragment?): BasePresenter<CategoryListGoodsByIdFragment>(view) {


    fun getCategoryListById(id:Int){
        requests.add(aistApi.getCategoriesListById(id,{
            view?.onCatalogListByIdSuccess(it.data)
        }, { errorMessage, errorCode ->
            view?.onCatalogListError(errorMessage)
        }).sendRequest())
    }

}