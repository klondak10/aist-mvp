package com.notyteam.aist.android.ui.fragments.main.setting.settings

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter
import com.notyteam.aist.android.utils.ActorUser
import com.notyteam.aist.android.utils.paper.PaperIO
import im.aist.core.viewmodel.FileVMCallback
import im.aist.core.viewmodel.UserVM
import im.aist.runtime.files.FileSystemReference
import im.aist.sdk.aist.ActorSDK
import im.aist.sdk.util.ActorSDKMessenger
import im.aist.sdk.util.ActorSDKMessenger.messenger
import im.aist.sdk.util.images.common.ImageLoadException
import im.aist.sdk.util.images.ops.ImageLoading

class SettingsFragmentPresenter(view: SettingsFragmentView?): BasePresenter<SettingsFragmentView>(view) {

    private val KEY_AUTH = "auth_yes"
    private val KEY_AUTH_UID = "auth_uid"

    private val KEY_PHONE = "auth_phone"
    private val KEY_SMS_HASH = "auth_sms_hash"
    private val KEY_SMS_CODE = "auth_sms_code"

    val user: UserVM? by lazy {
        ActorUser.getActorUser()
    }

    fun getInterestsList(){
        requests.add(aistApi.getUserInterestsList({
            view?.onGettingInterestsListSuccess(it)
        }, { errorMessage, errorCode ->
            view?.onGettingInterestsListError(errorMessage)
        }).sendRequest())
    }

    fun getLocalAvatar(){
           user?.avatar?.get()?.fullImage?.fileReference?.fileId?.let { fileId ->
               val file = ActorSDKMessenger.messenger().findDownloadedDescriptor(fileId)
               if (file != null) {
                   try {
                       val bitmap = ImageLoading.loadBitmapOptimized(file)
                       view?.onAvatarLoaded(bitmap)
                       return
                   } catch (e: ImageLoadException) {
                       view?.onAvatarLoadError("Avatar loading error")
                       e.printStackTrace()
                   }
               } else {
                   getAvatar()
               }
           }
    }

    fun getAvatar(){
        messenger().bindFile(user?.avatar?.get()?.fullImage?.fileReference, true, object : FileVMCallback {
            override fun onNotDownloaded() {
                view?.onAvatarLoadError("Avatar loading error")
            }
            override fun onDownloading(progressV: Float) {}
            override fun onDownloaded(reference: FileSystemReference) {
                try {
                    val bitmap = ImageLoading.loadBitmapOptimized(reference.descriptor)
                    view?.onAvatarLoaded(bitmap)
                } catch (e: ImageLoadException) {
                    e.printStackTrace()
                    view?.onAvatarLoadError("Avatar loading error")
                }

            }
        })
        view?.onAvatarLoadError("Avatar loading error")

    }

    fun logout(){
        ActorSDK.sharedActor().messenger.terminateAllSessions()
        ActorSDK.sharedActor().messenger.resetAuth()
        ActorSDK.sharedActor().messenger.getPreferences().putBool(KEY_AUTH, false)
        ActorSDK.sharedActor().messenger.getPreferences().putInt(KEY_AUTH_UID, 0)
        ActorSDK.sharedActor().messenger.getPreferences().putLong(KEY_PHONE, 0)
        ActorSDK.sharedActor().messenger.getPreferences().putString(KEY_SMS_HASH, null)
        ActorSDK.sharedActor().messenger.getPreferences().putInt(KEY_SMS_CODE, 0)
//        ActorSDK.sharedActor().messenger.terminateSession(PaperIO.getSessionId())
        PaperIO.clearData()
        view?.onLogoutSuccess()
    }

}