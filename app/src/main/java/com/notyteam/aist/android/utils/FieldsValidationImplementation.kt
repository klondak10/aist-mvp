package com.notyteam.aist.android.utils

import android.telephony.PhoneNumberUtils
import android.text.TextUtils
import com.notyteam.aist.android.ui.FieldsValidationInterface
import java.util.regex.Pattern

object FieldsValidationImplementation: FieldsValidationInterface {

    override fun isNameValid(name: String): Boolean {
        var isValid = true
        var spaceCount = 0
        name.forEach { s ->
            run {
                if (!s.isLetter() && !s.isWhitespace()) {
                    isValid = false
                } else if (s.isWhitespace()) {
                    spaceCount++
                    if (spaceCount > 1) isValid = false
                } else spaceCount = 0
            }
        }

        //val r = name.matches(Regex("^[ A-Za-zА-Яа-я]+\$"))
        return isValid
    }

    override fun isPhoneValid(phone: String): Boolean {
        return PhoneNumberUtils.isGlobalPhoneNumber(phone) && (phone.length==13 || phone.length==14)
    }

    override fun isEmailValid(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    override fun isPasswordValid(password: String): Boolean {
        val pattern = "^[a-zа-яA-ZА-Я0-9_@#\$%^&+=]*\$"
        val result = Pattern.compile(pattern).matcher(password).matches()
        return result
    }

    override fun isPromoCodeValid(password: String): Boolean {
        val pattern = "^[a-zа-яA-ZА-Я0-9]*\$"
        val result = Pattern.compile(pattern).matcher(password).matches()
        return result
    }

    override fun isPasswordLongEnough(password: String): Boolean {
        return password.length >= 6
    }
}