package com.notyteam.aist.android.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.annotation.ArrayRes
import android.support.annotation.ColorRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.CursorLoader
import android.util.TypedValue
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.noty.aist.android.R
import com.robertlevonyan.components.picker.ItemModel
import com.robertlevonyan.components.picker.PickerDialog
import java.io.IOException
import java.io.InputStream
import java.util.*
import java.util.regex.Pattern


fun dpToPx(c: Context, sizeInDp: Int): Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, sizeInDp.toFloat(), c.resources.displayMetrics).toInt()
fun getStringArray(@ArrayRes stringArray: Int, c: Context?): Array<out String> = c?.resources?.getStringArray(stringArray)!!
fun getColorResource(@ColorRes colorResource: Int, c: Context?)  = ContextCompat.getColor(c!!, colorResource)

infix fun <T> Collection<T>.sameContentWith(collection: Collection<T>?)
        = collection?.let { this.size == it.size && this.containsAll(it) }?:false

fun toggleArrow(show: Boolean, view: View): Boolean {
    return toggleArrow(show, view, true)
}

fun toggleArrow(show: Boolean, view: View, delay: Boolean): Boolean {
    if (show) {
        view.animate().setDuration((if (delay) 200 else 0).toLong()).rotation(90f)
        return true
    } else {
        view.animate().setDuration((if (delay) 200 else 0).toLong()).rotation(0f)
        return false
    }
}

fun inputStreamToString(inputStream: InputStream): String {
    try {
        val bytes = ByteArray(inputStream.available())
        inputStream.read(bytes, 0, bytes.size)
        return String(bytes)
    } catch (e: IOException) {
        return ""
    }
}

@SuppressLint("Recycle")
fun getRealPathFromDocumentUri(context: Context, uri: Uri): String {
    var filePath = ""

    val p = Pattern.compile("(\\d+)$")
    val m = p.matcher(uri.toString())
    if (!m.find()) {
//        Log.e(ImageConverter::class.java!!.getSimpleName(), "ID for requested image not found: $uri")
        return filePath
    }
    val imgId = m.group()

    val column = arrayOf(MediaStore.Images.Media.DATA)
    val sel = MediaStore.Images.Media._ID + "=?"

    val cursor = context.contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            column, sel, arrayOf(imgId), null)

    cursor?.let {
        val columnIndex = it.getColumnIndex(column[0])

        if (it.moveToFirst()) {
            filePath = it.getString(columnIndex)
        }
        it.close()
    }

    return filePath
}

fun openImagePicker(fragment:Fragment, onResult: (uri: Uri) -> Unit)
{
    val pickerDialog = PickerDialog.Builder(fragment)
            .setListType(PickerDialog.TYPE_LIST)       // Type of the picker, must be PickerDialog.TYPE_LIST or PickerDialog.TYPE_Grid
            .setItems(
                    arrayListOf(
                            ItemModel(ItemModel.ITEM_CAMERA, "Сделать фото",0,true,itemBackgroundColor = R.color.colorWhite),
                            ItemModel(ItemModel.ITEM_GALLERY, "Выбрать фото",0,true,itemBackgroundColor = R.color.colorWhite)))         // List of ItemModel-s which should be in picker
            .setDialogStyle(PickerDialog.DIALOG_MATERIAL)    // PickerDialog.DIALOG_STANDARD (square corners) or PickerDialog.DIALOG_MATERIAL (rounded corners)
            .create()
    pickerDialog.show()
    pickerDialog.setPickerCloseListener { type, uri ->
        when (type) {
            ItemModel.ITEM_CAMERA -> {
                onResult(uri)
            }
            ItemModel.ITEM_GALLERY -> {
                onResult(uri)
            }
        }
    }
}


fun getLocale() = Locale("ru")

inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)
