package com.notyteam.aist.android.ui.sticky_recycle_view;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.noty.aist.android.R;
import com.shuhart.stickyheader.StickyAdapter;

import java.util.ArrayList;
import java.util.List;

public class SectionAdapter extends StickyAdapter<RecyclerView.ViewHolder, RecyclerView.ViewHolder> {
    public List<Section> items;
    private ArrayList<Object> mSectionPositions;
    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(Section section);
    }

    public SectionAdapter(List<Section> list, OnItemClickListener onItemClickListener){
        items = list;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == Section.HEADER || viewType == Section.CUSTOM_HEADER) {
            return new HeaderViewholder(inflater.inflate(R.layout.item_country_header, parent, false));
        }
        return new ItemViewHolder(inflater.inflate(R.layout.item_country, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = items.get(position).type();
        int section = items.get(position).sectionPosition();
        String country = items.get(position).country();
        String code = items.get(position).code();
        String letter = items.get(position).letter();
        if (type == Section.HEADER) {
            ((HeaderViewholder) holder).textView.setText(letter);
        } else if (type == Section.ITEM){
            ((ItemViewHolder) holder).textView.setText(country);
            ((ItemViewHolder) holder).code.setText(code);
        } else {
            ((HeaderViewholder) holder).textView.setText("Custom header");
        }
    }
    @Override
    public int getItemViewType(int position) {
        return items.get(position).type();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        try {
            return items.get(itemPosition).sectionPosition();
        }catch (Exception e){
            return itemPosition-1;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int headerPosition) {
        try {
            String letter = items.get(headerPosition).letter();
            ((HeaderViewholder) holder).textView.setText(letter);
        }catch (Exception e){}
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return createViewHolder(parent, Section.HEADER);
    }

//    @Override
//    public Object[] getSections() {
//        List<String> sections = new ArrayList<>(26);
//        mSectionPositions = new ArrayList<>(26);
//        for (int i = 0, size = items.size(); i < size; i++) {
//            String section = String.valueOf(items.get(i).letter().charAt(0)).toUpperCase();
//            if (!sections.contains(section)) {
//                sections.add(section);
//                mSectionPositions.add(i);
//            }
//        }
//        return sections.toArray(new String[0]);
//    }

//    @Override
//    public int getPositionForSection(int sectionIndex) {
//        return (int) mSectionPositions.get(sectionIndex);
//    }
//
//    @Override
//    public int getSectionForPosition(int position) {
//        return 0;
//    }

    public class HeaderViewholder extends RecyclerView.ViewHolder {
        TextView textView;

        public HeaderViewholder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_view);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView textView,code;

        ItemViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.text_view);
            code = itemView.findViewById(R.id.code);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null)
                        onItemClickListener.onItemClick(items.get(getAdapterPosition()));
                }
            });
        }
    }
}
