package com.notyteam.aist.android.ui

import android.support.v4.app.Fragment
import com.notyteam.aist.android.ui.activities.BaseActivity

interface FragmentManagerInterface {
    fun pushBottomBarFragment(fragment: Fragment)

    fun popBottomBarFragment()

    fun replaceFragment(newFragment: Fragment)

    fun replaceFragment(newFragment: Fragment, animate: Boolean)

    fun addFragment(newFragment: Fragment)

    fun addFragmentWithAdd(newFragment: Fragment)

    fun popBackStackTillEntry(entryIndex: Int)

    interface ActivityInterface {
        fun getFragmentInterface(): FragmentManagerInterface
        fun getActivity(): BaseActivity
    }

    companion object {
        val empty: FragmentManagerInterface = object : FragmentManagerInterface {
            override fun pushBottomBarFragment(fragment: Fragment) {}
            override fun popBottomBarFragment() {}

            override fun replaceFragment(newFragment: Fragment) {}
            override fun replaceFragment(newFragment: Fragment, animate: Boolean) {}
            override fun addFragment(newFragment: Fragment) {}
            override fun addFragmentWithAdd(newFragment: Fragment) {}
            override fun popBackStackTillEntry(entryIndex: Int) {}
        }
    }
}