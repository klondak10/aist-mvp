package com.notyteam.aist.android.ui.fragments.main.setting.settings

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.ui.activities.splash.SplashActivity
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.desires.DesiresFragment
import com.notyteam.aist.android.ui.fragments.main.setting.policy.PolicyFragment
import com.notyteam.aist.android.ui.fragments.main.setting.profile.ProfileFragment
import com.notyteam.aist.android.ui.models.ui.UICategories
import com.notyteam.aist.android.utils.ActorUser
import im.aist.sdk.controllers.settings.BlockedListActivity
import im.aist.sdk.controllers.settings.ChatSettingsActivity
import im.aist.sdk.controllers.settings.NotificationsActivity
import im.aist.sdk.controllers.settings.WallpapersAdapter
import im.aist.sdk.view.adapters.HeaderViewRecyclerAdapter
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_settings_main.*
import android.content.pm.PackageManager


class SettingsFragment : BaseFragment(), SettingsFragmentView,View.OnClickListener {

    private var wallpaperAdapter: HeaderViewRecyclerAdapter? = null
    companion object {
        fun newInstance(): StoryPaymentFragment {
            return StoryPaymentFragment()
        }
    }

    private var presenter: SettingsFragmentPresenter = SettingsFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    var selectedInterestsList = arrayListOf<UICategories>()

    override fun layout(): Int = R.layout.fragment_settings_main

    override fun initialization(view: View, isFirstInit: Boolean) {

        getFirstTypeToolbar()
                ?.setLeftText(getString(R.string.bottom_bar_wishes))
                ?.setTitle(getString(R.string.settings))
                ?.setOnBackPressedListener{
                    onBackPressed()
                }

        activityBottomBarInterface.showBottomBar(true)

        if (isFirstInit) {
            initViews()
            getInterests()
        }
    }

    private fun initViews() {

        try {
            val pInfo = context!!.packageManager.getPackageInfo(activity!!.getPackageName(), 0)
            val version = pInfo.versionName
            versionCode.text = "Version "+version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

            initProfileLayout()
            rl_profile.setOnClickListener(this)
            rl_notification.setOnClickListener(this)
            rl_number.setOnClickListener(this)
            rl_media.setOnClickListener(this)
            rl_policy.setOnClickListener(this)
            tv_exit.setOnClickListener(this)
            rl_black_list.setOnClickListener(this)
            rl_problem.setOnClickListener(this)

            //Wallpaper
            wallpaperAdapter = HeaderViewRecyclerAdapter(WallpapersAdapter())
            wallpapers_list.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            wallpapers_list.adapter = wallpaperAdapter

    }

    private fun initProfileLayout(){
        getLocalAvatar()

        val user = ActorUser.getActorUser()
        tv_name.text = user?.name?.get()
        tv_categories.text = selectedInterestsList.joinToString { it.name }

        if(Paper.book().exist("birthday")){
            tv_birth_date.setText(Paper.book().read<Any>("birthday").toString())
        }else{
            tv_birth_date.setText("")
        }
    }

    override fun onFragmentResult(result: Any) {
        selectedInterestsList = result as ArrayList<UICategories>
        initProfileLayout()
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.rl_profile -> {
                val profile = ProfileFragment()
                prepareFragmentForResult(profile)
                profile.selectedInterestsList = selectedInterestsList
                addFragment(profile)
            }

            R.id.rl_notification -> {
                startActivity(Intent(activity, NotificationsActivity::class.java))
            }
            R.id.rl_media ->{
                startActivity(Intent(activity, ChatSettingsActivity::class.java))
            }
            R.id.rl_policy ->{
                addFragment(PolicyFragment())
            }
            R.id.tv_exit->{
                logout()
            }
            R.id.rl_problem->{
                val user = ActorUser.getActorUser()

                val emailIntent = Intent(Intent.ACTION_SENDTO)
                emailIntent.data = Uri.parse("mailto:" + "Aist" + "<info@aist.im>")
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Пользователь Aist:"+user?.name?.get())
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Суть проблемы: ")

                try {
                    startActivityForResult(Intent.createChooser(emailIntent, "Send email using..."),100)
                } catch (ex: android.content.ActivityNotFoundException) {
                    Toast.makeText(activity, "No email clients installed.", Toast.LENGTH_SHORT).show()
                }

            }
            R.id.rl_black_list->{
                startActivity(Intent(activity, BlockedListActivity::class.java))
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 100) {
            addFragment(DesiresFragment())
            addFragment(StoryPaymentFragment())
        }
    }

    private fun getInterests(){
        showProgress(true)
        presenter.getInterestsList()
    }

    private fun logout(){
        presenter.logout()
    }

    private fun getLocalAvatar() {
        presenter.getLocalAvatar()
    }


    /*Getting interests*/
    override fun onGettingInterestsListSuccess(response: InterestsListResponse) {
        showProgress(false)
        selectedInterestsList = response.data.map { UICategories(it.translatedName, it.id) } as ArrayList<UICategories>
        tv_categories?.text = selectedInterestsList.joinToString { it.name }
    }
    override fun onGettingInterestsListError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }
    /*end*/


    /*Getting avatar*/
    override fun onAvatarLoaded(avatar: Bitmap?) {
        Glide.with(baseContext)
                .load(avatar?:"")
                .apply(RequestOptions()
                        .override(300, 300)
                        .encodeQuality(60)
                        .centerCrop()
                        .fitCenter())
                .into(iv_avatar)
    }
    override fun onAvatarLoadError(errorMessage: String) {
        iv_avatar.setBackgroundResource(R.drawable.avatar)
    }
    /*end*/


    /*Logout*/
    override fun onLogoutSuccess() {
        finish()
        SplashActivity.start(baseActivity, shouldOpenLogin = true)
    }
    override fun onLogoutError() {}
    /*end*/


    override fun onResume() {
        super.onResume()
        if (wallpaperAdapter != null) {
            (wallpaperAdapter?.getWrappedAdapter() as WallpapersAdapter).setSelected(activity!!.getSharedPreferences("wallpaper", Context.MODE_PRIVATE)!!.getInt("wallpaper", 0))
        }
    }
}
