package com.notyteam.aist.android.utils

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

fun recyclerViewTwoColumn(recyclerView: RecyclerView) {
    val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(recyclerView.context, 2)
    recyclerView.layoutManager = layoutManager
}

fun recyclerViewFourthColumn(recyclerView: RecyclerView) {
    val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(recyclerView.context, 4)
    recyclerView.layoutManager = layoutManager
}

fun recyclerViewSevenColumn(recyclerView: RecyclerView) {
    val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(recyclerView.context, 7)
    recyclerView.layoutManager = layoutManager
}

fun recyclerViewTwoColumnHorizontal(recyclerView: RecyclerView) {
    val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(recyclerView.context, 2,
            GridLayoutManager.HORIZONTAL, false)
    recyclerView.layoutManager = layoutManager
}

fun recyclerViewVertical(recyclerView: RecyclerView) {
    val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.layoutManager = layoutManager
}


fun recyclerViewHorizontal(recyclerView: RecyclerView) {
    val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(recyclerView.context,
            LinearLayoutManager.HORIZONTAL, false)
    recyclerView.layoutManager = layoutManager
}

fun RecyclerView.verticalLayout() {
    val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
    this.layoutManager = layoutManager
}