package com.notyteam.aist.android.api.response.interest.all_interests

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("translated_name")
	val translatedName: String = "",

	@field:SerializedName("original_name")
	val originalName: String = "",

	@field:SerializedName("id")
	val id: Int = 0
)