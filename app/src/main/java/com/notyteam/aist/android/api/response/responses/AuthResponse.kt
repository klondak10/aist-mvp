package com.notyteam.aist.android.api.response.responses


import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
open class AuthResponse(
		@field:SerializedName("success")
	    var success: Boolean = false
): BaseResponse()