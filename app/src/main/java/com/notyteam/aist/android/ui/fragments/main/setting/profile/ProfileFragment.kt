package com.notyteam.aist.android.ui.fragments.main.setting.profile

import android.content.Intent
import android.graphics.Bitmap
import android.os.StrictMode
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.ui.activities.main.MainActivity
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.setting.categories.CategoriesFragment
import com.notyteam.aist.android.ui.models.ui.UICategories
import com.notyteam.aist.android.utils.ActorUser
import com.notyteam.aist.android.utils.DatePickerDialog
import com.notyteam.aist.android.utils.sameContentWith
import im.aist.sdk.controllers.fragment.preview.ViewAvatarActivity
import im.aist.sdk.util.ActorSDKMessenger.myUid
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_profile_main.*
import java.util.*


class ProfileFragment : BaseFragment(), ProfileFragmentView {
    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }

        const val AVATAR_RESULT_CODE = 123
    }


    private var presenter: ProfileFragmentPresenter = ProfileFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    var selectedInterestsList = arrayListOf<UICategories>()
    var isFirstInit = false

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            validateFields()
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    }

    override fun layout(): Int = R.layout.fragment_profile_main

    override fun initialization(view: View, isFirstInit: Boolean) {
        getFirstTypeToolbar()
                ?.setRightText(getString(if (this.isFirstInit) R.string.next else R.string.complete))
                ?.setLeftText(getString(R.string.settings))
                ?.setOnBackPressedListener {
                    onBackPressed()
                }
                ?.setEnabledRightText(this.isFirstInit)
                ?.setOnRightTextListener {
                    saveProfileData()
                }

        if (isFirstInit) {
            initViews()
        }
    }

    private fun initViews() {

        getLocalAvatar()

        val user = ActorUser.getActorUser()

        name_edit_text.setText(user?.name?.get())
        email_edit_text.setText(user?.nick?.get()?.replace("__","@")?.replace("_","."))
        if(Paper.book().exist("birthday")){
            birthday_edit_text.setText(Paper.book().read<Any>("birthday").toString())
        }else{
            birthday_edit_text.setText("")
        }

        rl_photo.setOnClickListener {
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
            startActivityForResult(ViewAvatarActivity.viewAvatar(myUid(), baseActivity), AVATAR_RESULT_CODE)
        }

        birthday_edit_text.apply {
            setOnClickListener {
                DatePickerDialog.show(text.toString(), baseActivity) { calendar, picker ->
                    val currentCalendar = Calendar.getInstance()
                    if (calendar.time.before(currentCalendar.time)) {
                        setText(DatePickerDialog.getUiDate(calendar))
                        picker.dismiss()
                    } else {
                        showErrorSnack(getString(R.string.incorrect_birth))
                    }
                }
            }
        }

        interests_edit_text.setOnClickListener {
            val fragment = CategoriesFragment()
            prepareFragmentForResult(fragment)
            fragment.selectedInterestsList = selectedInterestsList
            addFragment(fragment)
        }

        if (selectedInterestsList.isEmpty()) getInterests()
        else setInterestsToEditText(selectedInterestsList.joinToString { it.name })


        name_edit_text.addTextChangedListener(textWatcher)
        birthday_edit_text.addTextChangedListener(textWatcher)
        email_edit_text.addTextChangedListener(textWatcher)
    }

    override fun onFragmentResult(result: Any) {
        try {
            val selectedInterestsList = result as ArrayList<UICategories>
            if (!(this.selectedInterestsList sameContentWith selectedInterestsList)) {
                setInterestsToEditText(selectedInterestsList.joinToString { it.name })
                this.selectedInterestsList = selectedInterestsList
                getFirstTypeToolbarInstance()
                        ?.setEnabledRightText(true)
            }
        } catch (e: Exception) {
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AVATAR_RESULT_CODE) {
            getLocalAvatar()
            setResult(selectedInterestsList)
        }
    }

    private fun setInterestsToEditText(interests: String) {
        interests_edit_text?.setText(interests)
    }

    private fun getUserModel(): ProfileFragmentPresenter.User {
        val interests = selectedInterestsList.map { it.id } as ArrayList<Int>
        return ProfileFragmentPresenter.User(
                name_edit_text.text.toString(),
                email_edit_text.text.toString(),
                birthday_edit_text.text.toString(),
                interests)
    }

    private fun saveProfileData() {
        showProgress(true)
        baseActivity.toggleKeyboard(false)
        presenter.setUserData(getUserModel())
    }

    private fun getInterests() {
        showProgress(true)
        presenter.getInterestsList()
    }

    private fun getLocalAvatar() {
        presenter.getLocalAvatar()
    }

//    private fun getAvatar() {
//        presenter.getAvatar()
//    }

    fun validateFields() {
        var isValid = true
        isValid = isValid.and(name_edit_text.text.toString().trim().isNotEmpty())
        isValid = isValid.and(email_edit_text.text.toString().trim().isNotEmpty() &&
                email_edit_text.text.toString().length > 5 && email_edit_text.text.toString().length < 32)

        getFirstTypeToolbarInstance()
                ?.setEnabledRightText(isValid)
    }


    /*Getting avatar*/
    override fun onAvatarLoaded(avatar: Bitmap?) {
        Glide.with(baseContext)
                .load(avatar ?: "")
                .apply(RequestOptions()
                        .override(300, 300)
                        .encodeQuality(60)
                        .centerCrop()
                        .fitCenter())
                .into(iv_avatar)
    }

    override fun onAvatarLoadError(errorMessage: String) {
        iv_avatar.setBackgroundResource(R.drawable.avatar)
    }
    /*end*/


    /*Getting interests*/
    override fun onGettingInterestsListSuccess(response: InterestsListResponse) {
        showProgress(false)
        selectedInterestsList = response.data.map { UICategories(it.translatedName, it.id) } as ArrayList<UICategories>
        setInterestsToEditText(response.data.joinToString { it.translatedName })
    }

    override fun onGettingInterestsListError(errorMessage: String) {
        showProgress(false)
    }
    /*end*/


    /*Saving interests*/
    override fun onSettingInterestsListSuccess() {}

    override fun onSettingInterestsListError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }
    /*end*/


    /*Profile update*/
    override fun onProfileUpdateSuccess() {
        showProgress(false)

        if (isFirstInit) {
            MainActivity.start(baseContext)
        } else {
            setResult(selectedInterestsList)
            showSuccessSnack(getString(R.string.profile_update_success))
            onBackPressed()
        }
    }

    override fun onProfileUpdateError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }
    /*end*/


}
