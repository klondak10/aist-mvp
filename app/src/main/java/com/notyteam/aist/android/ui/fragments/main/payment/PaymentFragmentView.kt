package com.notyteam.aist.android.ui.fragments.main.payment

import com.notyteam.aist.android.api.response.payment.BindQrResponse
import com.notyteam.aist.android.api.response.payment.FindQrResponse
import com.notyteam.aist.android.mvp.PresenterDetachInterface

interface PaymentFragmentView: PresenterDetachInterface {

    fun onBindTheCardBuyerSuccess(response: BindQrResponse)
    fun onFindQrSuccess(response: FindQrResponse)
    fun onFindQrError(errorMessage: String)



}