package com.notyteam.aist.android.api.retrofit.interfaces

import com.notyteam.aist.android.api.response.responses.TokenResponse
import io.reactivex.Single
import retrofit2.http.*

interface TokenApiInterface {

    @FormUrlEncoded
    @POST("tokens/apple")
    fun setToken(@Field("token") token: String): Single<TokenResponse>
}