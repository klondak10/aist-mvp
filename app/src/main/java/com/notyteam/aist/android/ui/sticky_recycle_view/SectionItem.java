package com.notyteam.aist.android.ui.sticky_recycle_view;

public class SectionItem implements Section {

    private int section;
    private String country;
    private String code,letter, countryCode = "";
    public SectionItem(int section) {
        this.section = section;
    }

    public SectionItem(int section, String country, String code) {
        this.section = section;
        this.country = country;
        this.code = code;
    }

    public SectionItem(int section, String country, String code, String letter, String countryCode) {
        this.section = section;
        this.country = country;
        this.code = code;
        this.letter = letter;
        this.countryCode = countryCode;
    }

    @Override
    public int type() {
        return ITEM;
    }

    @Override
    public int sectionPosition() {
        return section;
    }

    @Override
    public String country() {
        return country;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String letter() {
        return letter;
    }

    @Override
    public String countryCode() {
        return countryCode;
    }
}
