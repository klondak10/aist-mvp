package com.notyteam.aist.android.ui.fragments.main.dialogs

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter

class DialogsFragmentPresenter(view: DialogsFragmentView?): BasePresenter<DialogsFragmentView>(view) {


    fun setToken(){
        requests.add(aistApi.setToken({
            view?.onTokenSuccess(it)
        }, { errorMessage, errorCode ->
            view?.onTokenError(errorMessage)
        }).sendRequest())
    }

    fun getInterestsList() {
        requests.add(aistApi.getUserInterestsList({
            view?.onGettingInterestsListSuccess(it)
        }, { errorMessage, errorCode ->
            view?.onGettingInterestsListError(errorMessage)
        }).sendRequest())
    }

}