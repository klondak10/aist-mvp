package com.notyteam.aist.android.api.retrofit.interfaces

import com.google.gson.JsonObject
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.api.response.payment.BindQrResponse
import com.notyteam.aist.android.api.response.payment.FindQrResponse
import com.notyteam.aist.android.api.response.responses.AuthResponse
import com.notyteam.aist.android.api.response.responses.BaseResponse
import io.reactivex.Single
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.http.*

interface PaymentApiInterface {

    @GET("qrs")
    fun findQr(@Query("filter[qrData]") filter: String): Single<FindQrResponse>

    @Headers("Content-Type: application/vnd.api+json")
    @POST("buyers/{id}/cards")
    fun bindTheCardBuyer(@Path("id") id: String,
                         @Body data: RequestBody): Single<BindQrResponse>

}