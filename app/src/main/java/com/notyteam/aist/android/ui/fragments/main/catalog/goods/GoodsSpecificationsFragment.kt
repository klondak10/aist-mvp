package com.notyteam.aist.android.ui.fragments.main.catalog.goods

import android.os.Bundle
import android.util.Log
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.catalog.CategoriesItem
import com.notyteam.aist.android.ui.adapters.catalog.SpecificationGoodsAdapter
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import com.notyteam.aist.android.ui.models.ui.catalog.UIGoodsSpecification
import com.notyteam.aist.android.utils.recyclerViewVertical
import kotlinx.android.synthetic.main.fragment_goods_specifications.*

class GoodsSpecificationsFragment : BaseFragment() {

    companion object {
        private const val SPECIFICATION_KEY = "SPECIFICATION_KEY"
        fun newInstance(specification:String): GoodsSpecificationsFragment {
            val f = GoodsSpecificationsFragment ()
            // Pass index input as an argument.
            val args = Bundle()
            args.putString(SPECIFICATION_KEY, specification)
            f.setArguments(args)
            return f
        }
        fun getSpecification(arguments: Bundle?) = arguments?.getString(SPECIFICATION_KEY)?:""
    }
    override fun layout(): Int = R.layout.fragment_goods_specifications
    lateinit var adapter: SpecificationGoodsAdapter
    override fun initialization(view: View, isFirstInit: Boolean) {
        if (isFirstInit) {
            initViews()
        }
    }

    private fun initViews() {

        recyclerViewVertical(rv_specification)
        adapter = SpecificationGoodsAdapter(arrayListOf())
        rv_specification.adapter = adapter

            val lstValues: List<String> = getSpecification(arguments).split("\",").map { it -> it.trim().replace("{","")
                    .replace("}","").replace("\"","")}
            val data: ArrayList<UIGoodsSpecification> = arrayListOf()
            lstValues.forEach { it ->
                if(it.contains(":")){
                    val strs = it.split(":").toTypedArray()
                    data.add(UIGoodsSpecification(strs[0], strs[1]))
                }
            }
            adapter.clear()
            adapter.addAll(data)






    }

}
