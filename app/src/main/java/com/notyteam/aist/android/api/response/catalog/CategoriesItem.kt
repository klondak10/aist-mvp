package com.notyteam.aist.android.api.response.catalog

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class CategoriesItem(

		@field:SerializedName("is_favorite")
		val is_favorite: Boolean? = null,

		@field:SerializedName("in_wish_list")
		val in_wish_list: Boolean? = null,

		@field:SerializedName("images")
		val images: ArrayList<String> = arrayListOf(),

		@field:SerializedName("price")
		val price: Int = 0,

		@field:SerializedName("id")
		val id: Int = 0,

		@field:SerializedName("name")
		val name: String = "",

		@field:SerializedName("has_sub_categories")
		var has_sub_categories: Boolean? = null
)