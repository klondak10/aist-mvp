package com.notyteam.aist.android.ui.fragments.main.setting.settings

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.adapters.StoryPaymentAdapter
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.utils.recyclerViewVertical
import kotlinx.android.synthetic.main.fragment_story_payment.*


class StoryPaymentFragment : BaseFragment(), StoryPaymentFragmentView {


    lateinit var adapter: StoryPaymentAdapter

            companion object {
        fun newInstance(): StoryPaymentFragment {
            return StoryPaymentFragment()
        }
    }

    private var presenter: StoryPaymentFragmentPresenter = StoryPaymentFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    override fun layout(): Int = R.layout.fragment_story_payment

    override fun initialization(view: View, isFirstInit: Boolean) {
        getFirstTypeToolbar()
                ?.setLeftText(getString(R.string.story_payment))
                ?.hideCenterText()
                ?.setOnBackPressedListener{
                    onBackPressed()
                }

        activityBottomBarInterface.showBottomBar(true)

        if (isFirstInit) {
            initViews()

        }
    }

    private fun initViews() {

        recyclerViewVertical(list_story_payment)
        adapter = StoryPaymentAdapter(arrayListOf(),
                onClick = {

                    val dialog = Dialog(context)
                    dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
                    dialog .setCancelable(false)
                    dialog .setContentView(R.layout.dialog_result)

                    val edSumm = dialog .findViewById(R.id.ed_summ) as EditText
                    val txtResult = dialog .findViewById(R.id.txt_result) as TextView
                    val yesBtn = dialog .findViewById(R.id.yesBtn) as Button
                    val noBtn = dialog .findViewById(R.id.noBtn) as Button


                     yesBtn.visibility = View.GONE
                     noBtn.text = "закрыть"
                     edSumm.visibility = View.GONE
                     txtResult.text = "Ваш чек успешно отправлен на e-mail"


                    yesBtn.setOnClickListener { dialog .dismiss() }

                    noBtn.setOnClickListener { dialog .dismiss() }

                    dialog .show()


        })

        adapter.add("")
        adapter.add("")
        adapter.add("")

        list_story_payment.adapter = adapter


    }


}
