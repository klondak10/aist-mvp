package com.notyteam.aist.android.ui.fragments.registration.phone

import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Editable
import android.text.InputFilter
import android.view.View
import android.view.WindowManager
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_phone.*
import com.notyteam.aist.android.ui.fragments.main.setting.policy.PolicyFragment
import com.notyteam.aist.android.ui.fragments.registration.countries.CountriesFragment
import com.notyteam.aist.android.ui.fragments.registration.sms.SmsFragment
import com.notyteam.aist.android.ui.sticky_recycle_view.Section
import com.notyteam.aist.android.ui.sticky_recycle_view.SectionItem
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.NumberParseException

class PhoneFragment : BaseFragment(), PhoneFragmentView {
    companion object {
        fun newInstance(): PhoneFragment {
            return PhoneFragment()
        }
    }

    private var presenter: PhoneFragmentPresenter = PhoneFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    var phoneUtil = PhoneNumberUtil.getInstance()
    var sectionCode: Section = SectionItem(0, "Россия", "+7", "", "RU")
    var phoneWatcher = getPhoneWatcher()

    override fun layout(): Int = R.layout.fragment_phone

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.setTransparentStatusBar(isTransparent = false, isDarkTexts = true)
        baseActivity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        hideToolbar()
        if (isFirstInit) {
            initViews()
        }
    }

    private fun initViews() {
        setHyperLink(text_view_privacy_policy, R.string.privacy_policy_hyper_text) {
            addFragment(PolicyFragment())
        }

        button_next.isEnabled = false

        text_view_country.setOnClickListener {
            val fragment = CountriesFragment()
            prepareFragmentForResult(fragment)
            fragment.sectionCode = sectionCode
            addFragment(fragment)
        }



        text_view_country.text = sectionCode.country()
        text_view_phone_code.text = sectionCode.code()
        edit_text_sms.addTextChangedListener(phoneWatcher)

        button_next.setOnClickListener {
            button_next.isEnabled = false
            try {
                val numberProto = phoneUtil.parse(edit_text_sms.text.toString(), sectionCode.countryCode())
                val phone = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)
                checkPhone(phone.removePrefix("+").toLong())
            } catch (e: NumberParseException) {

            }
        }
    }

    private fun checkPhone(phone: Long){
        showProgress(true)
        presenter.sendPhone(phone)
    }

    override fun onFragmentResult(result: Any) {
        sectionCode = (result as Section)
        text_view_country.text = sectionCode.country()
        text_view_phone_code.text = sectionCode.code()

        edit_text_sms.removeTextChangedListener(phoneWatcher)
        edit_text_sms.setText("")
        phoneWatcher = getPhoneWatcher()
        edit_text_sms.addTextChangedListener(phoneWatcher)
    }

    private fun getPhoneWatcher() = object: PhoneNumberFormattingTextWatcher(sectionCode.countryCode()) {
        override fun afterTextChanged(s: Editable?) {
            super.afterTextChanged(s)
            try {
                val numberProto = phoneUtil.parse(s.toString(), sectionCode.countryCode())
                val phone = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)
                val isValid = phoneUtil.isValidNumber(numberProto)
                button_next.isEnabled = isValid

                if(isValid){
                    if(sectionCode.countryCode()=="UA"){
                        edit_text_sms.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(phone.removePrefix("+").length-3)))
                    }else{
                        edit_text_sms.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(phone.removePrefix("+").length-1)))
                    }
                }

            } catch (e: NumberParseException) {
                button_next.isEnabled = false
            }
        }
    }

    override fun onPhoneResult(hash: String) {
        showProgress(false)
        button_next.isEnabled = true
        try {
            val numberProto = phoneUtil.parse(edit_text_sms.text.toString(), sectionCode.countryCode())
            val phone = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

            val bundle = Bundle()
            bundle.putString(SmsFragment.PHONE, phone)
            bundle.putString(SmsFragment.HASH, hash)
            val fragment = SmsFragment()
            fragment.arguments = bundle
            addFragment(fragment)
        } catch (e: NumberParseException) {

        }
    }

    override fun onPhoneError(e: Exception) {
        showProgress(false)
        button_next.isEnabled = true
        showErrorSnack(e.message.toString())
    }
//    class MyTextWatcher(private val et: EditText, val onTextChanged: (text: String) -> String) : TextWatcher {
//        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
//        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
//        override fun afterTextChanged(s: Editable) {
//            // Unregister self before update
//            et.removeTextChangedListener(this)
//
//            // The trick to update text smoothly.
//            val text = this.onTextChanged(s.toString())
//            s.replace(0, s.length, text)
//
//            // Re-register self after update
//            et.addTextChangedListener(this)
//        }
//    }

//    fun getTextWatcher(onTextChanged: (text: String) -> String) = MyTextWatcher(edit_text_phone, onTextChanged)

}
