package com.notyteam.aist.android.ui

import android.view.View

interface ToolbarHolderInterface {
    fun initFirstTypeToolbar(view: View?)
    fun initSecondTypeToolbar(view: View?)

    companion object {
        val empty: ToolbarHolderInterface = object: ToolbarHolderInterface {
            override fun initFirstTypeToolbar(view: View?) {}
            override fun initSecondTypeToolbar(view: View?) {}
        }
    }
}