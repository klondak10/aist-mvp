package com.notyteam.aist.android.ui.fragments.main.notifications

import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment

class NotificationsFragment : BaseFragment(), NotificationsFragmentView {
    companion object {
        fun newInstance(): NotificationsFragment {
            return NotificationsFragment()
        }
    }

    override fun layout(): Int = R.layout.fragment_desires

    override fun initialization(view: View, isFirstInit: Boolean) {
        getSecondTypeToolbar()
                ?.setTitle(getString(R.string.bottom_bar_notifications))
                ?.toggleLeftIcon(isVisible = false)
                ?.toggleRightIcon(isVisible = false)

        if (isFirstInit) {
            initViews()
        }
    }

    private var presenter = NotificationsFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    private fun initViews() {

    }
}
