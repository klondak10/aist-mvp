package com.notyteam.aist.android.ui.adapters.view_pager

import android.support.v4.app.FragmentManager
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.tutorial.TutorialPagerFragment
import com.notyteam.aist.android.utils.CustomViewPager
import com.notyteam.aist.android.utils.getStringArray
import java.lang.Exception

class TutorialPagerAdapter (fm: FragmentManager,
                            pager: CustomViewPager,
                            fragments: ArrayList<BaseFragment> = arrayListOf()) : BasePagerAdapter(fm, pager, fragments) {

    init {
        this.fragments.clear()
        for (i in 0..2) {
            val fragment = TutorialPagerFragment()
            fragment.title_tut = getStringArray(R.array.tutorial_titles, context)[i]
            fragment.description = getStringArray(R.array.tutorial_descriptions, context)[i]
            fragment.page = i
            this.fragments.add(fragment)
        }
    }

    override fun getTitle(pageIndex: Int): String {
        var title = ""
        try { title = getStringArray(R.array.tutorial_titles, context)[pageIndex] }
        catch (e: Exception) {}
        return title
    }
}