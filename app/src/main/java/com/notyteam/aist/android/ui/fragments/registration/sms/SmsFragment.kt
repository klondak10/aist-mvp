package com.notyteam.aist.android.ui.fragments.registration.sms

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.registration.about_yourself.AboutYourselfFragment
import com.notyteam.aist.android.ui.fragments.registration.phone.PhoneFragmentPresenter
import com.notyteam.aist.android.ui.fragments.registration.phone.PhoneFragmentView
import im.aist.core.network.RpcException
import kotlinx.android.synthetic.main.fragment_result_phone.*

class SmsFragment : BaseFragment(), SmsFragmentView, PhoneFragmentView {
    companion object {
        const val PHONE = "number"
        const val HASH = "hash"
        fun newInstance(): SmsFragment {
            return SmsFragment()
        }
    }

    private var phonePresenter: PhoneFragmentPresenter = PhoneFragmentPresenter(this)
    private var smsPresenter: SmsFragmentPresenter = SmsFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterfacesList = arrayListOf(phonePresenter, smsPresenter)
    }

    private var number: String? = ""
    private var hashCode: String = ""

    override fun layout(): Int = R.layout.fragment_result_phone

    var noCodeListener = View.OnClickListener {
        text_view_message.text = getString(R.string.activation_info_resend)
        text_view_resend_code.text = getString(R.string.change_number)
        text_view_resend_code.setOnClickListener(changeNumberListener)
        resendCode(number)
    }

    var changeNumberListener = View.OnClickListener {
        onBackPressed()
    }

    override fun initialization(view: View, isFirstInit: Boolean) {
        getFirstTypeToolbar()
                ?.setLeftText(getString(R.string.back))
                ?.setOnBackPressedListener {
                    onBackPressed()
                }
        if (isFirstInit) {
            initViews()
            startSmsTimer()
        }
    }

    private fun initViews() {
        number = arguments?.getString(PHONE,"")
        hashCode = arguments?.getString(HASH,"")?:""
        text_view_number.text = number

        text_view_resend_code.setOnClickListener(noCodeListener)
        edit_text_sms.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable) {
                if(s.length==5) {
                    validateCode(s.toString())
                }
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    private fun validateCode(code: String) {
        showProgress(true)
        edit_text_sms.isEnabled = false
        smsPresenter.validateCode(hashCode, code)
    }

    private fun resendCode(phone: String?) {
        showProgress(true)
        phonePresenter.sendPhone(phone?.removePrefix("+")?.toLong()?:0)
    }

    private fun startSmsTimer() {
        text_view_resend_code.isEnabled = false
        smsPresenter.startSmsResendTimer()
    }

    private fun stopSmsTimer() {
        smsPresenter.stopTimer()
    }


    /*Timer for sms resend*/
    override fun onResendTimerTick(time: Long) {
        text_view_resend_code.text = getString(R.string.no_code_timer, smsPresenter.timerDuration - time)
    }

    override fun onResendTimerCompleted() {
        text_view_resend_code.text = getString(R.string.no_code)
        text_view_resend_code.isEnabled = true
    }
    /*end*/


    /*Sms validation*/
    override fun onValidateSuccess() {
        showProgress(false)
        newRootFragment(AboutYourselfFragment())
    }

    override fun onValidateError(e: Exception) {

        showProgress(false)
        edit_text_sms.isEnabled = true
        showErrorSnack(e.message.toString())



    }

    override fun onInvalidCode(e: RpcException) {
        showProgress(false)
        edit_text_sms.isEnabled = true
        showErrorSnack(getString(R.string.sms_validation_error))
    }

    override fun onCodeExpired(e: RpcException) {
        showProgress(false)
        edit_text_sms.isEnabled = true
        stopSmsTimer()
        showErrorSnack(getString(R.string.sms_code_expired))
    }
    /*end*/


    /*Sms resending*/
    override fun onPhoneResult(hash: String) {
        showProgress(false)
        edit_text_sms.isEnabled = true
        showSuccessSnack(getString(R.string.sms_code_sent_success))
        hashCode = hash
    }

    override fun onPhoneError(e: Exception) {
        showProgress(false)
        showErrorSnack(e.message.toString())
    }
    /*end*/


    /*Auth error*/
    override fun onCompleteAuthError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }
    /*end*/
}
