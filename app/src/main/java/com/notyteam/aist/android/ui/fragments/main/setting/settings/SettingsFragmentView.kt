package com.notyteam.aist.android.ui.fragments.main.setting.settings

import android.graphics.Bitmap
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.mvp.PresenterDetachInterface

interface SettingsFragmentView: PresenterDetachInterface {
    fun onLogoutSuccess()
    fun onLogoutError()

    fun onGettingInterestsListSuccess(response: InterestsListResponse)
    fun onGettingInterestsListError(errorMessage: String)

    fun onAvatarLoaded(avatar: Bitmap?)
    fun onAvatarLoadError(errorMessage: String)
}