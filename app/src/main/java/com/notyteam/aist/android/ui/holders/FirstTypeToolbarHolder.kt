package com.notyteam.aist.android.ui.holders

import android.opengl.Visibility
import android.support.annotation.DrawableRes
import android.view.View
import com.notyteam.aist.android.utils.getColorResource
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.toolbar.view.*

class FirstTypeToolbarHolder(override val containerView: View?): LayoutContainer {

    fun showFirstTypeToolbar():FirstTypeToolbarHolder  {
        containerView?.visibility = View.VISIBLE
        containerView?.toolbar_layout_1?.visibility = View.VISIBLE
        containerView?.toolbar_layout_2?.visibility = View.GONE
        containerView?.toolbar_layout_3?.visibility = View.GONE
        toggleRightIcon(isVisible = false)
        setTitle("")
        setLeftText("")
        setRightText("")
        setEnabledRightText(true)
        return this
    }

    fun setTitle(title: String):FirstTypeToolbarHolder  {
        containerView?.screen_name_1?.text = title
        return this
    }

    fun setLeftText(title: String):FirstTypeToolbarHolder  {
        containerView?.previous_screen_name_1?.text = title
        return this
    }

    fun setRightText(title: String):FirstTypeToolbarHolder  {
        containerView?.text_right_1?.text = title
        return this
    }

    fun setEnabledRightText(show: Boolean):FirstTypeToolbarHolder{
        containerView?.text_right_1?.isEnabled = show
        return this
    }

    fun hideCenterText():FirstTypeToolbarHolder{
        containerView?.screen_name_1?.visibility = View.GONE
        return this
    }

    fun setOnRightTextListener(body: () -> Unit):FirstTypeToolbarHolder {
        containerView?.text_right_1?.setOnClickListener { body() }
        return this
    }
    fun setOnBackPressedListener(body: () -> Unit):FirstTypeToolbarHolder {
        containerView?.toolbar_layout_back_1?.setOnClickListener { body() }
        return this
    }

    fun setOnRightIconListener(body: () -> Unit):FirstTypeToolbarHolder {
        containerView?.button_right_1?.setOnClickListener { body() }
        return this
    }

    fun setRightIcon(@DrawableRes res: Int):FirstTypeToolbarHolder {
        containerView?.button_right_1?.setImageResource(res)
        return this
    }

    fun toggleRightIcon(isVisible: Boolean):FirstTypeToolbarHolder {
        containerView?.button_right_1?.visibility = if (isVisible) View.VISIBLE else View.GONE
        return this
    }

}