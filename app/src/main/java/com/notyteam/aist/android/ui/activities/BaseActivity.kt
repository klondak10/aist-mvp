package com.notyteam.aist.android.ui.activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.annotation.ColorRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.noty.aist.android.R

import com.notyteam.aist.android.AistApp
import com.notyteam.aist.android.Screens
import com.notyteam.aist.android.mvp.BaseViewInterface
import com.notyteam.aist.android.mvp.PresenterDetachInterface
import com.notyteam.aist.android.ui.ContextWrapper
import com.notyteam.aist.android.ui.common.BackButtonListener
import com.notyteam.aist.android.ui.common.RouterProvider
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.holders.ToolbarHolder
import com.notyteam.aist.android.utils.getLocale
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.*
import java.util.*
import kotlin.system.exitProcess

abstract class BaseActivity : AppCompatActivity(), BaseViewInterface, RouterProvider {

    lateinit var context: Context

    lateinit var requests: CompositeDisposable

    lateinit var mainRouter: Router

    lateinit var navigatorHolder: NavigatorHolder

    var toolbarHolder: ToolbarHolder? = null

    val navigator = object : SupportAppNavigator(this, R.id.main_container) {
        override fun applyCommands(commands: Array<Command>) {
            super.applyCommands(commands)
            supportFragmentManager.executePendingTransactions()
        }
    }

    private lateinit var progressDialog: Dialog
    var presenterDetachInterface: PresenterDetachInterface? = null

    abstract fun layout(): Int
    abstract fun initialization()

    override fun onCreate(savedInstanceState: Bundle?) {
        Locale.setDefault(getLocale())
        val config = Configuration()
        config.setLocale(getLocale())
        createConfigurationContext(config)

        super.onCreate(savedInstanceState)

        context = applicationContext
        requests = CompositeDisposable()
        val cicerone = Cicerone.create()
        mainRouter = cicerone.router
        navigatorHolder = cicerone.navigatorHolder

        initFragmentComponent()

        initProgressDialog()

        if (layout() != 0) {
            setContentView(layout())
            initialization()
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ContextWrapper.wrap(newBase, getLocale()))
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    fun Context.openAppSystemSettings(requestCode: Int) {
        startActivityForResult(Intent().apply {
            action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            data = Uri.fromParts("package", packageName, null)
        }, requestCode)
    }

    override fun getRouter(): Router {
        return mainRouter
    }

    fun replaceFragment(fragment: BaseFragment){
        navigator.applyCommands(arrayOf(Replace(Screens.FragmentScreen(fragment))))
    }

    fun addFragment(fragment: BaseFragment){
        navigator.applyCommands(arrayOf(Forward(Screens.FragmentScreen(fragment))))
    }

    fun newRootFragment(fragment: BaseFragment){
        navigator.applyCommands(arrayOf(BackTo(null), Replace(Screens.FragmentScreen(fragment))))
    }

    fun exit(){
        navigator.applyCommands(arrayOf(Back()))
    }

    fun initFragmentComponent() {
//        if (!::fragmentComponent.isInitialized) {
//            fragmentComponent = AistApp.appComponent.plus(FragmentModule(R.id.container, this))
//        }
//        fragmentComponent.inject(this)
    }

    private fun initProgressDialog() {
        progressDialog = Dialog(this, R.style.AppTheme)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        //progressDialog.setContentView(R.layout.dialog_progress)
        progressDialog.window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        progressDialog.setOnCancelListener {
            onBackPressed()
        }
        progressDialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun showProgressDialog(){
        if (!progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    override fun hideProgressDialog(){
        if (progressDialog.isShowing)
            progressDialog.dismiss()
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        var fragment: Fragment? = null
        val fragments = fm.fragments
        if (fragments != null) {
            for (f in fragments) {
                if (f.isVisible) {
                    fragment = f
                    break
                }
            }
        }

        if (fragment != null
                && fragment is BackButtonListener
                && (fragment as BackButtonListener).onBackPressed()) {
        } else { }
    }

    fun getFirstTypeToolbar() = toolbarHolder?.firstTypeToolbar?.showFirstTypeToolbar()

    fun getSecondTypeToolbar() = toolbarHolder?.secondTypeToolbar?.showSecondTypeToolbar()

    override fun onDestroy() {
        requests.clear()
        presenterDetachInterface?.presenterDetach()
        super.onDestroy()
    }

    fun setTransparentStatusBar(isTransparent: Boolean, isDarkTexts: Boolean){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            if (isTransparent) {
                window.statusBarColor = Color.TRANSPARENT
            }
            if (isDarkTexts && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }

//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            window.statusBarColor = if (isTransparent) getColorResource(android.R.color.transparent, applicationContext)
//            else getColorResource(R.color.colorWhite, applicationContext)
        }
        else setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, isTransparent)
    }

    private fun setWindowFlag(bits: Int, on: Boolean) {
        val winParams: WindowManager.LayoutParams = window.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        window.attributes = winParams
    }

    fun getColorResource(@ColorRes colorResource: Int, c: Context?)  = ContextCompat.getColor(c!!, colorResource)

    fun setHyperLink(textView: TextView, messageHyperTextStringId: Int, onClick:(textView: View) -> Unit){
        val i1 = textView.text.toString()
                .indexOf(getString(messageHyperTextStringId))
        val i2 = i1 + getString(messageHyperTextStringId).length
        val mySpannable = SpannableString(textView.text.toString())
        val span = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                ds.color = getColorResource(R.color.colorAccent, applicationContext)
                ds.isUnderlineText = false
            }

            override fun onClick(textView: View) {
                textView.invalidate()
                onClick(textView)
            }
        }
        mySpannable.setSpan(span, i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        textView.highlightColor = Color.TRANSPARENT
        textView.text = mySpannable
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    fun toggleKeyboard(show: Boolean){
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (!show)
            inputMethodManager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
        else
            inputMethodManager.toggleSoftInputFromWindow(window.decorView.windowToken, InputMethodManager.SHOW_FORCED, 0);
    }
}