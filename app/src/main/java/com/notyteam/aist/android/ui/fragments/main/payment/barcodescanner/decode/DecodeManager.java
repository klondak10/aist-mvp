package com.notyteam.aist.android.ui.fragments.main.payment.barcodescanner.decode;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import com.noty.aist.android.R;


public class DecodeManager {


    public void showCouldNotReadQrCodeFromScanner(Context context, final OnRefreshCameraListener listener) {
        new AlertDialog.Builder(context).setTitle(R.string.qr_code_notification)
                .setMessage(R.string.qr_code_could_not_read_qr_code_from_scanner)
                .setPositiveButton(R.string.qc_code_close, (dialog, which) -> {
                    dialog.dismiss();
                    if (listener != null) {
                        listener.refresh();
                    }
                }).show();
    }


    public interface OnRefreshCameraListener {
        void refresh();
    }

}
