package com.notyteam.aist.android.ui.fragments.main.catalog.goods


import android.os.Bundle
import android.support.v7.widget.SearchView
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CategoriesListById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.ui.adapters.catalog.GoodsAdapter
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.CatalogFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.view.CatalogFragmentView
import com.notyteam.aist.android.ui.fragments.main.catalog.presenter.GoodsListFragmentPresenter
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import com.notyteam.aist.android.utils.recyclerViewTwoColumn
import kotlinx.android.synthetic.main.fragment_dialogs_main.*
import kotlinx.android.synthetic.main.fragment_dialogs_main.search_view


class GoodsListFragment : BaseFragment(), CatalogFragmentView {
    override fun onFavoritesError(error: String) {}
    override fun onFavoritesSuccess(success: Boolean) {}
    override fun onGoodsByIdSuccess(data: GoodsResponseById) {}
    override fun onCatalogListByIdSuccess(list: CategoriesListById) {}
    override fun onCatalogListSuccess(list: CatalogListResponse) {}

    override fun onCatalogListSuccessPagePerPage(list: CatalogListResponse) {
        showProgress(false)
        val mappedList = list.data.flatMap { category ->
            arrayListOf( UICategoriesCatalog(category.is_favorite,category.in_wish_list,category.name,category.has_sub_categories, category.id, category.price, category.images) )
        } as ArrayList
        adapter.clear()
        adapter.addAll(mappedList)
        if(list.data.size==0){
            onBackPressed()
            showErrorSnack(getString(R.string.search_query))
        }
    }

    override fun onCatalogListError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }

    companion object {
        private const val POSITION_KEY = "POSITION_KEY"
        private const val NAME_CATEGORY_KEY = "NAME_CATEGORY_KEY"
        private const val NAME_KEY = "NAME_KEY"
        fun newInstance(position:Int,nameCategory:String,name:String): GoodsListFragment {
            val f = GoodsListFragment ()
            // Pass index input as an argument.
            val args = Bundle()
            args.putInt(POSITION_KEY, position)
            args.putString(NAME_CATEGORY_KEY, nameCategory)
            args.putString(NAME_KEY, name)
            f.setArguments(args)
            return f
        }

        fun getPosition(arguments: Bundle?) = arguments?.getInt(POSITION_KEY, 0)?:0
        fun getNameCategory(arguments: Bundle?) = arguments?.getString(NAME_CATEGORY_KEY)?:""
        fun getName(arguments: Bundle?) = arguments?.getString(NAME_KEY)?:""
    }


    private var presenter = GoodsListFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }
    lateinit var adapter: GoodsAdapter

    override fun layout(): Int = R.layout.fragment_dialogs_main

    override fun initialization(view: View, isFirstInit: Boolean) {

        if(getNameCategory(arguments).length<=20){
            getThirdTypeToolbar()
                    ?.setLeftText(getName(arguments))
                    ?.setTitle(getString(R.string.catalog))
                    ?.setOnBackPressedListener{
                        onBackPressed()
                    }
        }else {
             getThirdTypeToolbar()
                    ?.setLeftText("")
                    ?.setTitle(getString(R.string.catalog))
                    ?.setOnBackPressedListener{
                        onBackPressed()
                    }
        }
        activityBottomBarInterface.showBottomBar(true)

        initViews()

    }

    private fun initViews() {

        showProgress(true)
        if(getPosition(arguments)==0){
            search_view.onActionViewExpanded()
            search_view.post({ search_view.clearFocus() })
            search_view.queryHint = getNameCategory(arguments)
            search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    callSearch(query)
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean { return true }

                fun callSearch(query: String) {
                    search_view.clearFocus();
                    replaceFragment(newInstance(0,query,getString(R.string.back)))
                }

            })

            presenter.getSearchList(getNameCategory(arguments))
            search_view.visibility = View.VISIBLE
        }else {
            presenter.getCategoryListPagePerPage(getPosition(arguments))
        }

        recyclerViewTwoColumn(rv_dialogs)
        adapter = GoodsAdapter(arrayListOf(),
            onClick = {
                addFragment(GoodsByIdFragment.newInstance(it.id))
            }
        )
        rv_dialogs.adapter = adapter
    }




}
