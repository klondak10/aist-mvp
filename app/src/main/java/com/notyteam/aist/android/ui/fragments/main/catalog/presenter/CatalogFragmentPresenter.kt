package com.notyteam.aist.android.ui.fragments.main.catalog.presenter

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.CategoryListGoodsFragment

class CatalogFragmentPresenter(view: CategoryListGoodsFragment?): BasePresenter<CategoryListGoodsFragment>(view) {


    fun getCategoryList(){
        requests.add(aistApi.getCategoriesList({
            view?.onCatalogListSuccess(it)
        }, { errorMessage, errorCode ->
            view?.onCatalogListError(errorMessage)
        }).sendRequest())
    }

}