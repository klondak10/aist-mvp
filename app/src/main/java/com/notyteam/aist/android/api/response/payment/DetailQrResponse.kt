package com.notyteam.aist.android.api.response.payment

import com.google.gson.JsonObject
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.notyteam.aist.android.api.response.catalog.CategoriesItem
import com.notyteam.aist.android.api.response.responses.BaseResponse

@Generated("com.robohorse.robopojogenerator")
open class DetailQrResponse(

		@field:SerializedName("price")
		val price: Int = 0,

		@field:SerializedName("summary")
		val summary: String = "",

		@field:SerializedName("bindingPaymentFormUrl")
		val bindingPaymentFormUrl: String = "",

		@field:SerializedName("state")
		val state: String = ""

): BaseResponse()