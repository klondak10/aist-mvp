package com.notyteam.aist.android.utils

import android.support.v4.app.Fragment
import android.util.Log
import java.lang.Exception
import java.util.*

class BottomBarFragmentManager(fragmentsTabsArray: ArrayList<Fragment>, var currentTab: Int,
                               private val fragmentManager: FragmentManager) {

    var stacks = hashMapOf<Int, Stack<Fragment>>()
    private var currentFragment: Fragment? = null
    private var previousTabIndex = 0

    init {
        fragmentsTabsArray.forEachIndexed { index, _ -> stacks[index] = Stack() }
    }

    /*
     * Add first time tab in BottomNavigationView
     */
    fun addFirstTabFragment(show: Fragment) {
        stacks[currentTab]?.push(show)
        fragmentManager.replaceFragment(show)
        currentFragment = show
        previousTabIndex = currentTab
    }

    /*
     * Add additional tab in BottomNavigationView on click, apart from the initial one
     */
    fun addAdditionalTabFragment(show: Fragment,
                                 layoutId: Int,
                                 shouldAddToStack: Boolean) {
        try {
            if (shouldAddToStack) stacks[currentTab]?.push(show)
            if (previousTabIndex == currentTab)
                fragmentManager.setFragmentSlideInOutTransition(show)
            else
                fragmentManager.setFragmentFadeTransition(show)
            currentFragment?.let {
                fragmentManager.supportFragmentManager
                        .beginTransaction()
                        .add(layoutId, show)
                        .attach(show)
                        .detach(it)
                        .commit()
            }
            currentFragment = show
            previousTabIndex = currentTab
        }
        catch (e: Exception){
            Log.d("BottomBarManager", e.message)
        }
    }

    /*
     * Hide previous and show current tab fragment if it has already been added
     * In most cases, when tab is clicked again, not for the first time
     */
    fun showHideTabFragment(show: Fragment) {
        try {
            currentFragment?.let { current ->
                fragmentManager.setFragmentFadeTransition(current)
                fragmentManager.setFragmentFadeTransition(show)
                fragmentManager.supportFragmentManager
                        .beginTransaction()
                        .detach(current)
                        .commitNow()
                fragmentManager.supportFragmentManager
                        .beginTransaction()
                        .attach(show)
                        .commitNow()
                currentFragment = show
                previousTabIndex = currentTab
            }
        }
        catch (e: Exception){
            Log.d("BottomBarManager", e.message)
        }
    }

    fun popFragment() {
        currentFragment?.let {current ->
            stacks[currentTab]?.let {
                val newFragment = it.elementAt(it.size - 2)
                it.pop()
                removeFragment(newFragment, current)
                currentFragment = newFragment
            }
        }
    }


    private fun removeFragment(show: Fragment, remove: Fragment) {
        fragmentManager.supportFragmentManager
                .beginTransaction()
                .remove(remove)
                .attach(show)
                .commit()
    }

    fun getCurrentFragmentFromShownStack(): Fragment? {
        return  stacks[currentTab]?.let {
            it.elementAt(it.size - 1)
        }
    }
}