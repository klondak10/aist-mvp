package com.notyteam.aist.android.api.response.catalog

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class CategoriesListById(

		@field:SerializedName("sub_categories")
		val sub_categories:  ArrayList<CategoriesItem> = arrayListOf()

)