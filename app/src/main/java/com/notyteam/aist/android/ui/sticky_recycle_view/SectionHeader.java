package com.notyteam.aist.android.ui.sticky_recycle_view;

public class SectionHeader implements Section {

    private int section;
    private String country;
    private String code;
    private String letter;
    public SectionHeader(int section) {
        this.section = section;
    }

    public SectionHeader(int section, String letter) {
        this.section = section;
        this.letter = letter;
    }

    public SectionHeader(int section, String country, String code) {
        this.section = section;
        this.country = country;
        this.code = code;
    }

    @Override
    public int type() {
        return HEADER;
    }

    @Override
    public int sectionPosition() {
        return section;
    }

    @Override
    public String country() {
        return country;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String letter() {
        return letter;
    }

    @Override
    public String countryCode() {
        return null;
    }
}
