package com.notyteam.aist.android.ui

interface ActivityBottomBarInterface {
    fun showBottomBar(isShowed: Boolean)
    fun changeBottomBarScreen(index: Int)

    companion object {
        val empty: ActivityBottomBarInterface = object: ActivityBottomBarInterface {
            override fun showBottomBar(isShowed: Boolean) {}

            override fun changeBottomBarScreen(index: Int) {}
        }
    }
}