package com.notyteam.aist.android.ui.fragments.main.setting.profile

import android.graphics.Bitmap
import android.net.Uri
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.mvp.PresenterDetachInterface

interface ProfileFragmentView: PresenterDetachInterface {
    fun onSettingInterestsListSuccess()
    fun onSettingInterestsListError(errorMessage: String)
    fun onGettingInterestsListSuccess(response: InterestsListResponse)
    fun onGettingInterestsListError(errorMessage: String)

    fun onAvatarLoaded(avatar: Bitmap?)
    fun onAvatarLoadError(errorMessage: String)

    fun onProfileUpdateSuccess()
    fun onProfileUpdateError(errorMessage: String)
}