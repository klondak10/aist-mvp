package com.notyteam.aist.android.utils

import android.content.res.Resources

class DisplayMetricsHelper {
    companion object {
        fun dpToPx(dp: Int): Int {
            return (dp * Resources.getSystem().getDisplayMetrics().density).toInt()
        }

        fun pxToDp(px: Int): Int {
            return (px / Resources.getSystem().getDisplayMetrics().density).toInt()
        }
    }
}