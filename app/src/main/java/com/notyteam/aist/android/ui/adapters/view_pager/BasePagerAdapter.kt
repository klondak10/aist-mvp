package com.notyteam.aist.android.ui.adapters.view_pager

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.utils.CustomViewPager
import com.notyteam.aist.android.utils.getStringArray

abstract class BasePagerAdapter (fm: FragmentManager,
                        val pager: CustomViewPager,
                        val fragments: ArrayList<BaseFragment>) : FragmentPagerAdapter(fm) {
    val context: Context = pager.context

    private fun getVisibleCallback(fragment: Fragment):(fragment: Fragment) -> Boolean{
        return {
            val fragmentIndex = fragments.indexOf(fragment)
            fragmentIndex == pager.currentItem
        }
    }

    init {
        fragments.forEach {
            it.isVisible = getVisibleCallback(it)
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as BaseFragment
        fragments[position] = fragment
        return fragment
    }

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = fragments.size

    override fun getPageTitle(position: Int) = getTitle(position)

    abstract fun getTitle(pageIndex: Int): String
}