package com.notyteam.aist.android.ui.adapters

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.models.ui.UIImage
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_image.*

class GridImagesAdapter(list: MutableList<UIImage>,
                        val onClick: (data: UIImage) -> Unit = {}) :
        BaseAdapter<UIImage, GridImagesAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int): ViewHolder = ViewHolder(view)

    override fun getItemViewType(position: Int): Int = R.layout.item_image

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {
        init {

        }

        //override val containerView: View? get() = itemView

        override fun bind(pos: Int) {
            val iv_image = itemView.findViewById<ImageView>(R.id.iv_image)
            if (list[pos].link.isNotEmpty())
                containerView?.context?.let {
                    Glide.with(it)
                            .load(list[pos].link)
                            .into(iv_image)
                }
        }
    }
}