package com.notyteam.aist.android.ui.models.ui

data class UIImage(
        var link: String = "",
        var res: Int = 0
) : Comparable<UIImage> {
    override fun compareTo(other: UIImage): Int {
        return link.compareTo(other.link)
    }
}