package com.notyteam.aist.android.ui.models.ui.catalog

import com.notyteam.aist.android.api.response.catalog.CategoriesItem

data class UICategoriesCatalog(
        var is_favorite: Boolean? = null,
        var in_wish_list: Boolean? = null,
        var name: String = "",
        var has_sub_categories: Boolean? = null,
        var id: Int = 0,
        var price: Int = 0,
        val images: ArrayList<String> = arrayListOf(),
        val data: ArrayList<CategoriesItem> = arrayListOf())
    : Comparable<UICategoriesCatalog> {
    override fun compareTo(other: UICategoriesCatalog): Int {
        return name.compareTo(other.name)
    }
}