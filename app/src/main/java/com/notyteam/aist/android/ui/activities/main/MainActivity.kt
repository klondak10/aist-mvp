package com.notyteam.aist.android.ui.activities.main

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.jakewharton.rxbinding.view.RxView
import com.noty.aist.android.R

import com.notyteam.aist.android.Screens
import com.notyteam.aist.android.ui.ActivityBottomBarInterface
import com.notyteam.aist.android.ui.activities.BaseActivity
import com.notyteam.aist.android.ui.common.BackButtonListener
import com.notyteam.aist.android.ui.common.RouterProvider
import com.notyteam.aist.android.ui.holders.ToolbarHolder
import com.notyteam.aist.android.utils.ActionDebounceFilter
import im.aist.sdk.util.ActorSDKMessenger.messenger
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.Router
import java.util.*
import kotlin.system.exitProcess


class MainActivity : BaseActivity(), MainActivityView, RouterProvider, ActivityBottomBarInterface {

    companion object {
        fun start(c: Context) {
            c.startActivity(Intent(c, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
        }
    }

    override fun getRouter(): Router {
        return mainRouter
    }

    private var presenter:MainActivityPresenter? = null

    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    override fun layout(): Int = R.layout.activity_main

    private val clicksFilter = ActionDebounceFilter()
    private var isFastClick: Boolean = false

    override fun initialization() {
        toolbarHolder = ToolbarHolder(toolbar_layout)
        getSecondTypeToolbar()
                ?.setTitle("Screen title_tut")
                ?.setOnLeftIconListener {  }

        presenter = MainActivityPresenter(this)

        setTransparentStatusBar(isTransparent = false, isDarkTexts = true)
        initBottomBar()

        changeBottomBarScreen(2)
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == PROFILE_UPDATE && resultCode == Activity.RESULT_OK) {
//
//        }
//    }

    private fun initBottomBar() {
        bot_bar.addItems(createBotItems())
        bot_bar.titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
        bot_bar.accentColor = getColorResource(R.color.colorAccent, this)
        bot_bar.inactiveColor = getColorResource(R.color.overlay_dark_30, this)
        bot_bar.setOnTabSelectedListener { position, wasSelected ->
            bot_bar.changeTab(position, wasSelected)
        }
    }


    protected fun AHBottomNavigation.changeTab(pos: Int, wasSelected: Boolean): Boolean {
        isFastClick = false

        RxView.focusChanges(this)
                .filter {
                    clicksFilter.filterAction()
                }
                .subscribe {
                    if (!wasSelected) {
                        when (pos) {
                            0 -> selectTab(Screens.MainScreens.DESIRES_SCREEN)
                            1 -> selectTab(Screens.MainScreens.NOTIFICATIONS_SCREEN)
                            2 -> selectTab(Screens.MainScreens.DIALOGUES_SCREEN)
                            3 -> selectTab(Screens.MainScreens.PAYMENT_SCREEN)
                            4 -> selectTab(Screens.MainScreens.CATALOG_SCREEN)
                        }
                        isFastClick = true
                    }
                }

        return isFastClick
    }

    private fun createBotItems(): List<AHBottomNavigationItem> {
        val items = ArrayList<AHBottomNavigationItem>()
        items.add(AHBottomNavigationItem(getString(R.string.bottom_bar_wishes), R.drawable.bottom_wish))
        items.add(AHBottomNavigationItem(getString(R.string.bottom_bar_notifications), R.drawable.bottom_notification))
        items.add(AHBottomNavigationItem(getString(R.string.bottom_bar_dialogs), R.drawable.bottom_dialogs))
        items.add(AHBottomNavigationItem(getString(R.string.bottom_bar_payment), R.drawable.ic_payment_black_24dp))
        items.add(AHBottomNavigationItem(getString(R.string.bottom_bar_catalog), R.drawable.bottom_catalog))
        return items
    }

    //NAVIGATION
    private fun selectTab(screen: Screens.MainScreens) {
        val tab: String = screen.name

        val fm = supportFragmentManager
        var currentFragment: Fragment? = null
        val fragments = fm.fragments
        if (fragments != null) {
            for (f in fragments) {
                if (f.isVisible) {
                    currentFragment = f
                    break
                }
            }
        }
        val newFragment = fm.findFragmentByTag(tab)

        if (currentFragment != null && newFragment != null && currentFragment === newFragment) return

        val transaction = fm.beginTransaction()

        if (newFragment == null) {
            transaction.add(R.id.main_container, Screens.TabScreen(screen).fragment, tab)
        }
        if (newFragment != null) {
            transaction.attach(newFragment)
        }
        if (currentFragment != null) {
            transaction.detach(currentFragment)
        }

        transaction.commitNow()

    }

    override fun showBottomBar(isShowed: Boolean) {
        if(isShowed) bot_bar.visibility = View.VISIBLE
        else bot_bar.visibility = View.GONE
    }

    override fun changeBottomBarScreen(index: Int) {
        bot_bar.currentItem = index
        bot_bar.changeTab(index, false)
    }


    override fun onDestroy() {
        super.onDestroy()
        moveTaskToBack(true);
        exitProcess(-1)
    }

}
