package com.notyteam.aist.android.ui.fragments.registration.phone

import com.notyteam.aist.android.mvp.BasePresenter
import im.aist.core.entity.AuthStartRes
import im.aist.runtime.promise.Promise
import im.aist.sdk.aist.ActorSDK
import im.aist.sdk.util.ActorSDKMessenger

class PhoneFragmentPresenter(view: PhoneFragmentView?): BasePresenter<PhoneFragmentView>(view) {

    fun sendPhone(phone: Long){
        startAuth(ActorSDK.sharedActor().messenger.doStartPhoneAuth(phone))
    }

    private fun startAuth(res: Promise<AuthStartRes>) {
        res.then { authStartRes ->
            view?.onPhoneResult(authStartRes.transactionHash)
        }.failure {
            view?.onPhoneError(it)
        }
        ActorSDKMessenger.messenger()
    }
}