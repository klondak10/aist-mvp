package com.notyteam.aist.android.ui.test;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.noty.aist.android.R;

import im.aist.core.entity.Avatar;
import im.aist.core.viewmodel.Command;
import im.aist.core.viewmodel.CommandCallback;
import im.aist.core.viewmodel.GroupVM;
import im.aist.core.viewmodel.UserVM;
import im.aist.runtime.function.BiFunction;
import im.aist.runtime.function.Consumer;
import im.aist.runtime.function.Function;
import im.aist.runtime.mvvm.Value;
import im.aist.runtime.mvvm.ValueChangedListener;
import im.aist.runtime.mvvm.ValueDoubleChangedListener;
import im.aist.runtime.promise.Promise;
import im.aist.sdk.aist.ActorSDK;
import im.aist.sdk.aist.ActorStyle;
import im.aist.sdk.controllers.ActorBinder;
import im.aist.sdk.view.avatar.AvatarView;

import static im.aist.sdk.util.ActorSDKMessenger.messenger;

public class BaseActivity extends AppCompatActivity {

    public static final ActorStyle STYLE = ActorSDK.sharedActor().style;

    private final ActorBinder BINDER = new ActorBinder();

    private boolean isResumed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActorSDK.sharedActor().waitForReady();
        super.onCreate(savedInstanceState);

        notifyOnResume();

        if (getSupportActionBar() != null && STYLE.getToolBarColor() != 0) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(STYLE.getToolBarColor()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        onPerformBind();
        notifyOnResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BINDER.unbindAll();
        notifyOnPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        notifyOnPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        notifyOnPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        notifyOnPause();
    }


    // Binding

    protected void onPerformBind() {

    }

    public void bind(final TextView textView, Value<String> value) {
        BINDER.bind(textView, value);
    }

    public <T> void bind(final TextView textView, final Value<T> value, final Function<T, CharSequence> bind) {
        BINDER.bind(value, new ValueChangedListener<T>(){
            @Override
            public void onChanged(T val, Value<T> valueModel) {
                textView.setText(bind.apply(val));
            }
        });
    }

    public <T1, T2> void bind(final TextView textView, final Value<T1> value1, final Value<T2> value2, final BiFunction<T1, T2, CharSequence> bind) {
        BINDER.bind(value1, value2, new ValueDoubleChangedListener<T1, T2>(){
            @Override
            public void onChanged(T1 val, Value<T1> valueModel, T2 val2, Value<T2> valueModel2) {
                textView.setText(bind.apply(val, val2));
            }
        });
    }

    public void bind(final AvatarView avatarView, final int id,
                     final Value<Avatar> avatar, final Value<String> name) {
        BINDER.bind(avatarView, id, avatar, name);
    }

    public void bind(final TextView textView, final UserVM user) {
        BINDER.bind(textView, user);
    }

    public void bind(final TextView textView, View titleContainer, GroupVM value) {
        BINDER.bind(textView, titleContainer, value);
    }

    public void bindGlobalCounter(ValueChangedListener<Integer> callback) {
        BINDER.bindGlobalCounter(callback);
    }

    public void bindGroupTyping(final TextView textView, final View container, final View titleContainer, final Value<int[]> typing) {
        BINDER.bindGroupTyping(textView, container, titleContainer, typing);
    }

    public void bindPrivateTyping(final TextView textView, final View container, final View titleContainer, final Value<Boolean> typing) {
        BINDER.bindPrivateTyping(textView, container, titleContainer, typing);
    }

    public <T> void bind(Value<T> value, ValueChangedListener<T> listener) {
        BINDER.bind(value, listener);
    }

    public <T> void bind(Value<T> value, ValueChangedListener<T> listener, boolean notify) {
        BINDER.bind(value, listener, notify);
    }

    public <T, V> void bind(final Value<T> value1, final Value<V> value2,
                            final ValueDoubleChangedListener<T, V> listener) {
        BINDER.bind(value1, value2, listener);
    }

    // Toolbar

    protected void setToolbar(int text, boolean enableBack) {
        if (getSupportActionBar() == null) {
            throw new RuntimeException("Action bar is not set!");
        }
        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (enableBack) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setTitle(text);
    }

    protected void setToolbar(int text) {
        setToolbar(text, true);
    }

    protected void setToolbar(View view, ActionBar.LayoutParams params, boolean enableBack) {
        if (getSupportActionBar() == null) {
            throw new RuntimeException("Action bar is not set!");
        }
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(false);
        getSupportActionBar().setCustomView(view, params);
        if (enableBack) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } else {
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            Toolbar parent = (Toolbar) view.getParent();
            parent.setContentInsetsAbsolute(0, 0);
        }
    }

    protected void setToolbar(View view, ActionBar.LayoutParams params) {
        setToolbar(view, params, true);
    }


    // Executions

    public <T> void execute(Command<T> cmd, int title, final CommandCallback<T> callback) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(title));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        cmd.start(new CommandCallback<T>() {
            @Override
            public void onResult(T res) {
                dismissDialog(progressDialog);
                callback.onResult(res);
            }

            @Override
            public void onError(Exception e) {
                dismissDialog(progressDialog);
                callback.onError(e);
            }
        });
    }

    public <T> void execute(Command<T> cmd) {
        execute(cmd, R.string.progress_common);
    }

    public <T> void execute(Command<T> cmd, final CommandCallback<T> callback) {
        cmd.start(callback);
    }

    public <T> void execute(Command<T> cmd, int title) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(title));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        cmd.start(new CommandCallback<T>() {
            @Override
            public void onResult(T res) {
                dismissDialog(progressDialog);
            }

            @Override
            public void onError(Exception e) {
                dismissDialog(progressDialog);
            }
        });
    }

    public <T> void execute(Promise<T> promise) {
        execute(promise, R.string.progress_common);
    }

    public <T> void execute(Promise<T> promise, int title) {
        final ProgressDialog dialog = ProgressDialog.show(this, "", getString(title), true, false);
        promise.then(new Consumer<T>(){
            @Override
            public void apply(T t) {
                dismissDialog(dialog);
            }
        }).failure(new Consumer<Exception>(){
            @Override
            public void apply(Exception e) {
                dismissDialog(dialog);
            }
        });
    }


    // Tools

    public void dismissDialog(ProgressDialog progressDialog) {
        try {
            progressDialog.dismiss();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void notifyOnResume() {
        if (isResumed) {
            return;
        }
        isResumed = true;

        messenger().onActivityOpen();
    }

    private void notifyOnPause() {
        if (!isResumed) {
            return;
        }
        isResumed = false;
        messenger().onActivityClosed();
    }

    protected boolean getIsResumed() {
        return isResumed;
    }
}
