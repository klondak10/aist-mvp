package com.notyteam.aist.android.api.response.payment

import com.google.gson.JsonObject
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.notyteam.aist.android.api.response.catalog.CategoriesItem
import com.notyteam.aist.android.api.response.responses.BaseResponse

@Generated("com.robohorse.robopojogenerator")
open class FindQrResponse(

		@field:SerializedName("data")
		val data: ArrayList<FindQrResponse> = arrayListOf(),

		@field:SerializedName("attributes")
		val attributes: DetailQrResponse

): BaseResponse()