package com.notyteam.aist.android.ui.fragments.main.catalog

import android.os.Handler
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.SearchView
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.CategoryListGoodsByIdFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.CategoryListGoodsFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.ForMeGoodsFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.GoodsListFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.presenter.CatalogsFragmentPresenter
import com.notyteam.aist.android.ui.fragments.main.catalog.view.CatalogsFragmentView
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import kotlinx.android.synthetic.main.fragment_catalogs.*
import kotlin.collections.ArrayList


class CatalogFragment : BaseFragment(), CatalogsFragmentView {

    companion object {
        fun newInstance(): CatalogFragment {
            return CatalogFragment()
        }
    }

    private var presenter = CatalogsFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }


    override fun layout(): Int = R.layout.fragment_catalogs

    override fun initialization(view: View, isFirstInit: Boolean) {

        getSecondTypeToolbar()
                ?.setTitle(getString(R.string.bottom_bar_catalog))
                ?.toggleLeftIcon(isVisible = false)
                ?.toggleRightIcon(isVisible = false)

        if (isFirstInit) {
            initViews()
        }
        activityBottomBarInterface.showBottomBar(true)

    }

    private fun initViews() {

        setupViewPager(viewpager)
        tabs.setupWithViewPager(viewpager)

        search_view.onActionViewExpanded()
        search_view.post({ search_view.clearFocus() })
        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                callSearch(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean { return true }

             fun callSearch(query: String) {
                 //Do searching
                 search_view.setQuery("", false);
                 search_view.clearFocus();
                 addFragment(GoodsListFragment.newInstance(0,query,getString(R.string.back)))
            }

        })
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(getChildFragmentManager())
        adapter.addFragment(CategoryListGoodsFragment(),getString(R.string.all_goods))
        adapter.addFragment(ForMeGoodsFragment(),getString(R.string.for_me))
        viewPager.offscreenPageLimit = 2
        viewPager.adapter = adapter
    }

    fun openCategory(item: UICategoriesCatalog){
        this@CatalogFragment.addFragment(CategoryListGoodsByIdFragment.newInstance(item.id, item.name, getString(R.string.catalog)))
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager) {

        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }


    }



}
