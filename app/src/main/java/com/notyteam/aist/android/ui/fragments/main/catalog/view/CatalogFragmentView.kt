package com.notyteam.aist.android.ui.fragments.main.catalog.view

import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CatalogListResponseById
import com.notyteam.aist.android.api.response.catalog.CategoriesListById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.mvp.PresenterDetachInterface

interface CatalogFragmentView: PresenterDetachInterface {

    fun onFavoritesError(error: String)
    fun onFavoritesSuccess(success: Boolean)

    fun onGoodsByIdSuccess(data: GoodsResponseById)

    fun onCatalogListByIdSuccess(list: CategoriesListById)
    fun onCatalogListSuccess(list: CatalogListResponse)
    fun onCatalogListSuccessPagePerPage(list: CatalogListResponse)
    fun onCatalogListError(errorMessage: String)

}