package com.notyteam.aist.android

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.support.multidex.MultiDexApplication
import com.google.firebase.analytics.FirebaseAnalytics
import com.notyteam.aist.android.api.retrofit.api.AistApi
import com.notyteam.aist.android.api.retrofit.api.QrApi
import com.notyteam.aist.android.api.retrofit.RetrofitModule
import com.notyteam.aist.android.api.retrofit.RetrofitModulePayment
import com.notyteam.aist.android.subnavigation.LocalCiceroneHolder
import io.paperdb.Paper
import java.lang.ref.WeakReference


class AistApp: MultiDexApplication(), LifecycleObserver {


    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    companion object {
        var isResumed = false
        lateinit var localCiceroneHolder: LocalCiceroneHolder
        lateinit var aistApi: AistApi
        lateinit var qrApi: QrApi
        @JvmField
        var context: Context? = null
        // Not really needed since we can access the variable directly.
        @JvmStatic fun getMyApplicationContext(): Context? {
            return context
        }


    }
    override fun onCreate() {
        super.onCreate()
        val retrofitModule = RetrofitModule(this)
        val retrofitModulePayment = RetrofitModulePayment(this)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        aistApi = AistApi(WeakReference(this), retrofitModule.retrofit)
        qrApi = QrApi(WeakReference(this), retrofitModulePayment.retrofit)
        localCiceroneHolder = LocalCiceroneHolder()
        Paper.init(this)
        context = applicationContext

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        isResumed = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onAppResumed() {
        isResumed = true
    }

}