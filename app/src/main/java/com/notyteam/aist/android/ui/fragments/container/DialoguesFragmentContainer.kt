package com.notyteam.aist.android.ui.fragments.container

import android.os.Bundle
import com.noty.aist.android.R
import com.notyteam.aist.android.Screens
import com.notyteam.aist.android.ui.fragments.main.dialogs.DialogsFragment


class DialoguesFragmentContainer : ContainerFragment() {

    companion object {
        fun getNewInstance(name: String): DialoguesFragmentContainer {
            val fragment = DialoguesFragmentContainer()
            val arguments = Bundle()
            arguments.putString(EXTRA_NAME, name)
            fragment.arguments = arguments
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.ftc_container) == null) {
            getCicerone().router.replaceScreen(Screens.FragmentScreen(DialogsFragment()))
        }
    }
}