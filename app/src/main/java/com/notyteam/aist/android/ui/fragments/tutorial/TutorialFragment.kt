package com.notyteam.aist.android.ui.fragments.tutorial

import android.support.v4.view.ViewPager
import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.adapters.view_pager.TutorialPagerAdapter
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.registration.phone.PhoneFragment
import kotlinx.android.synthetic.main.fragment_tutorial.*

class TutorialFragment : BaseFragment() {
    companion object {
        fun newInstance(): TutorialFragment {
            return TutorialFragment()
        }
    }

    override fun layout(): Int = R.layout.fragment_tutorial

    override fun initialization(view: View, isFirstInit: Boolean) {
        baseActivity.setTransparentStatusBar(isTransparent = false, isDarkTexts = true)
        hideToolbar()
        if (isFirstInit) {
            initViews()
        }
    }

    private fun initViews() {
        view_pager.adapter = TutorialPagerAdapter(childFragmentManager, view_pager)
        view_pager.setPagingEnabled(false)
        view_pager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                setNextButtonText(position)
            }
        })
        setNextButtonText(view_pager.currentItem)

        button_next.setOnClickListener {
            val page = view_pager.currentItem
            if (page < 2) view_pager.currentItem = page + 1
            else {
                openNextScreen()
            }
        }

        button_skip_tutorial.setOnClickListener {
            openNextScreen()
        }
    }

    private fun setNextButtonText(position: Int){
        button_next.text = if (position == 0) "Начать"
        else "Далее"
    }

    private fun openNextScreen(){
        replaceFragment(PhoneFragment())
    }

}
