package com.notyteam.aist.android.ui.models.ui
data class UICategories(
        var name: String = "",
        var id: Int = 0,
        var isChecked:Boolean = false
) : Comparable<UICategories> {
    override fun compareTo(other: UICategories): Int {
        return name.compareTo(other.name)
    }
}