package com.notyteam.aist.android.ui.models.ui.header;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class ContentItem  extends ListItem {

    public String name;
    public ArrayList<String> images = new ArrayList<>();
    public Boolean is_favorite;
    public int price,id;


    public ContentItem(String name, ArrayList<String> images ,Boolean is_favorite,int price,int id) {
        this.name = name;
        this.images = images;
        this.is_favorite = is_favorite;
        this.price = price;
        this.id = id;

    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public void setIs_favorite(Boolean is_favorite) {
        this.is_favorite = is_favorite;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public Boolean getIs_favorite() {
        return is_favorite;
    }

    public int getPrice() {
        return price;
    }
}