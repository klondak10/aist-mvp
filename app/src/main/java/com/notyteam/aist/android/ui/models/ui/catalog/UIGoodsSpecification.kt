package com.notyteam.aist.android.ui.models.ui.catalog



data class UIGoodsSpecification(
        var type: String = "",
        var value: String? = "")
    : Comparable<UIGoodsSpecification> {
    override fun compareTo(other: UIGoodsSpecification): Int {
        return type.compareTo(other.type)
    }
}