package com.notyteam.aist.android.utils

object Constants {
    val phoneMask = "+# ### ### ## ##"

    enum class DeferredOrderItemType {
        MAIN_ITEM,
        BOTTOM_ITEM;
    }

    enum class FavoriteOrderItemType {
        MAIN_ITEM,
        BOTTOM_ITEM;
    }

    object ActivityRequestCodes {
        const val PROFILE_UPDATE = 25
    }

    object SalonPractitionerScreen {
        const val LOGIN_SUCCESS = "LOGIN_SUCCESS"
    }
}