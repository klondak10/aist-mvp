package com.notyteam.aist.android.mvp

interface BaseViewInterface {
    fun showProgressDialog()

    fun hideProgressDialog()
}