package com.notyteam.aist.android.ui.adapters.catalog

import android.text.Html
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.noty.aist.android.R
import com.notyteam.aist.android.ui.adapters.BaseAdapter
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import com.notyteam.aist.android.ui.models.ui.catalog.UIGoodsSpecification
import kotlinx.android.extensions.LayoutContainer

class SpecificationGoodsAdapter(list: MutableList<UIGoodsSpecification>) :
        BaseAdapter<UIGoodsSpecification, SpecificationGoodsAdapter.ViewHolder>(list) {

    override fun createViewHolder(view: View, layoutId: Int): ViewHolder = ViewHolder(view)

    override fun getItemViewType(position: Int): Int = R.layout.item_specification

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {

        init { }

        override val containerView: View? get() = itemView

        override fun bind(pos: Int) {

            val txt_type_specification = itemView.findViewById<TextView>(R.id.txt_type_specification)
            txt_type_specification.text = list[pos].type

            val txt_value_specification = itemView.findViewById<TextView>(R.id.txt_value_specification)
            txt_value_specification.text = list[pos].value

        }
    }
}