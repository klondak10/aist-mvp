package com.notyteam.aist.android.ui.holders

import android.view.View

class ToolbarHolder(val containerView: View?) {

    var firstTypeToolbar: FirstTypeToolbarHolder? = null
    var secondTypeToolbar: SecondTypeToolbarHolder? = null
    var thirdTypeToolbar: ThirdTypeToolbarHolder? = null

    init {
        firstTypeToolbar = FirstTypeToolbarHolder(containerView)
        secondTypeToolbar = SecondTypeToolbarHolder(containerView)
        thirdTypeToolbar = ThirdTypeToolbarHolder(containerView)
    }

    fun hideToolbar(){
        containerView?.visibility = View.GONE
    }
}