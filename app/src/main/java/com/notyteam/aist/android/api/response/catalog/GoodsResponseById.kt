package com.notyteam.aist.android.api.response.catalog

import com.google.gson.JsonObject
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import com.notyteam.aist.android.api.response.interest.all_interests.DataItem
import com.notyteam.aist.android.api.response.responses.BaseResponse

@Generated("com.robohorse.robopojogenerator")
open class GoodsResponseById(

		@field:SerializedName("is_favorite")
		val is_favorite: Boolean? = null,

		@field:SerializedName("in_wish_list")
		val in_wish_list: Boolean? = null,

		@field:SerializedName("data")
		val data: GoodsResponseById,

		@field:SerializedName("price")
		val price: Int = 0,

		@field:SerializedName("inner_id")
		val inner_id: Int = 0,

		@field:SerializedName("id")
		val id: Int = 0,

		@field:SerializedName("name")
		val name: String = "",

		@field:SerializedName("add_to_cart_url")
		val add_to_cart_url: String = "",


		@field:SerializedName("url")
		val url: String = "",

		@field:SerializedName("description")
		val description: String = "",

		@field:SerializedName("images")
		val images: ArrayList<String> = arrayListOf(),

		@field:SerializedName("details")
		val details: JsonObject

): BaseResponse()