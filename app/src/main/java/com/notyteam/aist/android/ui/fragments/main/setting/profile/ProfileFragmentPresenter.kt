package com.notyteam.aist.android.ui.fragments.main.setting.profile

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter
import com.notyteam.aist.android.utils.ActorUser
import im.aist.core.viewmodel.CommandCallback
import im.aist.core.viewmodel.FileVMCallback
import im.aist.runtime.files.FileSystemReference
import im.aist.core.viewmodel.UserVM
import im.aist.sdk.util.ActorSDKMessenger.messenger
import im.aist.sdk.util.images.common.ImageLoadException
import im.aist.sdk.util.images.ops.ImageLoading
import io.paperdb.Paper
import java.lang.Exception


class ProfileFragmentPresenter(view: ProfileFragmentView?) : BasePresenter<ProfileFragmentView>(view) {

    val user: UserVM? by lazy {
        ActorUser.getActorUser()
    }
    fun getInterestsList() {
        requests.add(aistApi.getUserInterestsList({
            view?.onGettingInterestsListSuccess(it)
        }, { errorMessage, errorCode ->
            view?.onGettingInterestsListError(errorMessage)
        }).sendRequest())
    }
    fun getLocalAvatar() {
        user?.avatar?.get()?.fullImage?.fileReference?.fileId?.let { fileId ->
            val file = messenger().findDownloadedDescriptor(fileId)
            if (file != null) {
                try {
                    val bitmap = ImageLoading.loadBitmapOptimized(file)
                    view?.onAvatarLoaded(bitmap)
                    return
                } catch (e: ImageLoadException) {
                    view?.onAvatarLoadError("Avatar loading error")
                    e.printStackTrace()
                }
            } else getAvatar()
        }
    }

    fun getAvatar() {
        messenger().bindFile(user?.avatar?.get()?.fullImage?.fileReference, true, object : FileVMCallback {
            override fun onNotDownloaded() {
                view?.onAvatarLoadError("Avatar loading error")
            }

            override fun onDownloading(progressV: Float) {}
            override fun onDownloaded(reference: FileSystemReference) {
                try {
                    val bitmap = ImageLoading.loadBitmapOptimized(reference.descriptor)
                    view?.onAvatarLoaded(bitmap)
                } catch (e: ImageLoadException) {
                    e.printStackTrace()
                    view?.onAvatarLoadError("Avatar loading error")
                }

            }
        })
    }

    class User(val name: String = "",
               val email: String = "",
               val birthDate: String = "",
               val interestsList: ArrayList<Int> = arrayListOf())

    fun setUserData(user: User) {
        messenger().editMyName(user.name)?.start(
                object : CommandCallback<Boolean> {
                    override fun onResult(res: Boolean) {
                        setInterestsList(user)
                    }
                    override fun onError(e: Exception) {
                        view?.onProfileUpdateError(e.message ?: "")
                    }
                })

        messenger().editMyNick(user.email.replace("@","__").replace(".","_"))?.start(
                object : CommandCallback<Boolean> {
                    override fun onResult(res: Boolean) {}
                    override fun onError(e: Exception) { //view?.onProfileUpdateError(e.message ?: "")
                         }
                })

        Paper.book().write("birthday",user.birthDate);
    }

    fun setInterestsList(user: User) {
        requests.add(aistApi.setUserInterestsList(user.interestsList, {

            view?.onSettingInterestsListSuccess()
            view?.onProfileUpdateSuccess()

        }, { errorMessage, errorCode ->
            view?.onSettingInterestsListError(errorMessage)
        }).sendRequest())
    }
}

