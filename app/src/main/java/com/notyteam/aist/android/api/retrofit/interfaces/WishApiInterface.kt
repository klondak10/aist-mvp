package com.notyteam.aist.android.api.retrofit.interfaces

import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CatalogListResponseById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.api.response.responses.BaseResponse
import io.reactivex.Single
import retrofit2.http.*

interface WishApiInterface {

    @GET("users/{user_id}/wishes")
    fun getWishList(@Path("user_id") user_id: Int): Single<CatalogListResponse>

    @FormUrlEncoded
    @POST("users/{user_id}/wishes/products")
    fun addWish(@Path("user_id") user_id: Int,@Field("product_id") product_id: Int): Single<BaseResponse>

    @DELETE("users/{user_id}/wishes/{wish_id}")
    fun deleteWish(@Path("user_id") user_id: Int,@Path("wish_id") wish_id: Int): Single<BaseResponse>


}