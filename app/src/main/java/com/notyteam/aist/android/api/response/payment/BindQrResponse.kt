package com.notyteam.aist.android.api.response.payment

import com.google.gson.annotations.SerializedName
import com.notyteam.aist.android.api.response.responses.BaseResponse
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
open class BindQrResponse(

        @field:SerializedName("data")
		val data: BindQrResponse,

        @field:SerializedName("attributes")
		val attributes: DetailQrResponse

): BaseResponse()