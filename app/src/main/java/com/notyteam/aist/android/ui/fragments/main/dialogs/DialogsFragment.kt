package com.notyteam.aist.android.ui.fragments.main.dialogs

import android.content.Intent
import android.util.Log
import android.view.View
import com.noty.aist.android.BuildConfig
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.api.response.responses.TokenResponse
import com.notyteam.aist.android.ui.fragments.BaseFragment
import im.aist.sdk.aist.ActorSDK
import im.aist.sdk.controllers.compose.CreateGroupActivity
import im.aist.sdk.controllers.contacts.ContactsActivity
import im.aist.sdk.controllers.root.RootFragment
import im.aist.sdk.util.ActorSDKMessenger.messenger
import io.paperdb.Paper

class DialogsFragment : BaseFragment(), DialogsFragmentView {
    override fun onGettingInterestsListSuccess(response: InterestsListResponse) {
        Paper.book().write("interestsList",response.data)

    }

    override fun onGettingInterestsListError(errorMessage: String) {}

    override fun onTokenSuccess(success: TokenResponse) {
        //showSuccessSnack(success.success.toString())
        //Log.d("myLogs","--- "+success.success )
    }

    override fun onTokenError(errorMessage: String) { showErrorSnack(errorMessage) }

    companion object {
        fun newInstance(): DialogsFragment {
            return DialogsFragment()
        }
    }

    override fun layout(): Int = R.layout.fragment_dialogs_main

    override fun initialization(view: View, isFirstInit: Boolean) {
        getSecondTypeToolbar()
                ?.setLeftIcon(R.drawable.ic_add_new_chat)
                ?.setRightIcon(R.drawable.ic_add_new_group_chat)
                ?.setTitle(getString(R.string.bottom_bar_dialogs))
                ?.setOnLeftIconListener {
                    val intent = Intent(activity, ContactsActivity::class.java)
                    startActivity(intent)
                }
                ?.setOnRightIconListener {
                    val intent = Intent(activity, CreateGroupActivity::class.java)
                    startActivity(intent)
        }

        var fragment = ActorSDK.sharedActor().delegate.fragmentForRoot()
        if (fragment == null) {
            fragment = RootFragment()
        }
        activity?.getSupportFragmentManager()?.beginTransaction()
                ?.add(R.id.ftc_container, fragment)
                ?.commit()

       // presenter.setToken()
        presenter.getInterestsList()


    }

    private var presenter: DialogsFragmentPresenter = DialogsFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }


}
