package com.notyteam.aist.android.ui.fragments.container

import android.os.Bundle
import com.noty.aist.android.R
import com.notyteam.aist.android.Screens
import com.notyteam.aist.android.ui.fragments.main.payment.PaymentFragment

class PaymentFragmentContainer: ContainerFragment() {

    companion object {
        fun getNewInstance(name: String): PaymentFragmentContainer {
            val fragment = PaymentFragmentContainer()
            val arguments = Bundle()
            arguments.putString(EXTRA_NAME, name)
            fragment.arguments = arguments
            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.ftc_container) == null) {
            getCicerone().router.replaceScreen(Screens.FragmentScreen(PaymentFragment()))
        }
    }
}