package com.notyteam.aist.android.ui.fragments.main.catalog.presenter

import com.notyteam.aist.android.AistApp.Companion.aistApi
import com.notyteam.aist.android.mvp.BasePresenter
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.ForMeGoodsFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.goods.GoodsListFragment

class GoodsListForMeFragmentPresenter(view: ForMeGoodsFragment?): BasePresenter<ForMeGoodsFragment>(view) {


    fun getGoodsListForMe(interest_id:Int){
        requests.add(aistApi.getGoodsListForMe(interest_id,{
            view?.onCatalogListSuccessPagePerPage(it)
        }, { errorMessage, errorCode ->
            view?.onCatalogListError(errorMessage)
        }).sendRequest())
    }



}