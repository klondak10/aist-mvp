package com.notyteam.aist.android.api.retrofit

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.noty.aist.android.BuildConfig
import com.notyteam.aist.android.AistApp
import com.notyteam.aist.android.utils.paper.PaperIO
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class RetrofitModule(val application: AistApp) {

    private fun provideHttpCache(application: Application): Cache {
        val cacheSize = 10L * 1024 * 1024
        return Cache(application.cacheDir, cacheSize)
    }

    private fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        return gsonBuilder.create()
    }

    private fun provideOkhttpClient(cache: Cache): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.cache(cache)

        val defaultLocale = Locale.getDefault()

        if (BuildConfig.DEBUG) {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(httpLoggingInterceptor)
        }

        httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        httpClient.addInterceptor { chain ->
            val request = chain.request()
            val requestBuilder = request.newBuilder()

            val authId = PaperIO.getAuthId()
            val sessionId = PaperIO.getSessionId()

            if (authId != 0 && sessionId != 0)
                requestBuilder
                        .header("X-Auth-Id", authId.toString())
                        .header("X-Session-Id", sessionId.toString())
                        .header("X-Locale", defaultLocale.language)

            chain.proceed(requestBuilder.build())
        }

        return httpClient.build()
    }

    private fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    val retrofit: Retrofit by lazy {
        val cache = provideHttpCache(application)
        val okHttpClient = provideOkhttpClient(cache)
        val gson = provideGson()
        provideRetrofit(gson, okHttpClient)
    }
}