package com.notyteam.aist.android.ui.models.ui

import im.aist.core.entity.Avatar
import im.aist.core.entity.Peer

data class UIDialogs(
        var link: Avatar?,
        var peer: Peer?,
        var name: String = "",
        var code: String = "",
        var time: String = ""
) : Comparable<UIDialogs> {
    override fun compareTo(other: UIDialogs): Int {
        return name.compareTo(other.name)
    }
}