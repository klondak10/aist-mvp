package com.notyteam.aist.android.ui.fragments.main.catalog.goods


import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.catalog.CatalogListResponse
import com.notyteam.aist.android.api.response.catalog.CategoriesListById
import com.notyteam.aist.android.api.response.catalog.GoodsResponseById
import com.notyteam.aist.android.ui.adapters.catalog.CategoriesAdapter
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.CatalogFragment
import com.notyteam.aist.android.ui.fragments.main.catalog.presenter.CatalogFragmentPresenter
import com.notyteam.aist.android.ui.fragments.main.catalog.view.CatalogFragmentView
import com.notyteam.aist.android.ui.models.ui.catalog.UICategoriesCatalog
import com.notyteam.aist.android.utils.recyclerViewVertical

import kotlinx.android.synthetic.main.fragment_dialogs_main.*

class CategoryListGoodsFragment : BaseFragment(), CatalogFragmentView {

    override fun onFavoritesError(error: String) {}
    override fun onFavoritesSuccess(success: Boolean) {}
    override fun onGoodsByIdSuccess(data: GoodsResponseById) {}
    override fun onCatalogListSuccessPagePerPage(list: CatalogListResponse) {}
    override fun onCatalogListByIdSuccess(list: CategoriesListById) {}

    companion object {
        fun newInstance(): CategoryListGoodsFragment {
              return CategoryListGoodsFragment()
        }
    }

    private var presenter = CatalogFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }
    lateinit var adapter: CategoriesAdapter

    override fun layout(): Int = R.layout.fragment_dialogs_main

    override fun initialization(view: View, isFirstInit: Boolean) {

        getSecondTypeToolbar()
                ?.setTitle(getString(R.string.bottom_bar_catalog))
                ?.toggleLeftIcon(isVisible = false)
                ?.toggleRightIcon(isVisible = false)
        initViews()

    }

    private fun initViews() {
        showProgress(true)
        presenter.getCategoryList()

        recyclerViewVertical(rv_dialogs)
        adapter = CategoriesAdapter(arrayListOf(),
            onClick = {
                if(it.has_sub_categories==true){
                    (parentFragment as CatalogFragment).openCategory(it)
                }
            }
        )
        rv_dialogs.adapter = adapter
    }

    override fun onCatalogListSuccess(list: CatalogListResponse) {
        showProgress(false)
        val mappedList = list.data.flatMap { category ->
            arrayListOf( UICategoriesCatalog(category.is_favorite,category.in_wish_list,category.name,category.has_sub_categories, category.id) )
        } as ArrayList
        adapter.clear()
        adapter.addAll(mappedList)

    }

    override fun onCatalogListError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }


}
