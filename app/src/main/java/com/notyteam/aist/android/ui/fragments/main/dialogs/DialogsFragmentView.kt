package com.notyteam.aist.android.ui.fragments.main.dialogs

import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.api.response.responses.TokenResponse
import com.notyteam.aist.android.mvp.PresenterDetachInterface

interface DialogsFragmentView: PresenterDetachInterface {

    fun onTokenSuccess(success: TokenResponse)
    fun onTokenError(errorMessage: String)
    fun onGettingInterestsListSuccess(response: InterestsListResponse)
    fun onGettingInterestsListError(errorMessage: String)



}