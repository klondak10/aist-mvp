package com.notyteam.aist.android.ui.fragments.main.setting.categories

import android.view.View
import com.noty.aist.android.R
import com.notyteam.aist.android.api.response.interest.all_interests.InterestsListResponse
import com.notyteam.aist.android.ui.adapters.CategoriesAdapter
import com.notyteam.aist.android.ui.fragments.BaseFragment
import com.notyteam.aist.android.ui.models.ui.UICategories
import com.notyteam.aist.android.utils.recyclerViewVertical
import kotlinx.android.synthetic.main.fragment_categories.*


class CategoriesFragment : BaseFragment(), CategoriesFragmentView {
    companion object {
        fun newInstance(): CategoriesFragment {
            return CategoriesFragment()
        }
    }

    private var presenter: CategoriesFragmentPresenter = CategoriesFragmentPresenter(this)
    override fun presenterDetach() {
        presenterDetachInterface = presenter
    }

    var selectedInterestsList = arrayListOf<UICategories>()

    override fun layout(): Int = R.layout.fragment_categories

    lateinit var adapter: CategoriesAdapter

    override fun initialization(view: View, isFirstInit: Boolean) {
        getFirstTypeToolbar()
                ?.setRightText(getString(R.string.complete))
                ?.setLeftText(getString(R.string.back))
                ?.setOnBackPressedListener {
                    onBackPressed()
                }
                ?.setOnRightTextListener {
                    setResult(adapter.getCheckedItems())
                    onBackPressed()
                }
        if (isFirstInit) {
            initViews()
            getInterestsList()
        }
    }

    private fun initViews() {
        recyclerViewVertical(rv_categories)
        adapter = CategoriesAdapter(arrayListOf()) {}
        rv_categories.adapter = adapter
    }

    private fun getInterestsList(){
        showProgress(true)
        presenter.getInterestsList()
    }

    override fun onInterestsListSuccess(list: InterestsListResponse) {
        showProgress(false)
        val mappedList = list.data.flatMap { interest ->
            var isSelected = false
            selectedInterestsList.forEach { if(it.id == interest.id) { isSelected = true; return@forEach} }
            arrayListOf( UICategories(interest.translatedName, interest.id, isSelected) )
        } as ArrayList
        adapter.clear()
        adapter.addAll(mappedList)
    }

    override fun onInterestsListError(errorMessage: String) {
        showProgress(false)
        showErrorSnack(errorMessage)
    }
}
