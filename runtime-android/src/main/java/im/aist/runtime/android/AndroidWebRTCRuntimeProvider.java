package im.aist.runtime.android;

import android.os.Handler;
import android.os.HandlerThread;

import org.jetbrains.annotations.NotNull;
import org.webrtc.PeerConnectionFactory;

import im.aist.runtime.WebRTCRuntime;
import im.aist.runtime.android.webrtc.AndroidAudioSource;
import im.aist.runtime.android.webrtc.AndroidVideoSource;
import im.aist.runtime.promise.Promise;
import im.aist.runtime.promise.PromiseFunc;
import im.aist.runtime.android.webrtc.AndroidMediaStream;
import im.aist.runtime.android.webrtc.AndroidPeerConnection;
import im.aist.runtime.webrtc.WebRTCIceServer;
import im.aist.runtime.webrtc.WebRTCMediaStream;
import im.aist.runtime.webrtc.WebRTCPeerConnection;
import im.aist.runtime.webrtc.WebRTCSettings;

public class AndroidWebRTCRuntimeProvider implements WebRTCRuntime {

    public static final PeerConnectionFactory FACTORY;
    private static final Handler sVcHandler;

    static {
        PeerConnectionFactory.initializeAndroidGlobals(AndroidContext.getContext(), true, true, true);
        FACTORY = new PeerConnectionFactory();
        HandlerThread vcthread = new HandlerThread("PeerConnectionConnectionThread");
        vcthread.start();
        sVcHandler = new Handler(vcthread.getLooper());
    }


    @NotNull
    @Override
    public Promise<WebRTCPeerConnection> createPeerConnection(final WebRTCIceServer[] webRTCIceServers, final WebRTCSettings settings) {
        return new Promise<>((PromiseFunc<WebRTCPeerConnection>) resolver -> {
            resolver.result(new AndroidPeerConnection(webRTCIceServers, settings));
        });
    }

    @NotNull
    @Override
    public Promise<WebRTCMediaStream> getUserMedia(boolean isAudioEnabled, boolean isVideoEnabled) {
        return new Promise<>((PromiseFunc<WebRTCMediaStream>) resolver -> {
            sVcHandler.post(() -> {
                AndroidAudioSource audioSource = null;
                if (isAudioEnabled) {
                    audioSource = AndroidAudioSource.pickAudioSource();
                }
                AndroidVideoSource videoSource = null;
                if (isVideoEnabled) {
                    videoSource = AndroidVideoSource.pickVideoSource();
                }
                resolver.result(new AndroidMediaStream(audioSource, videoSource));
            });
        });
    }

    @Override
    public boolean supportsPreConnections() {
        return false;
    }

    public static void postToHandler(Runnable r) {
        sVcHandler.post(r);
    }
}