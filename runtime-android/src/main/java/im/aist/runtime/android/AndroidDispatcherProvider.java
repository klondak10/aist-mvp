/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.runtime.android;

import im.aist.runtime.DispatcherRuntime;
import im.aist.runtime.actors.ThreadPriority;
import im.aist.runtime.android.threading.AndroidImmediateDispatcher;

public class AndroidDispatcherProvider implements DispatcherRuntime {

    private AndroidImmediateDispatcher dispatcher;

    public AndroidDispatcherProvider() {

    }

    @Override
    public synchronized void dispatch(Runnable runnable) {
        if (dispatcher == null) {
            dispatcher = new AndroidImmediateDispatcher("callback_dispatcher", ThreadPriority.LOW);
        }
        dispatcher.dispatchNow(runnable);
    }
}
