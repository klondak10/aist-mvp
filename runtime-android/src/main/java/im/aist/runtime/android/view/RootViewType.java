package im.aist.runtime.android.view;

public enum RootViewType {
    FRAME_LAYOUT, LINEAR_LAYOUT
}
