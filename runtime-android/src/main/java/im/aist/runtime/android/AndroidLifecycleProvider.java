/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */
package im.aist.runtime.android;

import im.aist.runtime.LifecycleRuntime;
import im.aist.runtime.android.power.AndroidWakeLock;
import im.aist.runtime.power.WakeLock;

public class AndroidLifecycleProvider implements LifecycleRuntime {

    @Override
    public void killApp() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    public WakeLock makeWakeLock() {
        return new AndroidWakeLock();
    }
}
