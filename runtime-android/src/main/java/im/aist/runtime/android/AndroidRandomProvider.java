package im.aist.runtime.android;

import im.aist.runtime.generic.GenericRandomProvider;
import im.aist.runtime.android.crypto.PRNGFixes;

public class AndroidRandomProvider extends GenericRandomProvider {

    private static boolean isFixApplied = false;

    public AndroidRandomProvider() {
        if (isFixApplied) {
            isFixApplied = true;
            PRNGFixes.apply();
        }
    }
}
