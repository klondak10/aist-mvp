/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.runtime.android;

import im.aist.runtime.android.crypto.AndroidSHA256;
import im.aist.runtime.crypto.Digest;
import im.aist.runtime.generic.GenericCryptoProvider;

public class AndroidCryptoProvider extends GenericCryptoProvider {

    @Override
    public Digest SHA256() {
        return new AndroidSHA256();
    }
}
