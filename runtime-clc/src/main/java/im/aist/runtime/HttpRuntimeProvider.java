package im.aist.runtime;

import im.aist.runtime.http.HTTPResponse;
import im.aist.runtime.promise.Promise;

public class HttpRuntimeProvider implements HttpRuntime {

    @Override
    public Promise<HTTPResponse> getMethod(String url, int startOffset, int size, int totalSize) {
        return Promise.failure(new RuntimeException("Dumb"));
    }

    @Override
    public Promise<HTTPResponse> putMethod(String url, byte[] contents) {
        return Promise.failure(new RuntimeException("Dumb"));
    }
}
