package im.aist.runtime;

import im.aist.runtime.promise.Promise;
import im.aist.runtime.webrtc.WebRTCIceServer;
import im.aist.runtime.webrtc.WebRTCMediaStream;
import im.aist.runtime.webrtc.WebRTCPeerConnection;
import im.aist.runtime.webrtc.WebRTCSettings;
import org.jetbrains.annotations.NotNull;

public class WebRTCRuntimeProvider implements WebRTCRuntime {

    @NotNull
    @Override
    public Promise<WebRTCPeerConnection> createPeerConnection(WebRTCIceServer[] webRTCIceServers, WebRTCSettings settings) {
        return null;
    }

    @NotNull
    @Override
    public Promise<WebRTCMediaStream> getUserMedia(boolean isAudioEnabled, boolean isVideoEnabled) {
        return null;
    }

    @Override
    public boolean supportsPreConnections() {
        return false;
    }
}
