package im.aist.runtime;

import im.aist.runtime.generic.GenericEnginesProvider;
import im.aist.runtime.generic.mvvm.DisplayList;

public class EnginesRuntimeProvider extends GenericEnginesProvider {
    public EnginesRuntimeProvider() {
        super(DisplayList.OperationMode.GENERAL);
    }
}
