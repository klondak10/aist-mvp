package im.aist.runtime;

import im.aist.runtime.clc.ClcDispatcher;
import im.aist.runtime.generic.*;
import im.aist.runtime.threading.Dispatcher;

public class ThreadingRuntimeProvider extends GenericThreadingProvider {

    public ThreadingRuntimeProvider(){
    }

    @Override
    public Dispatcher createDispatcher(String name) {
        return new ClcDispatcher(name);
    }
}
