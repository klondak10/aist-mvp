package im.aist.runtime.clc;

import im.aist.runtime.LifecycleRuntime;
import im.aist.runtime.power.WakeLock;

/**
 * Created by amir on 3/14/16.
 */
public class ClcLifecycleProvider implements LifecycleRuntime {

    @Override
    public void killApp() {
    }

    @Override
    public WakeLock makeWakeLock() {
        return new ClcWakeLock();
    }
}
