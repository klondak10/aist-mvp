package im.aist.runtime.clc;

/**
 * Created by amir on 4/13/16.
 */
public interface ClcContext {
    void setContext(String context);
    String getContext();
}
