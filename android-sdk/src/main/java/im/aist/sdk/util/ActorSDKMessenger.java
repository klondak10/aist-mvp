package im.aist.sdk.util;

import im.aist.core.AndroidMessenger;
import im.aist.core.entity.Group;
import im.aist.core.entity.User;
import im.aist.core.viewmodel.GroupVM;
import im.aist.core.viewmodel.UserVM;
import im.aist.runtime.mvvm.MVVMCollection;
import im.aist.sdk.aist.ActorSDK;

public class ActorSDKMessenger {

    public static AndroidMessenger messenger() {
        return ActorSDK.sharedActor().getMessenger();
    }

    public static MVVMCollection<User, UserVM> users() {
        ActorSDK.sharedActor().waitForReady();
        return messenger().getUsers();
    }

    public static MVVMCollection<Group, GroupVM> groups() {
        return messenger().getGroups();
    }

    public static int myUid() {
        ActorSDK.sharedActor().waitForReady();
        return messenger().myUid();
    }
}
