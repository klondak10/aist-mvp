package im.aist.sdk.view.emoji.keyboard.emoji;

import im.aist.core.entity.Sticker;

public interface OnStickerClickListener {
    void onStickerClicked(Sticker sticker);
}