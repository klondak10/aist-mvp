package im.aist.sdk.view.emoji.keyboard.emoji.smiles;

/**
* Created by Jesus Christ. Amen.
*/
public interface OnSmileClickListener {
    public void onEmojiClicked(String smile);
}