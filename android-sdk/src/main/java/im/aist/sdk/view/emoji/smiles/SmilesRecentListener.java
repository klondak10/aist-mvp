/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.sdk.view.emoji.smiles;

/**
 * Created by Jesus Christ. Amen.
 */
public interface SmilesRecentListener {
     void onSmilesUpdated();
}
