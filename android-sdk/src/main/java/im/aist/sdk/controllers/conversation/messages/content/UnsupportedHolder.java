/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.sdk.controllers.conversation.messages.content;

import android.view.View;

import im.aist.core.entity.Peer;
import im.aist.sdk.R;
import im.aist.core.entity.Message;
import im.aist.sdk.controllers.conversation.messages.MessagesAdapter;
import im.aist.sdk.controllers.conversation.messages.content.preprocessor.PreprocessedData;

public class UnsupportedHolder extends TextHolder {

    protected String text;

    public UnsupportedHolder(MessagesAdapter fragmeadaptert, View itemView, Peer peer) {
        super(fragmeadaptert, itemView, peer);

        text = fragmeadaptert.getMessagesFragment().getResources().getString(R.string.chat_unsupported);
        onConfigureViewHolder();
    }

    @Override
    protected void bindData(Message message, long readDate, long receiveDate, boolean isUpdated, PreprocessedData preprocessedData) {
        bindRawText(text, readDate, receiveDate, null, message, true);
    }
}
