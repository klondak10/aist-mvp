package im.aist.sdk.controllers.contacts;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import im.aist.core.entity.PhoneBookContact;
import im.aist.core.viewmodel.CommandCallback;
import im.aist.core.viewmodel.UserVM;
import im.aist.runtime.Log;
import im.aist.sdk.R;
import im.aist.sdk.aist.ActorSDK;
import im.aist.sdk.controllers.Intents;
import im.aist.sdk.controllers.activity.BaseFragmentActivity;
import im.aist.sdk.controllers.dialogs.DialogsDefaultFragment;
import im.aist.sdk.core.AndroidPhoneBook;
import im.aist.sdk.util.KeyboardHelper;
import im.aist.sdk.view.adapters.OnItemClickedListener;
import io.paperdb.Paper;

import static im.aist.sdk.util.ActorSDKMessenger.messenger;

public class AddContactActivity extends BaseFragmentActivity {

    private KeyboardHelper helper;
    private EditText searchQuery;
    String phoneNumber;
    private List<PhoneBookContact> contacts;
    PhoneBookContact phoneBookContact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(false);
        getSupportActionBar().setTitle(R.string.add_contact_title);

        helper = new KeyboardHelper(this);

        setContentView(R.layout.activity_add);
        findViewById(R.id.container).setBackgroundColor(ActorSDK.sharedActor().style.getMainBackgroundColor());

        searchQuery = (EditText) findViewById(R.id.searchField);
        searchQuery.setTextColor(ActorSDK.sharedActor().style.getTextPrimaryColor());
        searchQuery.setHintTextColor(ActorSDK.sharedActor().style.getTextHintColor());
        findViewById(R.id.dividerTop).setBackgroundColor(ActorSDK.sharedActor().style.getDividerColor());
        findViewById(R.id.dividerBot).setBackgroundColor(ActorSDK.sharedActor().style.getDividerColor());

        ((TextView) findViewById(R.id.cancel)).setTextColor(ActorSDK.sharedActor().style.getTextPrimaryColor());
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ((TextView) findViewById(R.id.ok)).setTextColor(ActorSDK.sharedActor().style.getTextPrimaryColor());
        findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String query = searchQuery.getText().toString();
                if (query.length() == 0) {
                    return;
                }
                execute(messenger().findUsers(query), R.string.progress_common, new CommandCallback<UserVM[]>() {
                    @Override
                    public void onResult(final UserVM[] res) {
                        if (res.length == 0) {

                                    AddContactActivity.this.runOnUiThread(() -> {
                                        boolean aboolean = false;
                                        Cursor cursor = null;
                                        try {
                                            cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                                           int phoneNumberIdx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                                           cursor.moveToFirst();
                                            do {

                                                String phoneNumber = cursor.getString(phoneNumberIdx);
                                                if(phoneNumber.contains(query)){
                                                    aboolean = true;
                                                    break;
                                                }
                                            } while (cursor.moveToNext());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        } finally {
                                            if (cursor != null) {
                                                cursor.close();
                                            }
                                        }
                                       if(aboolean){
                                            new AlertDialog.Builder(AddContactActivity.this)
                                                    .setMessage(getString(R.string.alert_invite_text).replace("{0}", query)
                                                            .replace("{appName}", ActorSDK.sharedActor().getAppName()))
                                                    .setPositiveButton(R.string.alert_invite_yes, (dialogInterface, i1) -> {
                                                        String inviteMessage = getString(R.string.invite_message).replace("{inviteUrl}",
                                                                ActorSDK.sharedActor().getInviteUrl()).replace("{appName}",
                                                                ActorSDK.sharedActor().getAppName());
                                                        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                                                        sendIntent.setData(Uri.parse("sms:" + query));
                                                        sendIntent.putExtra("sms_body", inviteMessage);
                                                        startActivity(sendIntent);
                                                        finish();
                                                    })
                                                    .setNegativeButton(R.string.dialog_cancel, null)
                                                    .show()
                                                    .setCanceledOnTouchOutside(true);

                                        }else {
                                            new AlertDialog.Builder(AddContactActivity.this)
                                                    .setMessage("Добавить новый контакт в тел. книгу")
                                                    .setPositiveButton("Да", (dialogInterface, i1) -> {
                                                        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                                        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                                        contactIntent
                                                                .putExtra(ContactsContract.Intents.Insert.NAME, "")
                                                                .putExtra(ContactsContract.Intents.Insert.PHONE, query);

                                                        startActivityForResult(contactIntent, 1);
                                                    })
                                                    .setNegativeButton(R.string.dialog_cancel, null)
                                                    .show()
                                                    .setCanceledOnTouchOutside(true);


                                        }


                                    });




                        } else {
                            execute(messenger().addContact(res[0].getId()), R.string.progress_common, new CommandCallback<Boolean>() {
                                @Override
                                public void onResult(Boolean res2) {
                                    startActivity(Intents.openPrivateDialog(res[0].getId(),
                                            true,
                                            AddContactActivity.this));
                                    finish();
                                }

                                @Override
                                public void onError(Exception e) {
                                    startActivity(Intents.openPrivateDialog(res[0].getId(),
                                            true,
                                            AddContactActivity.this));
                                    finish();
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        // Never happens
                    }
                });
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == 1)
        {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, "Номер успекшно добавлен", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Вы отменили добавления номера", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        helper.setImeVisibility(searchQuery, true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        helper.setImeVisibility(searchQuery, false);
    }
}
