package im.aist.sdk.controllers.fragment.help;

import android.os.Bundle;

import im.aist.sdk.R;
import im.aist.sdk.controllers.activity.BaseFragmentActivity;

public class HelpActivity extends BaseFragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(R.string.help_title);

        showFragment(new HelpFragment(), false);
    }
}
