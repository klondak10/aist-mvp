package im.aist.sdk.controllers.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;

import im.aist.sdk.controllers.activity.BaseFragmentActivity;

public class ContactsActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            showFragment(new ContactsFragment(), false);
        }
    }

}
