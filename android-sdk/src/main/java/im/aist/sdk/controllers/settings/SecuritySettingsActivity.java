package im.aist.sdk.controllers.settings;

import android.os.Bundle;

import im.aist.sdk.aist.ActorSDK;
import im.aist.sdk.R;
import im.aist.sdk.controllers.activity.BaseFragmentActivity;
import im.aist.sdk.intents.ActorIntentFragmentActivity;

public class SecuritySettingsActivity extends BaseFragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(R.string.security_title);

        SecuritySettingsFragment fragment;
        ActorIntentFragmentActivity securitySettingsIntent = ActorSDK.sharedActor().getDelegate().getSecuritySettingsIntent();
        if (securitySettingsIntent != null) {
            fragment = (SecuritySettingsFragment) securitySettingsIntent.getFragment();
        } else {
            fragment = new SecuritySettingsFragment();
        }

        if (savedInstanceState == null) {
            showFragment(fragment, false);
        }
    }
}
