package im.aist.sdk.controllers.search;

public interface GlobalSearchStateDelegate {

    void onGlobalSearchStarted();

    void onGlobalSearchEnded();
}
