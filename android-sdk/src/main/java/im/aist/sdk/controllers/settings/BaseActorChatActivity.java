package im.aist.sdk.controllers.settings;

import android.content.Intent;

import im.aist.core.entity.Peer;
import im.aist.sdk.controllers.conversation.messages.MessagesFragment;
import im.aist.sdk.intents.ActorIntentFragmentActivity;

public abstract class BaseActorChatActivity extends ActorIntentFragmentActivity {
    public BaseActorChatActivity(Intent intent) {
        super(intent);
    }

    public BaseActorChatActivity(Intent intent, MessagesFragment fragment) {
        super(intent, fragment);
    }

    public BaseActorChatActivity() {
        super();
    }

    public MessagesFragment getChatFragment(Peer peer) {
        return null;
    }
}
