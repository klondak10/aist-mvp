package im.aist.sdk.controllers;

import im.aist.core.entity.Peer;

public interface PeerListFragmentDelegate {
    void onPeerClicked(Peer peer);

    boolean onPeerLongClicked(Peer peer);
}
