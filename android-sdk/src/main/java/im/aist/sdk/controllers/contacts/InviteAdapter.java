package im.aist.sdk.controllers.contacts;

import android.content.Context;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import im.aist.core.entity.PhoneBookContact;
import im.aist.sdk.view.adapters.HolderAdapter;
import im.aist.sdk.view.adapters.OnItemClickedListener;
import im.aist.sdk.view.adapters.ViewHolder;
import io.paperdb.Paper;

import static im.aist.sdk.util.ActorSDKMessenger.messenger;

public class InviteAdapter extends HolderAdapter<PhoneBookContact> {

    List<PhoneBookContact> phoneBook;
    List<PhoneBookContact> phoneBookFilter ;
    Context context;
    private final HashSet<PhoneBookContact> selectedUsers = new HashSet<PhoneBookContact>();
    private final HashMap<PhoneBookContact, Integer> selectedContactType = new HashMap<PhoneBookContact, Integer>();
    private String query = "";
    private OnItemClickedListener<PhoneBookContact> onItemClickedListener;
    PhoneBookContact itemsCopy,items;

    public InviteAdapter(Context context, List<PhoneBookContact> phoneBook, OnItemClickedListener<PhoneBookContact> onItemClickedListener) {
        super(context);
        this.phoneBook = phoneBook;
        this.phoneBookFilter = phoneBook;
        selectedUsers.addAll(phoneBook);
        this.context = context;
        this.onItemClickedListener = onItemClickedListener;
    }


    @Override
    protected void onBindViewHolder(ViewHolder<PhoneBookContact> holder, PhoneBookContact obj, int index, Context context) {
        itemsCopy = phoneBook.get(index);

        String fastName = null;
        if (index == 0) {
            fastName = messenger().getFormatter().formatFastName(itemsCopy.getName());
        } else {
            String prevName = messenger().getFormatter().formatFastName(phoneBook.get(index - 1).getName());
            String currentFastName = messenger().getFormatter().formatFastName(itemsCopy.getName());
            if (!prevName.equals(currentFastName)) {
                fastName = currentFastName;
            }
        }
        Integer type = selectedContactType.get(itemsCopy);
        ((InviteContactHolder) holder).bind(itemsCopy, fastName, "", selectedUsers.contains(itemsCopy), type == null ? -1 : type, index == getCount() - 1);
    }

    public  void filter(String text) {

        phoneBookFilter = Paper.book().read("listContact", new ArrayList<>());
        phoneBook.clear();
        text = text.toLowerCase();

        if(text.isEmpty()){
            phoneBook.addAll(phoneBookFilter);
            notifyDataSetChanged();
        } else {
            for (int i = 0; i < phoneBookFilter.size(); i++) {
                items = phoneBookFilter.get(i);
                if (items.getName().toLowerCase().contains(text) || items.getPhones().toString().toLowerCase().contains(text)) {
                    phoneBook.add(items);
                }
            }
            notifyDataSetChanged();
        }

    }



    public void select(PhoneBookContact contact, int type) {
        selectedUsers.add(contact);
        selectedContactType.put(contact, type);
    }

    public void unselect(PhoneBookContact uid) {
        selectedUsers.remove(uid);
    }

    public PhoneBookContact[] getSelected() {
        return selectedUsers.toArray(new PhoneBookContact[selectedUsers.size()]);
    }

    public HashMap<PhoneBookContact, Integer> getSelectedContactsTypes() {
        return selectedContactType;
    }

    public boolean isSelected(PhoneBookContact id) {
        return selectedUsers.contains(id);
    }

    @Override
    public int getCount() {
        return phoneBook.size();
    }

    @Override
    public PhoneBookContact getItem(int position) {
        return phoneBook.get(position);
    }

    @Override
    public long getItemId(int position) {
        return phoneBook.get(position).getContactId();
    }

    @Override
    protected ViewHolder<PhoneBookContact> createHolder(PhoneBookContact obj) {
        return new InviteContactHolder(new FrameLayout(context), context, onItemClickedListener);
    }

    public void selectNone() {
        selectedContactType.clear();
        selectedUsers.clear();
        notifyDataSetChanged();
    }

    public void selectAll() {
        selectedUsers.addAll(phoneBook);
        notifyDataSetChanged();
    }
}
