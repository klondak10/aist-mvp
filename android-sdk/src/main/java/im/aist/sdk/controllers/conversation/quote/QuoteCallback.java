package im.aist.sdk.controllers.conversation.quote;

public interface QuoteCallback {
    void onQuoteCancelled();
}
