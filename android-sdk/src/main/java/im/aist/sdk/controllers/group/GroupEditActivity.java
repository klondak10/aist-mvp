package im.aist.sdk.controllers.group;

import android.os.Bundle;

import im.aist.sdk.controllers.Intents;
import im.aist.sdk.controllers.activity.BaseFragmentActivity;

public class GroupEditActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            showFragment(GroupEditFragment.create(
                    getIntent().getIntExtra(Intents.EXTRA_GROUP_ID, 0)), false);
        }
    }
}
