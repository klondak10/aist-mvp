package im.aist.sdk.controllers.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import im.aist.sdk.aist.ActorSDK;
import im.aist.sdk.aist.ActorStyle;
import im.aist.sdk.R;
import im.aist.sdk.controllers.conversation.view.ChatBackgroundView;
import im.aist.sdk.controllers.BaseFragment;
import im.aist.sdk.util.Screen;
import im.aist.sdk.view.BackgroundPreviewView;
import io.paperdb.Paper;

import static im.aist.sdk.util.ActorSDKMessenger.messenger;

public class PickWallpaperFragment extends BaseFragment {

    private ChatBackgroundView wallpaper;
    SharedPreferences shp;
    SharedPreferences.Editor ed;
    int selectedWallpaper = 0;
    private static final int REQUEST_GALLERY = 0;

    public static PickWallpaperFragment chooseWallpaper(int id) {
        Bundle args = new Bundle();
        args.putInt("EXTRA_ID", id);
        PickWallpaperFragment res = new PickWallpaperFragment();
        res.setArguments(args);
        return res;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActorStyle style = ActorSDK.sharedActor().style;
        shp = getActivity().getSharedPreferences("wallpaper", Context.MODE_PRIVATE);
        ed = shp.edit();
        selectedWallpaper = getArguments().getInt("EXTRA_ID");
        if (selectedWallpaper == -1) {
            selectedWallpaper = BackgroundPreviewView.getBackgroundIdByUri(messenger().getSelectedWallpaper(), getContext(), shp.getInt("wallpaper", 0));
        }
        View res = inflater.inflate(R.layout.fragment_pick_wallpaper, container, false);

        res.setBackgroundColor(ActorSDK.sharedActor().style.getMainBackgroundColor());
        ((TextView) res.findViewById(R.id.cancel)).setTextColor(ActorSDK.sharedActor().style.getTextPrimaryColor());
        ((TextView) res.findViewById(R.id.ok)).setTextColor(ActorSDK.sharedActor().style.getTextPrimaryColor());

        res.findViewById(R.id.dividerTop).setBackgroundColor(ActorSDK.sharedActor().style.getDividerColor());
        res.findViewById(R.id.dividerBot).setBackgroundColor(ActorSDK.sharedActor().style.getDividerColor());

        res.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        res.findViewById(R.id.rl_gallery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/* video/*");
                startActivityForResult(intent, REQUEST_GALLERY);

            }
        });

        res.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Фоновое изображение - успешно установлено", Toast.LENGTH_SHORT).show();
                Paper.book().delete("backWallpaper");
                messenger().changeSelectedWallpaper("local:".concat(getResources().getResourceEntryName(BackgroundPreviewView.getBackground(selectedWallpaper))));
                ed.putInt("wallpaper", selectedWallpaper);
                ed.commit();
                getActivity().finish();
            }
        });

        wallpaper = (ChatBackgroundView) res.findViewById(R.id.wallpaper);
        wallpaper.bind(selectedWallpaper);

        LinearLayout botContainer = (LinearLayout) res.findViewById(R.id.wallpaper_preview_container);
        LinearLayout wallpaperContainer = (LinearLayout) res.findViewById(R.id.background_container);
        botContainer.setBackgroundColor(style.getMainBackgroundColor());
        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                selectedWallpaper = (int) v.getTag();
                wallpaper.bind(selectedWallpaper);
            }
        };
        int previewSize = 90;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Screen.dp(previewSize), Screen.dp(previewSize));
        for (int i = 0; i < BackgroundPreviewView.getSize(); i++) {
            FrameLayout frame = new FrameLayout(getActivity());
            BackgroundPreviewView bckgrnd = new BackgroundPreviewView(getActivity());
            bckgrnd.init(Screen.dp(previewSize), Screen.dp(previewSize));
            bckgrnd.bind(i);
            //bckgrnd.setPadding(Screen.dp(5), Screen.dp(10), Screen.dp(5), Screen.dp(20));
            frame.setTag(i);
            frame.setOnClickListener(ocl);
            frame.addView(bckgrnd);
            wallpaperContainer.addView(frame, params);

        }

        return res;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_GALLERY) {
                if (data.getData() != null) {

                    ed.putInt("wallpaper", -2);
                    ed.commit();
                    Toast.makeText(getActivity(), "Фоновое изображение - успешно установлено", Toast.LENGTH_SHORT).show();
                    Uri uri = data.getData();
                    Paper.book().write("backWallpaper",getRealPathFromURI(uri));

                    im.aist.runtime.Log.d("myLogs","  "+getRealPathFromURI(uri));

                    getActivity().finish();
                }
            }
        }
    }



    public String getRealPathFromURI (Uri contentUri) {
        String path = null;
        String[] proj = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }


}
