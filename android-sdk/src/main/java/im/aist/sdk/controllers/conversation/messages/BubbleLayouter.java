package im.aist.sdk.controllers.conversation.messages;

import android.view.ViewGroup;

import im.aist.core.entity.Peer;
import im.aist.core.entity.content.AbsContent;
import im.aist.sdk.controllers.conversation.messages.content.AbsMessageViewHolder;

public interface BubbleLayouter {

    boolean isMatch(AbsContent content);

    AbsMessageViewHolder onCreateViewHolder(MessagesAdapter adapter, ViewGroup root, Peer peer);
}