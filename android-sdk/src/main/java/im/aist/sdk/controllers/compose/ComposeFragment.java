package im.aist.sdk.controllers.compose;

import android.content.Intent;

import im.aist.core.entity.Contact;
import im.aist.sdk.R;
import im.aist.sdk.aist.ActorSDK;
import im.aist.sdk.controllers.Intents;
import im.aist.sdk.controllers.contacts.BaseContactFragment;

public class ComposeFragment extends BaseContactFragment {

    public ComposeFragment() {
        super(true, true, false);
        setRootFragment(true);
        setTitle(R.string.compose_title);
        setHomeAsUp(true);
    }

    @Override
    protected void addFootersAndHeaders() {
        super.addFootersAndHeaders();

        addFooterOrHeaderAction(ActorSDK.sharedActor().style.getActionShareColor(),
                R.drawable.ic_group_white_24dp, R.string.main_fab_new_group, false, () -> {
                    startActivity(new Intent(getActivity(), CreateGroupActivity.class)
                            .putExtra(CreateGroupActivity.EXTRA_IS_CHANNEL, false));
                    getActivity().finish();
                }, true);

        addFooterOrHeaderAction(ActorSDK.sharedActor().style.getActionShareColor(),
                R.drawable.ic_megaphone_18dp_black, R.string.main_fab_new_channel, false, () -> {
                    startActivity(new Intent(getActivity(), CreateGroupActivity.class)
                            .putExtra(CreateGroupActivity.EXTRA_IS_CHANNEL, true));
                    getActivity().finish();
                }, true);
    }

    @Override
    public void onItemClicked(Contact contact) {
        getActivity().startActivity(Intents.openPrivateDialog(contact.getUid(), true, getActivity()));
        getActivity().finish();
    }
}
