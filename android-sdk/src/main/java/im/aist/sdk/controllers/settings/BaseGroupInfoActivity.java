package im.aist.sdk.controllers.settings;

import android.content.Intent;

import im.aist.sdk.controllers.group.GroupInfoFragment;
import im.aist.sdk.intents.ActorIntentFragmentActivity;

public abstract class BaseGroupInfoActivity extends ActorIntentFragmentActivity {
    public BaseGroupInfoActivity(Intent intent) {
        super(intent);
    }

    public BaseGroupInfoActivity(Intent intent, BaseActorSettingsFragment fragment) {
        super(intent, fragment);
    }

    public BaseGroupInfoActivity() {
        super();
    }

    public GroupInfoFragment getGroupInfoFragment(int chatId) {
        return GroupInfoFragment.create(chatId);
    }
}
