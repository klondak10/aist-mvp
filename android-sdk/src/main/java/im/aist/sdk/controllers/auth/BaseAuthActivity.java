package im.aist.sdk.controllers.auth;

import im.aist.sdk.intents.ActorIntentFragmentActivity;

public class BaseAuthActivity extends ActorIntentFragmentActivity {

    public BaseAuthActivity() {
        super();
    }

    public BaseAuthFragment getAuthFragment() {
        return null;
    }

}
