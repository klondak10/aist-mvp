package im.aist.sdk.controllers.contacts;

import android.os.Bundle;

import im.aist.sdk.R;
import im.aist.sdk.controllers.activity.BaseFragmentActivity;

public class InviteActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(R.string.contacts_invite_via_link);


        if (savedInstanceState == null) {
            showFragment(new InviteFragment(), false);
        }
    }
}
