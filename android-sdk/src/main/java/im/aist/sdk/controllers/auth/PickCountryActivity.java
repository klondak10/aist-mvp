package im.aist.sdk.controllers.auth;

import android.os.Bundle;

import im.aist.sdk.R;
import im.aist.sdk.controllers.activity.BaseFragmentActivity;

public class PickCountryActivity extends BaseFragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.auth_phone_country_title);

        if (savedInstanceState == null) {
            showFragment(new PickCountryFragment(), false);
        }
    }
}
