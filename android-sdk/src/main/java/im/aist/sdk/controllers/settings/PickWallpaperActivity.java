package im.aist.sdk.controllers.settings;

import android.os.Bundle;

import im.aist.sdk.R;
import im.aist.sdk.controllers.activity.BaseFragmentActivity;

public class PickWallpaperActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int id = getIntent().getIntExtra("EXTRA_ID", 0);

        getSupportActionBar().setTitle(R.string.wallpaper);

        if (savedInstanceState == null) {
            showFragment(PickWallpaperFragment.chooseWallpaper(id), false);
        }
    }
}
