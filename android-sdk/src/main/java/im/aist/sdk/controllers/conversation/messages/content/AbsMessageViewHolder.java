package im.aist.sdk.controllers.conversation.messages.content;

import android.view.View;

import im.aist.core.entity.Message;
import im.aist.runtime.android.view.BindedViewHolder;
import im.aist.sdk.controllers.conversation.messages.content.preprocessor.PreprocessedData;

public abstract class AbsMessageViewHolder extends BindedViewHolder {
    public AbsMessageViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindData(Message message, Message prev, Message next, long readDate, long receiveDate, PreprocessedData preprocessedData);

    public abstract void unbind();
}
