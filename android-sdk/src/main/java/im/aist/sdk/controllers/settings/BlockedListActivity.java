package im.aist.sdk.controllers.settings;

import android.os.Bundle;

import im.aist.sdk.R;
import im.aist.sdk.controllers.activity.BaseFragmentActivity;

public class BlockedListActivity extends BaseFragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle(R.string.blocked_title);


        if (savedInstanceState == null) {
            showFragment(new BlockedListFragment(), false);
        }
    }
}
