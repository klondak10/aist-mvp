package im.aist.sdk.receivers;

import im.aist.runtime.Log;
import im.aist.runtime.json.JSONObject;
import im.aist.sdk.aist.ActorSDK;
import im.aist.sdk.push.ActorPushReceiver;

public class SDKPushReceiver extends ActorPushReceiver {

    @Override
    public void onPushReceived(String payload) {
        try {
            JSONObject object = new JSONObject(payload);
            if (object.has("data")) {
                JSONObject data = object.getJSONObject("data");
                ActorSDK.sharedActor().waitForReady();
                if (data.has("seq")) {
                    int seq = data.getInt("seq");
                    int authId = data.optInt("authId");
                    Log.d("SDKPushReceiver", "Seq Received: " + seq);
                    ActorSDK.sharedActor().getMessenger().onPushReceived(seq, authId);
                } else if (data.has("callId")) {
                    Long callId = Long.parseLong(data.getString("callId"));
                    int attempt = 0;
                    if (data.has("attemptIndex")) {
                        attempt = data.getInt("attemptIndex");
                    }
                    Log.d("SDKPushReceiver", "Received Call #" + callId + " (" + attempt + ")");
                    ActorSDK.sharedActor().getMessenger().checkCall(callId, attempt);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}