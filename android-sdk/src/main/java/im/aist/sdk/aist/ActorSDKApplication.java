package im.aist.sdk.aist;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;

import im.aist.runtime.android.AndroidContext;

/**
 * Implementation of Application object that handles everything required for creating and
 * managing Actor SDK
 */
public class ActorSDKApplication extends Application {

    private static ActorSDKApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();

        int id = android.os.Process.myPid();
        String myProcessName = getPackageName();
        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo procInfo : activityManager.getRunningAppProcesses()) {
            if (id == procInfo.pid) {
                myProcessName = procInfo.processName;
            }
        }

        // Protection on double start
        if (!myProcessName.endsWith(":actor_push")) {
            AndroidContext.setContext(this);
            onConfigureActorSDK();
            ActorSDK.sharedActor().createActor(this);
        }
    }

    public ActorSDKApplication() {
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }

    /**
     * Override this method for implementing Actor SDK Implementation
     */
    public void onConfigureActorSDK() {

    }
}