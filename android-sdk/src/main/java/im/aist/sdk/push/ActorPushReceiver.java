package im.aist.sdk.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Abstract class for Actor Push receiver
 */
public abstract class ActorPushReceiver extends BroadcastReceiver {

    public ActorPushReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {

         onPushReceived(intent.getStringExtra("push_payload"));

    }

    /**
     * Called when push is received
     *
     * @param payload payload of the push
     */
    public abstract void onPushReceived(String payload);
}