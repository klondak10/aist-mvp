/*
 * Copyright (C) 2015 Actor LLC. <https://actor.im>
 */

package im.aist.runtime.cocoa;

import im.aist.runtime.cocoa.threading.CocoaDispatcher;
import im.aist.runtime.generic.GenericThreadingProvider;
import im.aist.runtime.threading.Dispatcher;

public class CocoaThreadingProvider extends GenericThreadingProvider {

    @Override
    public Dispatcher createDispatcher(String name) {
        return new CocoaDispatcher();
    }
}
