package im.aist.runtime.cocoa.threading;

import im.aist.runtime.threading.DispatchCancel;
import im.aist.runtime.threading.Dispatcher;

public class CocoaDispatcher implements Dispatcher {

    public static CocoaDispatcherProxy dispatcherProxy;

    public static CocoaDispatcherProxy getDispatcherProxy() {
        return dispatcherProxy;
    }

    public static void setDispatcherProxy(CocoaDispatcherProxy dispatcherProxy) {
        CocoaDispatcher.dispatcherProxy = dispatcherProxy;
    }

    @Override
    public DispatchCancel dispatch(Runnable message, long delay) {
        return dispatcherProxy.dispatchOnBackground(message, delay);
    }
}
