package im.aist.runtime.cocoa;

import com.google.j2objc.annotations.ObjectiveCName;

import org.jetbrains.annotations.NotNull;

import im.aist.runtime.WebRTCRuntime;
import im.aist.runtime.promise.Promise;
import im.aist.runtime.webrtc.WebRTCIceServer;
import im.aist.runtime.webrtc.WebRTCMediaStream;
import im.aist.runtime.webrtc.WebRTCPeerConnection;
import im.aist.runtime.webrtc.WebRTCSettings;

public class CocoaWebRTCProxyProvider implements WebRTCRuntime {

    private static WebRTCRuntime rtcRuntime;

    @ObjectiveCName("setWebRTCRuntime:")
    public static void setWebRTCRuntime(WebRTCRuntime rtcRuntime) {
        CocoaWebRTCProxyProvider.rtcRuntime = rtcRuntime;
    }

    @NotNull
    @Override
    public Promise<WebRTCPeerConnection> createPeerConnection(WebRTCIceServer[] webRTCIceServers, WebRTCSettings settings) {
        if (rtcRuntime == null) {
            return Promise.failure(new RuntimeException("WebRTC Runtime not set"));
        }
        return rtcRuntime.createPeerConnection(webRTCIceServers, settings);
    }

    @NotNull
    @Override
    public Promise<WebRTCMediaStream> getUserMedia(boolean isAudioEnabled, boolean isVideoEnabled) {
        if (rtcRuntime == null) {
            return Promise.failure(new RuntimeException("WebRTC Runtime not set"));
        }
        return rtcRuntime.getUserMedia(isAudioEnabled, isVideoEnabled);
    }

    @Override
    public boolean supportsPreConnections() {
        if (rtcRuntime == null) {
            throw new RuntimeException("WebRTC Runtime not set");
        }
        return rtcRuntime.supportsPreConnections();
    }
}
