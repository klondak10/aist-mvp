package im.aist.runtime.cocoa;

import im.aist.runtime.generic.GenericEnginesProvider;
import im.aist.runtime.generic.mvvm.DisplayList;

public class CocoaEnginesProvider extends GenericEnginesProvider {
    public CocoaEnginesProvider() {
        super(DisplayList.OperationMode.IOS);
    }
}
